<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommercialTraining extends Model
{
     protected $table = 'commercial_training';
}
