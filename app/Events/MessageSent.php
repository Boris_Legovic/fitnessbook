<?php
namespace App\Events;
use App\User;
use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * User that sent the message
     *
     * @var User
     */
    public $user;
    /**
     * Message details
     *
     * @var Message
     */
    public $messageData;
    public $chanel;
    public $usertToSendID;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($usertToSendID,$msg)
    {
        // $this->user = $user;
        $this->usertToSendID = $usertToSendID;
        $this->messageData = $msg;
        // $this->chanel = $chanel;


    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('message.' .  $this->usertToSendID);
    }
}