<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {

        $user = User::where('email',$request->email)->first();

     if($user != null)
     {
     
         if($user->status == 0)
         { 
             return ['email' => 'inactive','password'=>'Please go to your email and confirm Your account! If You did not revice email, please contact customer sevice.'];
         }
         else
         {
                return ['email'=>$request->email, 'password'=>$request->password,'status'=>1];
         }
     }
     else
     {
             return ['email'=>$request->email, 'password'=>$request->password,'status'=>1];
     }

    }
}
