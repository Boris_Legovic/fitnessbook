<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserOptions;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str; 
use Mail;
use App\Mail\verifyEmail;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'type' => 'required',
            'gender' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        Session::flash('status','Registered! but verify your email to activate your account!'); 

        $user = User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'type' => $data['type'], 
            'gender' => $data['gender'],
             'status' => 0,
            'verify_token' =>Str::random(40),
        ]);
        $thisUser = User::findOrFail($user->id);

        $coaching = "No";

        if($data['type'] == 'trainer')
        {
            $coaching = "Yes";
        }

        UserOptions::create([
            'user_id' => $user->id,
            'birthdate' => NULL,
            'max_number_of_pictures' => "15",
            'online_coaching_period' => "1", 
            'coaching_price' => 0,
             'currency' => '€',
            'weight_unit' =>'kg',
            'size_unit' =>'cm',
            'onlineCoaching' =>$coaching,
        ]);


        $this->sendEmail($thisUser);
       
        return $user;
    }

    public function sendEmail($thisUser)
    {
      

    
     $userMail = $thisUser->email;

     $data = ['email'=>$thisUser->email, 'token'=>$thisUser->verify_token];

     Mail::send('email.backup_email', $data, function ($m) use ($userMail) {
            $m->from('Fitnessbook-support@no-reply.org', 'Fitnessbook-support@no-reply.org');

            $m->to($userMail)->subject('Account verification');
        });





    } 

    public function verifyEmailFirst()
    {
        return view('verifyEmailFirst');
    }

    public function sendEmailDone($email,$verifyToken)
    {
        $user = User::where(['email'=>$email,'verify_token'=>$verifyToken])->first();
        
        if($user)
        {
             $data = ['reg'=>'not important', 'test'=>'no important'];
          


            $userMailPersonal = 'bodi_bodileg@yahoo.com';
           // mail for know about new user
             Mail::send('email.new_user', $data, function ($m) use ( $userMailPersonal) {
            $m->from('Fitnessbook-support@no-reply.org', 'Fitnessbook-support@no-reply.org');

            $m->to($userMailPersonal)->subject('New user');
          
             });



            user::where(['email'=>$email,'verify_token'=>$verifyToken])->update(['status'=>'1','verify_token'=>NULL]);
            return redirect('/login')->with('verification',true);
        }
        else
        {
              return redirect('/login');
        }
    }
}
