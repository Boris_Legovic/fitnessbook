<?php
namespace App\Http\Controllers;
use App\Message;
use App\Events\MessageSent;
use Illuminate\Support\Facades\DB;
use Session;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ChatsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */

    public $secondUser;



    /************************HERE START NEW IMPLEMNETATION *******/
    /************************HERE START NEW IMPLEMNETATION *******/
    /************************HERE START NEW IMPLEMNETATION *******/
    /************************HERE START NEW IMPLEMNETATION *******/
    /************************HERE START NEW IMPLEMNETATION *******/


    public function messages()
    {

        // DB::table('conversation')->insert([
        //           'user_one' => $userToSend,
        //           'user_two' => Auth::user()->id,
                 
        //         ]);
        return view('messages');
    }






    public function index(Request $req)
    {
        
         // echo $this->secondUser;
        // Session::flash('second_chat_id', $req->id); 
        // $req->session()->put('second_chat_id', $req->id);
        // echo intval(Session::get('second_chat_id'));
        // $this->secondUser = intval(Session::get('second_chat_id'));
         return view('chat');
    }
    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {

       //  $id = Auth::user()->id;
       //  $second = 0;
         
       //  $second =  intval(Session::get('second_chat_id'));

       // return Message::with('user')
       //   ->where('user_id',$id)
       //  ->where('user_id_second', 2)
       //  ->get();
        return Message::with('user')->get();
    }
    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();
        // $second = intval( $request->session()->get('second_chat_id'));
        $message = $user->messages()->create
        ([
            'message' => $request->input('message')
        ]);

      

        broadcast(new MessageSent($user, $message))->toOthers();
        return ['status' => 'Message Sent!'];
    }







}