<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;
use Auth;



class CoachListController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('training_list');
    }


      public function showCoachingDetail(Request $req)
    {
        $coachID = $req->coachId;
    
        $data = DB::table('user_options')->where('user_id',$coachID)->get();

        $onlineCoachings = DB::table('online_coachings')->where('coach_id',$coachID)->where('user_customer_id',Auth::user()->id)->orderBy('end_period','desc')->get();

        $userAlreadyCoaching = false;
       

        if(count($onlineCoachings)>0)
        {
            //user already have online program or he had it in past but expired
             $time = Carbon::now();
             $time = $time->toDateTimeString();

            if(strtotime($onlineCoachings[0]->end_period) > strtotime($time))
            {
                    $userAlreadyCoaching = true;
            }

             
        }
        
           return view('coaching_detail_view')->with('data',$data)->with('userAlreadyCoaching',$userAlreadyCoaching)->with('onlineCoachings',$onlineCoachings);
   }

   public function clientProgress()
    {
        $id = Auth::user()->id;
        $time = Carbon::now();
        $time = $time->toDateTimeString();
        
        $data = DB::table('online_coachings')->where('coach_id',$id)
        ->join('meassurement_data','online_coachings.user_customer_id','=','meassurement_data.user_id') // if user dont have any meassurek, we dont show user, this is a problem and must be changed
        ->join('users','online_coachings.user_customer_id','=','users.id')
        ->where('meassurement_data.date_of_meassure','>=','online_coachings.start_period')
        ->where('online_coachings.end_period','>=',$time)
        ->orderBy('online_coachings.user_customer_id','desc')
        ->orderBy('meassurement_data.date_of_meassure','asc')
        ->get();


        // more values in this query, now we have to filter
       
        
        $data1 = DB::table('online_coachings')->where('coach_id',$id)
          ->join('users','online_coachings.user_customer_id','=','users.id')
        // ->join('meassurement_data','online_coachings.user_customer_id','=','meassurement_data.user_id') // if user dont have any meassurek, we dont show user, this is a problem and must be changed
        ->where('online_coachings.end_period','>=',$time)
        ->orderBy('online_coachings.user_customer_id','desc')
        ->get();

        
         

         $arrayOfEmptyData = array();

         for($i = 0; $i<count($data1); $i++)
         {
             $counter = 0;
                for($j = 0; $j<count($data); $j++)
                {

                   
                    if($data1[$i]->user_customer_id == $data[$j]->user_customer_id)
                    {
                        $counter += 1;
                        break;
                    }
                }

                if($counter == 0)
                {
                    array_push($arrayOfEmptyData,$data1[$i]);
                    
                }
                
                 
         }
         

        


         return view('client_progress_visualisation')
         ->with('data',$data)->with('encoded',json_encode($data))
         ->with('encodedEmpty',json_encode($arrayOfEmptyData))
         ->with('emptyData',$arrayOfEmptyData);
   }
     

}