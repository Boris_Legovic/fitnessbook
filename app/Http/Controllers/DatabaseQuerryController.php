<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use App\UserOptions;
use Illuminate\Support\Facades\Storage;
use File;




class DatabaseQuerryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function addNewTraining()
    {
        return view('ajaxRequest');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

   


    public function addNewTrainingPost(Request $req)
    {
       $trainingName = $req->input(['trainingName']);
       $userId = Auth::user()->id;
       $id = DB::table('training')->insertGetId(
                 ['training_name' => $trainingName, 'user_id' => $userId]
            );
       $dataToInsert = array();

       DB::table('user_own_training')->insert(
                 ['training_id' => $id, 'user_id' => Auth::user()->id]
            );

       $trainingId = $req->input(['trainingId']);
       $sets = $req->input(['sets']);
       $exercizeNames = $req->input(['exercizeNames']);
       $reps = $req->input(['reps']);
       $comments = $req->input(['comments']);
    
      for($i=0; $i<count($exercizeNames);$i++)
      {
          $exercizeId = DB::table('exercize')->insertGetId(
                 ['exercize_name' => $exercizeNames[$i], 'comment' => $comments[$i],'sets' => $sets[$i],'training_id' => $id]
            );
         for($j=0; $j<count($reps[$i]);$j++)
         {
            DB::table('repsRange')->insert(['repRange'=>$reps[$i][$j], 'exercize_id'=>$exercizeId]); 
          }
       }
             echo "Success!";
    }

    
    public function deleteTraining(Request $req)
    {

        DB::table('user_own_training')->where('training_id',$req->id)->where('user_id',Auth::user()->id)->delete();

        $result = DB::table('user_own_training')->where('training_id',$req->id)->get();

        if(count($result) < 1)
        {
                    DB::table('training')->where('id',$req->id)->delete();

        }

    }

    public function loadClientList()
    {
       $clients =  DB::table('online_coachings')
       ->join('users', 'online_coachings.user_customer_id','=','users.id')
       ->where('coach_id',Auth::user()->id)->get();

            return view('inc.modals.clientList')->with('clients',$clients);
    }


     public function sendTraining(Request $req)
    {

         DB::table('user_own_training')->insert(
                 ['training_id' => $req->id, 'user_id' => $req->customerId]
            );

                return 'sucess!';

    }




    public function loadTrainingList(Request $req)
    {
      $id = Auth::user()->id;
      $buttonPressedId = $req->buttonPressedId;
      
        $trainingData = DB::table('user_own_training') ->join('training','user_own_training.training_id','=','training.id')
                            ->select('user_own_training.training_id','training.training_name','training.id')
                            ->where('user_own_training.user_id',Auth::user()->id)->get();


    
     
       return view('inc.modals.show_trainings_modal')->with('trainingData',$trainingData)->with('buttonPressedId',$buttonPressedId);
       // ovaj view koji ce se pokazat mora znat koji je gum klikunut za redirect sa liste
      
    }

    public function loadTrainingDataInsert(Request $req)
    {
          $id = $req->input(['id']);
          $trainingData = DB::table('exercize')
          ->select('exercize_name', 'sets','id','training_id')
          ->where('training_id', '=', $id)->get(); 

          $weightUnit = DB::table('user_options')
          ->select('weight_unit')
          ->where('user_id', '=', Auth::user()->id)->get();

           /// start here


                //first get last training id




                    $arrayOfData = array();
                    $insideData = array();
                    $dateAndData = array();
                    $trainingId = $id;
                    $arrayOfId = array();
                    $allExercizeTest = array();

                          $weightUnitNew = DB::table('user_options')
                          ->select('weight_unit')
                          ->where('user_id', '=', Auth::user()->id)->get(); 


                           $dateId = DB::table('exercizeDate')
                          ->select('id')
                          ->where('trainingId', '=', $id)->max('id'); 

                           $weightUnitNew = $weightUnit[0]->weight_unit;

                  
                  if($dateId != NULL)
                  {
                    
                      $trainingData = DB::table('exercize')
                      ->where('training_id', '=', $trainingId)->get(); 

                       foreach ($trainingData as $data )
                       {
                          
                          $detailArray = array();
                          
                          array_push($detailArray,$data->id); // exercize name

                          array_push($detailArray,$data->exercize_name);

                          array_push($arrayOfId,$detailArray); // id 
                       }

                      // echo 'checkFirst ' . var_dump($arrayOfId);

                       foreach($arrayOfId as $data)
                       {
                           $id = $data[0];
                           $exercizeName = $data[1];
                                                  
                            $detailTrainingData = DB::table('reps')
                            ->where('reps.exercize_id','=',$id)
                            ->where('date_id',$dateId)
                            ->get();
                             // this insert

                            foreach ($detailTrainingData as $row)
                            {

                                if($weightUnitNew == 'Lbs') // convert to kg
                                {
                                   $row->kg = $row->kg * 2.20462262;
                                }

                               
                                array_push($allExercizeTest, $row->kg );
                                 array_push($allExercizeTest, $row->reps_done );
                                 
                              

                            }
                           
                        }
                  }

                     
       

         return view('inc.modals.insertTrainingDataModal')->with('trainingData',$trainingData)->with('weightUnit',$weightUnit[0]->weight_unit)->with('data',$allExercizeTest);
     }

     public function addNewTrainingDataToDB(Request $req)
     {
        $trainingDate = $req->input(['trainingDate']);
        

          $weightUnit = DB::table('user_options')
          ->select('weight_unit')
          ->where('user_id', '=', Auth::user()->id)->get(); 

           $weightUnit = $weightUnit[0]->weight_unit;

           $format = 'Y-m-d H:i:s';
            $d = DateTime::createFromFormat($format, $trainingDate);
          
           $isValidate =  $d && $d->format($format) == $trainingDate;
          
          if($isValidate)
          {
             $trainingId = $req->input(['trainingId']);
             $trainingData = $req->input(['trainingData']);

             $last_id = DB::table('exercizeDate')->insertGetId(['trainingDate'=>$trainingDate, 'trainingId'=>$trainingId]); 

             $exercizesNumber = count($trainingData);
             $dataToInsertInDB = array();
             $singleRowContainer  = array();
            
            for($i = 0; $i<$exercizesNumber; $i++)
            {
                for($j = 0; $j<count($trainingData[$i][1]); $j++)
                {
                    if($weightUnit == 'Lbs') // convert to kg
                    {
                        $trainingData[$i][1][$j][0] = $trainingData[$i][1][$j][0] * 0.45359237;
                    }

                    $singleRow = ['exercize_id'=>$trainingData[$i][0],'reps_done'=>$trainingData[$i][1][$j][1],'kg'=>$trainingData[$i][1][$j][0],'date_id'=>$last_id];
                    
                     array_push($singleRowContainer,$singleRow); 
                }
            } 

            DB::table('reps')->insert
            (
                 $singleRowContainer   
            ); 

            echo "Sucess!";
          }
          else
          {
              echo "false";
          }


        
     }

     function loadTrainingProgress(Request $req)
     {
                    $arrayOfData = array();
                    $insideData = array();
                    $dateAndData = array();
                    $trainingId = $req->id;
                    $arrayOfId = array();

                          $weightUnit = DB::table('user_options')
                          ->select('weight_unit')
                          ->where('user_id', '=', Auth::user()->id)->get(); 

                           $weightUnit = $weightUnit[0]->weight_unit;

                  
                    
                      $trainingData = DB::table('exercize')
                      ->where('training_id', '=', $trainingId)->get(); 

                       foreach ($trainingData as $data )
                       {
                          
                          $detailArray = array();
                          
                          array_push($detailArray,$data->id);
                          array_push($detailArray,$data->exercize_name);


                          array_push($arrayOfId,$detailArray);
                       }

                       // echo 'checkFirst ' . var_dump($arrayOfId);

                       foreach($arrayOfId as $data)
                       {
                           $id = $data[0];
                           $exercizeName = $data[1];
                                                  
                            $detailTrainingData = DB::table('reps') ->join('exercizeDate','reps.date_id','=','exercizeDate.id')
                            ->select('reps.*','exercizeDate.trainingDate')
                            ->where('reps.exercize_id','=',$id)->get();

                            foreach ($detailTrainingData as $row)
                            {

                                if($weightUnit == 'Lbs') // convert to kg
                                {
                                   $row->kg = $row->kg * 2.20462262;
                                }


                                $exercizes = array($row->trainingDate,$row->reps_done,$row->kg);
                                array_push($insideData, $exercizes );
                            }
                            $complete = array($exercizeName,$insideData);
                            $insideData = array();    
                            array_push($dateAndData,$complete);
                        }

                         // return $dateAndData;
                      return [json_encode($dateAndData),$weightUnit]; 

            }

         function loadAddTrainingModal(Request $req)
         {
            return view('inc.modals.add_training_modal');
         }  


         function addNewMeassurementToDB(Request $req)
         {

            $user_id =  $req->input(['userId']);
            $date = $req->input(['date']);
            $size = $req->input(['size']);
            $bodyPart = $req->input(['selectedBodyPart']);
            $unit = $req->input(['unit']);

           if($bodyPart == 'weight')
           {
              if($unit == 'second')
              {
                $unit = 'Lbs';
              }
              else
              {
                  $unit = 'Kg';
              }

                 DB::table('user_options')
                     ->where('user_id',$user_id)
                     ->update(['weight_unit' => $unit ]);
          }
          else
          {
            if($unit == 'second')
              {
                $unit = 'Inch';
              }
              else
              {
                  $unit = 'Cm';
              }

                 DB::table('user_options')
                     ->where('user_id',$user_id)
                     ->update(['size_unit' => $unit ]);
          }
           


       

          



            DB::table('meassurement_data')->insert
            (
                 ['user_id' =>  $user_id,'date_of_meassure' => $date,'size_cm_or_kg' => $size,'exercize_name_or_weight' => $bodyPart]
            );



           $arrayOfData = array();
           $exercizeName = ['Chest','Right_biceps','Right_forearm', 'Waist','Right_glut','Right_calf','Shoulders','Left_biceps','Left_forearm','Hip', 'Left_glut','Left_calf','Neck','Weight','Height'];
           for($i = 0; $i<15;$i++)
           {
                $data = DB::table('meassurement_data') ->select('date_of_meassure', 'size_cm_or_kg')
                      ->where('user_id', '=', $user_id)
                      ->where('exercize_name_or_weight', '=', $exercizeName[$i])
                      ->orderBy('date_of_meassure', 'desc') ->first();
                      
                    array_push($arrayOfData,$data);

            }

            $neck = 0;
            $waist = 0;
            $height = 0;
            $weight = 0;
            $hip = 0;

             if($arrayOfData[9] != null)
             {
                $hip = $arrayOfData[9]->size_cm_or_kg;
             }

             if($arrayOfData[12] != null)
             {
                $neck = $arrayOfData[12]->size_cm_or_kg;

             }

             if($arrayOfData[3] != null)
             {

                $waist = $arrayOfData[3]->size_cm_or_kg;
             }

             if($arrayOfData[14] != null)
             {
                $height = $arrayOfData[14]->size_cm_or_kg;

             }

             if($arrayOfData[13] != null)
             {
                 $weight = $arrayOfData[13]->size_cm_or_kg;

             }

           // TODO check if male or female
             

               $weightCalc = 0;
                                            //waist-neck
               $shouldUpdateStats = false;

             if(Auth::user()->gender == 'male')
              {
                    if($waist != NULL && $neck !=NULL && $height!=NULL  )
                    {
                        $weightCalc = 495/(1.0324 - 0.19077 * log10($waist-$neck)  + 0.15456*log10($height)) - 450;

                         $shouldUpdateStats = true;
                     }
               

              }
              else
              {    
                    if($waist != NULL && $neck !=NULL && $height!=NULL && $hip != NULL  )
                    {
                     $weightCalc = 495/(1.29579 - 0.35004 * log10($waist+$hip-$neck)  + 0.22100*log10($height)) - 450;

                      $shouldUpdateStats = true;
                   }
              }
           
            
           

            $height_cm = 0;
          
            $bmi = 0;

            $FM = round(($weightCalc/100) * $weight); // fat mass

            

            $LM = $weight - $FM; // lean mass

            $m_cub = $height/100;

            $m_cub = $m_cub * $m_cub;

            if($weight > 0 && $m_cub > 0 )
            {
                $bmi = $weight/$m_cub; 
            }

           // this is bmi

             
              

            $muscle_ratio = $LM - $FM;


            if(is_nan($LM))
            {
              $LM = 0;
            }  

            if(is_nan($FM))
            {
              $FM = 0;
            } 

             if(is_nan($muscle_ratio))
            {
               $muscle_ratio = 0;
            }  


              if(is_nan($bmi))
            {
               $bmi = 0;
            }  

            

              $userExcist = DB::table('body_rank')->where('user_id',$user_id)->get();

              if($shouldUpdateStats)
              {


                  if(count($userExcist)>0) // update 
                  {

                       DB::table('body_rank')
                         ->where('user_id',$user_id)
                         ->update(
                            ['user_id' => Auth::user()->id, 'body_fat' => $FM,'lean_mass'=>$LM,'muscle_percentage'=>$muscle_ratio,'BMI'=>$bmi]
                         );
                    
                  }
                  else // insert
                  {
                      DB::table('body_rank')->insert(
                            ['user_id' => Auth::user()->id, 'body_fat' => $FM,'lean_mass'=>$LM,'muscle_percentage'=>$muscle_ratio,'BMI'=>$bmi]
                       );
                  }

                }

                 
                  return "Sucess!";

     }  

         // load data meassurements

         function loadMeassurementData(Request $req)
         {

             $id = Auth::user()->id;
              $userMeassureTypeValue = DB::table('user_options')->select('weight_unit','size_unit')
              ->where('user_id',$id)->get();

              $arrayOfDataGraphs = array();
              $exercizeName = ['Chest','Right_biceps','Right_forearm', 'Waist','Right_glut','Right_calf','Shoulders','Left_biceps','Left_forearm','Hip', 'Left_glut','Left_calf','Neck','Weight','Height'];

              for($i = 0; $i<15;$i++)
              {
                  $data = DB::table('meassurement_data') ->select('date_of_meassure', 'size_cm_or_kg')
                  ->where('user_id', '=', $id)
                  ->where('exercize_name_or_weight', '=', $exercizeName[$i])
                  ->orderBy('date_of_meassure', 'asc') ->get();
                   $encodeData = $data->toArray();
                  array_push($arrayOfDataGraphs,$encodeData);
       
              }

                  $data = DB::table('body_rank')
                  ->where('user_id', '=', $id)->get();

                      $result = [0,0];

                    if(count($data)>0)
                    {
                      $result = [$data[0]->body_fat,$data[0]->lean_mass];
                    }

                 $res = [$arrayOfDataGraphs,$result,$userMeassureTypeValue[0]];

                 return  $res;
         }

   public function loadBodyRank()
   {
        $isThereData = false;
         $data = DB::table('body_rank')
            ->orderBy('muscle_percentage', 'asc')->get();

             $sum_people = DB::table('body_rank')->count();
            
             $counter = 0;

            for($i = 0; $i<count($data);$i++)
            {     
                    $counter ++;

                if($data[$i]->user_id ==  Auth::user()->id )
                {
                    $isThereData = true;
                    break;
                }
            }



           
            $result = [$counter,$sum_people,$isThereData];

            return $result;

   }      


   public function loadKcal(Request $req)
      {
         $id = $req->input(['id']); 
        $kcalData = DB::table('kcal')
        ->select('kcal', 'date')
        ->where('user_id', '=', $id)->get(); 
         
         return json_encode($kcalData->toArray());
      }

      public function insertKcal(Request $req)
      {
           $id = $req->input(['id']);
           $kcalDate = $req->input(['kcalDate']);
           $kcalVal = $req->input(['kcalVal']);

            $format = 'Y-m-d H:i:s';
            $d = DateTime::createFromFormat($format, $kcalDate);
          
            $isValidate =  $d && $d->format($format) == $kcalDate;
          
          if($isValidate)
          {

             DB::table('kcal')->insert(
                   ['user_id' => $id, 'kcal' => $kcalVal,'date'=>$kcalDate]
              );

             return "Success!";
         }
         else
         {
              return "Error";
         }
          
      }


     public function loginUserWithFacebook(Request $req)
     {

        $name = $req->name;
        $lastname =  $req->lastname;
        $email =  $req->email;
        $picture = $req->picture;


        if(!isset($req->email))
        {
          $email = "no_email@" . $name . $lastname;
        }

       $result = DB::table('users')->where('email',$email)->get();

        

       if(count($result)>0) // user already registered in app
       {
            
            
              
            

        //   $profilePath = Storage::disk('do_spaces')->putFile('public/' . $result[0]->id ,$img,'public');
         

         


          if($result[0]->gender == 'not_selected') // user first time login and he dont have set gender and trainer, we have to ask him 
          {
             

           


             // DB::table('users')
             //             ->where('id',$result[0]->id)
             //             ->update(
             //                ['profile_picture' =>$profilePath ]
             //              );

                      $req->session()->put('user_id', $result[0]->id);

             // return ["display_options",$result[0]->id];
                         return "display_options";
          } 
          else
          {

              // DB::table('users')
              //            ->where('id',$result[0]->id)
              //            ->update(
              //               ['profile_picture' =>$profilePath ]
              //            );

              //return ["log_in",$result[0]->id];
                         $req->session()->put('user_id', $result[0]->id);

                return "log_in";         

          } // user already set trainer and loging so he is ready to get into app


       }
       else
       {

          $passString = 'fb';

         for($i = 0;  $i<8;$i++)
         {
            $passString = $passString . rand();
         }

         

           // user never logged in so we have to insert his data to db
          $id = DB::table('users')->insertGetId(
                ['name' => $name, 'lastname' =>$lastname ,'gender'=>'not_selected','type'=>'not_selected','email'=>$email,'password'=>$passString,'status'=>1,'profile_picture'=>$picture]
           );

           
           
          
           //  $profilePath = Storage::disk('do_spaces')->putFile('public/' . $id , file_get_contents($picture,true),'public');

               // DB::table('users')
               //           ->where('id',$id)
               //           ->update(
               //              ['profile_picture' =>$profilePath ]
               //           );


           // return ['display_options',$result[0]->user_id];
              $req->session()->put('user_id', $id);

           return "display_options";
       }
    
        

     }


     public function enterApp()
     {

          $id = session()->get('user_id');

        Auth::loginUsingId($id);

        return view('home');

     }


     public function checkIfUserHaveStatusVerified(Request $req)
     {
         $result = DB::table('users')->select('status')->where('email',$req->email)->get();
         if(count($result) > 0)
         {
            if($result[0]->status == 0)
            {
                return '0';
            }
            else
            {
                return '1';
            }

            return '1';
         }
         else
         {
            return '1';
         }
     }
     

     public function goToDetailFacebookSelection()
     {
            return view('facebook_login_details_enter');

     }

    public function enterAppWithDetails(Request $req)
    {
        $gender = $req->gender;
        $type = $req->type;

        $id = session()->get('user_id');

        DB::table('users')
                    ->where('id',$id)
                    ->update(
                       ['gender' =>$gender, 'type'=>$type]
                     );


          $coaching = "No";

        if($type == 'trainer')
        {
            $coaching = "Yes";
        }

        UserOptions::create([
            'user_id' =>$id,
            'birthdate' => NULL,
            'max_number_of_pictures' => "15",
            'online_coaching_period' => "1", 
            'coaching_price' => 0,
             'currency' => '€',
            'weight_unit' =>'kg',
            'size_unit' =>'cm',
            'onlineCoaching' =>$coaching,
        ]);           

        Auth::loginUsingId($id);
        
         return view('home');

    }

}