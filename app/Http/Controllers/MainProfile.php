<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MainProfile extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showPicturesView()
    { 
          $maxNumberOfPicsExcided = false;

           $countPictures = DB::table('user_pictures')->where('user_id',Auth::user()->id)->count();

              $license = DB::table('user_options')->select('max_number_of_pictures')->where('user_id',Auth::user()->id) ->get();

              if($countPictures > $license[0]->max_number_of_pictures)
              {

                   $maxNumberOfPicsExcided = true;
              }

         $owner_or_uploader_id =  Auth::user()->id;

         $profile = DB::table('users')->select('profile_picture')
         ->where('id',$owner_or_uploader_id)->get();

          $cover = DB::table('users')->select('cover_picture')
         ->where('id',$owner_or_uploader_id)->get();

         $allImages = DB::table('user_pictures')->select('image')
         ->where('user_id',$owner_or_uploader_id)->get();

          $personalData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.id', $owner_or_uploader_id )
          ->get();

         return view('pictures_view')->with('cover',$cover[0]->cover_picture)->with('profile',$profile[0]->profile_picture)->with('allPictures',$allImages)->with('isOwnerOfProfile',true)
         ->with('personalData',$personalData)->with('maxNumberOfPicsExcided',$maxNumberOfPicsExcided)->with('picturesToShow',$license[0]->max_number_of_pictures);
    }
    
    public function uploadCoverPicture(Request $request)
    {

        $image = $request->file('cover_image');
        $owner_or_uploader_id =  Auth::user()->id;
        
        $coverPath = Storage::disk('do_spaces')->putFile('public/' . $owner_or_uploader_id ,$image,'public');
       
        // $coverPath = substr($coverPath,7);


        //delete previous cover to free mb on server
            $coverToDelete =  DB::table('users')
            ->where('id', $owner_or_uploader_id)->get();

              if($coverToDelete[0]->cover_picture!=null)
              {
                Storage::disk('do_spaces')->delete('public/'. $coverToDelete[0]->cover_picture);
              }


           

        DB::table('users')
            ->where('id', $owner_or_uploader_id)
            ->update(['cover_picture' => $coverPath]);

        $cover = DB::table('users')->select('cover_picture')
         ->where('id',$owner_or_uploader_id)->get();

         $profile = DB::table('users')->select('profile_picture')
         ->where('id',$owner_or_uploader_id)->get();

         $allImages = DB::table('user_pictures')->select('image')
         ->where('user_id',$owner_or_uploader_id)->get();

         $personalData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.id', $owner_or_uploader_id )
          ->get();


        // we must return all photos from user

        // return view('pictures_view')->with('cover',$cover)->with('profile',$profile[0]->profile_picture)->with('allPictures',$allImages);
          return redirect('showPicturesView')->with('cover',$cover[0]->cover_picture)->with('profile',$profile[0]->profile_picture)->with('allPictures',$allImages)
           ->with('personalData',$personalData);
    }



     public function uploadProfilePicture(Request $request)
    {

       
        $image = $request->file('profile_image');
        $owner_or_uploader_id =  Auth::user()->id;
        
        $imagePath = 'https://fitnessbookspace.ams3.digitaloceanspaces.com/' . Storage::disk('do_spaces')->putFile('public/' . $owner_or_uploader_id,$image,'public');

        // $imageName = substr($imagePath,7);
                
             $profileToDelete =  DB::table('users')
            ->where('id', $owner_or_uploader_id)->get();

               if($profileToDelete[0]->profile_picture!=null)
              {
                             Storage::disk('do_spaces')->delete('public/'. $profileToDelete[0]->profile_picture);

              }

       DB::table('users')
            ->where('id', $owner_or_uploader_id)
            ->update(['profile_picture' => $imagePath]);

          $profile = DB::table('users')->select('profile_picture')
         ->where('id',$owner_or_uploader_id)->get();

          $cover = DB::table('users')->select('cover_picture')
         ->where('id',$owner_or_uploader_id)->get();

         $allImages = DB::table('user_pictures')->select('image')
         ->where('user_id',$owner_or_uploader_id)->get();

            $personalData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.id', $owner_or_uploader_id )
          ->get();


             return redirect('showPicturesView')->with('cover',$cover[0]->cover_picture)->with('profile',$profile[0]->profile_picture)->with('allPictures',$allImages)
              ->with('personalData',$personalData);
        
        // return view('pictures_view')->with('trainingImagePath',$imagePath);

    }
     public function uploadNormalPicture(Request $request)
    {
        $maxNumberOfPicsExcided = false;

        $image = $request->file('normal_image');
        $owner_or_uploader_id =  Auth::user()->id;
        
        $license = DB::table('user_options')->select('max_number_of_pictures')->where('user_id',Auth::user()->id) ->get();

        
        if($request->picture != null) // we just update the pic
        {
              
                 $imagePath = Storage::disk('do_spaces')->putFile('public/' . $owner_or_uploader_id ,$image,'public');
       
                 // $imageName = substr($imagePath,7);
                  $imageName = $imagePath;

                DB::table('user_pictures')->where('image',$request->picture)
                ->update(
                ['user_id' => $owner_or_uploader_id, 'image' => $imagePath]
                 );
            
              Storage::disk('do_spaces')->delete($request->picture);


              
             
        }
        else
        {


              $countPictures = DB::table('user_pictures')->where('user_id',Auth::user()->id)->count();

             

              


              if($countPictures < $license[0]->max_number_of_pictures)
              {
                       $imagePath = Storage::disk('do_spaces')->putFile('public/' . $owner_or_uploader_id ,$image,'public');
       
                       // $imageName = substr($imagePath,7);
                       $imageName = $imagePath;

                       DB::table('user_pictures')->insert(
                      ['user_id' => $owner_or_uploader_id, 'image' => $imagePath]
                     );
              }
              else
              {
                  $maxNumberOfPicsExcided = true;
              }
             
             

        }

         $profile = DB::table('users')->select('profile_picture')
         ->where('id',$owner_or_uploader_id)->get();

          $cover = DB::table('users')->select('cover_picture')
         ->where('id',$owner_or_uploader_id)->get();

         $allImages = DB::table('user_pictures')->select('image')
         ->where('user_id',$owner_or_uploader_id)->get();


             $personalData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.id', $owner_or_uploader_id )
          ->get();

         return redirect('showPicturesView')->with('cover',$cover[0]->cover_picture)->with('profile',$profile[0]->profile_picture)->with('allPictures',$allImages)
            ->with('personalData',$personalData)->with('maxNumberOfPicsExcided',$maxNumberOfPicsExcided)->with('picturesToShow',$license[0]->max_number_of_pictures);;
            ;
    }


    public function loadTrainerProfile(Request $request)
    {
           $owner_or_uploader_id =  $request->id;
         $idOfLoggedUser = Auth::user()->id;
         $isOwnerOfProfile = false;

         if($owner_or_uploader_id == $idOfLoggedUser)
         {
                $isOwnerOfProfile  = true;
         }

          $maxNumberOfPicsExcided = false;

          $countPictures = DB::table('user_pictures')->where('user_id',$owner_or_uploader_id)->count();

          $license = DB::table('user_options')->select('max_number_of_pictures')->where('user_id',$owner_or_uploader_id) ->get();

         if($countPictures > $license[0]->max_number_of_pictures)
         {

              $maxNumberOfPicsExcided = true;
         }

        
          $profile = DB::table('users')->select('profile_picture')
         ->where('id',$owner_or_uploader_id)->get();

          $cover = DB::table('users')->select('cover_picture')
         ->where('id',$owner_or_uploader_id)->get();

         $allImages = DB::table('user_pictures')->select('image')
         ->where('user_id',$owner_or_uploader_id)->get();

          $personalData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.id', $owner_or_uploader_id )
          ->get();

         return view('pictures_view')->with('cover',$cover[0]->cover_picture)->with('profile',$profile[0]->profile_picture)->with('allPictures',$allImages)->with('isOwnerOfProfile',$isOwnerOfProfile )->with('profile_id',$request->id)->with('personalData',$personalData)->with('picturesToShow',$license[0]->max_number_of_pictures);;
    }


  public function deletePicture(Request $request)
  {
           DB::table('user_pictures')
            ->where('user_id', Auth::user()->id)
             ->where('image',$request->picture)
            ->delete(); 

             Storage::disk('do_spaces')->delete($request->picture);
            return redirect('showPicturesView');
  }


    
}