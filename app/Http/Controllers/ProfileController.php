<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\friendships;
use App\notifcations;
use App\User;
use Carbon\Carbon;
use App\Events\MessageSent;
class ProfileController extends Controller {


    public function resetPassword()
    {
        return view('auth.passwords.reset');
    }
 
 
 public function trainerRequestList()
    {
        
          

          $coachingData = DB::table('request_coach')
                 ->join('users','users.id','request_coach.user_that_requested_id')
                  ->where('trainer_to_recive_request_id',Auth::user()->id)
                  ->where('aprove_status',0)
                  
                  ->get();

          return view('inc.requestCoachList')->with('data',$coachingData);
    }

 


    public function notificationList()
    {
        
          $id_of_notification =  $_GET['id_of_notification'];  
           DB::table('online_coachings')->where('id',  $id_of_notification)->update(['read_status' => 1]);
           

          $coachingData = DB::table('online_coachings')
                  ->join('users','users.id','online_coachings.user_customer_id')
                  ->where('online_coachings.coach_id',Auth::user()->id)
                  ->orderBy('start_period', 'asc')
                  ->get();

          return   view('notificationList')->with('data',$coachingData);
    }


    
      public function setPass(Request $request)
      {
        $email = $request->email;
        $pass = $request->pass;
        $cpass = $request->confrim_pass;
      if($pass == $cpass)
      {
        //update the pass for this user
          DB::table('users')->where('email',$email)->update(['password' =>bcrypt($pass)]);
          return back();
      }
      else
      {
        echo "passwords not matched";
      }
    }
    public function editProfileForm() 
    {
        return view('profile.editProfile')->with('data', Auth::user()->profile);
    }
    public function updateProfile(Request $request) {
        $user_id = Auth::user()->id;
        DB::table('profiles')->where('user_id', $user_id)->update($request->except('_token'));
        return back();
    }
    public function findFriends() {
        $uid = Auth::user()->id;
        $allUsers = DB::table('profiles')
        ->leftJoin('users', 'users.id', '=', 'profiles.user_id')
        ->where('users.id', '!=', $uid)->get();
        return view('profile.findFriends', compact('allUsers'));
    }
    public function sendRequest($id) {
        Auth::user()->addFriend($id);
        return back();
    }
    public function requests() {
        $uid = Auth::user()->id;
        $FriendRequests = DB::table('friendships')
                        ->rightJoin('users', 'users.id', '=', 'friendships.requester')
                        ->where('status', '=', Null)
                        ->where('friendships.user_requested', '=', $uid)->get();
        return view('profile.requests', compact('FriendRequests'));
    }
    public function accept($name, $id) {
        $uid = Auth::user()->id;
        $checkRequest = friendships::where('requester', $id)
                ->where('user_requested', $uid)
                ->first();
        if ($checkRequest) {
            // echo "yes, update here";
            $updateFriendship = DB::table('friendships')
                    ->where('user_requested', $uid)
                    ->where('requester', $id)
                    ->update(['status' => 1]);
            $notifcations = new notifcations;
            $notifcations->note = 'accepted your friend request';
            $notifcations->user_hero = $id; // who is accepting my request
            $notifcations->user_logged = Auth::user()->id; // me
            $notifcations->status = '1'; // unread notifications
            $notifcations->save();
            if ($notifcations) {
                return back()->with('msg', 'You are now Friend with ' . $name);
            }
        } else {
            return back()->with('msg', 'You are now Friend with this user');
        }
    }
    public function friends() {
        $uid = Auth::user()->id;
        $friends1 = DB::table('friendships')
                ->leftJoin('users', 'users.id', 'friendships.user_requested') // who is not loggedin but send request to
                ->where('status', 1)
                ->where('requester', $uid) // who is loggedin
                ->get();
        //dd($friends1);
        $friends2 = DB::table('friendships')
                ->leftJoin('users', 'users.id', 'friendships.requester')
                ->where('status', 1)
                ->where('user_requested', $uid)
                ->get();
        $friends = array_merge($friends1->toArray(), $friends2->toArray());
        return view('profile.friends', compact('friends'));
    }
    public function requestRemove($id) {
        DB::table('friendships')
                ->where('user_requested', Auth::user()->id)
                ->where('requester', $id)
                ->delete();
        return back()->with('msg', 'Request has been deleted');
    }
    public function notifications($id) {
         $uid = Auth::user()->id;
        $notes = DB::table('notifcations')
                ->leftJoin('users', 'users.id', 'notifcations.user_logged')
                ->where('notifcations.id', $id)
                ->where('user_hero', $uid)
                ->orderBy('notifcations.created_at', 'desc')
                ->get();
        $updateNoti = DB::table('notifcations')
                     ->where('notifcations.id', $id)
                    ->update(['status' => 0]);
       return view('profile.notifcations', compact('notes'));
    }

    public function idOfUserToRecive(Request $request)
    {
        $id = $request->session()->get('user_to_send_message_id');
        $myID = Auth::user()->id;



         $checkCon1 = DB::table('conversation')->where('user_one',$myID)
        ->where('user_two',$id)->get(); // if loggedin user started conversation
        $checkCon2 = DB::table('conversation')->where('user_two',$myID)
        ->where('user_one',$id)->get(); // if loggedin recviced message first

       //  session()->forget('user_to_send_message_id');    

        $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
        
              if(count($allCons) > 0)
              {
                return $allCons[0]->id;
              }
               else
              {
                 return '' ;
              }

        

         
    }

    public function updateMessageNotification(Request $request)
     {
          $conversationId = $request->updateNotificationsOfUserId;

          $unreadMessagesWhereUserIsOne =  DB::table('conversation')->where('user_one',Auth::user()->id)
          ->where('user_one_status',1)
          ->get();
          $unreadMessagesWhereUserIsTwo =  DB::table('conversation')->where('user_two',Auth::user()->id)
          ->where('user_two_status',1)
          ->get();

          $allConversations = array_merge( $unreadMessagesWhereUserIsOne->toArray(),$unreadMessagesWhereUserIsTwo->toArray());

          return count($allConversations);
      }  

     public function updateNotification (Request $request)
     {

         $userToGetNotification = $request->updateNotificationsOfUserId;

          $countOfUnread =  DB::table('online_coachings')->where('coach_id',Auth::user()->id)
          ->where('read_status',0)
          ->count();

           $countOfUnreadRequests =  DB::table('request_coach')->where('trainer_to_recive_request_id',Auth::user()->id)
          ->where('read_status',0)
          ->count();
          

         

          return count($countOfUnread+$countOfUnreadRequests);

     }

      public function updateNotificationRequest (Request $request)
     {

         $userToGetNotification = $request->updateNotificationsOfUserId;

         

           $countOfUnreadRequests =  DB::table('request_coach')->where('trainer_to_recive_request_id',Auth::user()->id)
          ->where('aprove_status',0)
          ->count();
          

         

          return $countOfUnreadRequests;

     }


   
    public  function sendMessage(Request $request){
       
       $conID = $request->conID;
       $msg = $request->msg;
       
        // broadcast(new MessageSent($user, $message))->toOthers();//new MessageSent('How re You!!!');
       //broadcast(new MessageSent($user, $message))->toOthers();
     
       $myID = Auth::user()->id;
       
      
        $checkUserId = DB::table('messages')->where('conversation_id', $conID)->get();
     
             if(count($checkUserId) == 0 ) 
             {

                //this is a new conversation, we need to add new conversations Id and send message


              $userToSendId = $request->session()->get('user_to_send_message_id');
              
              // now we check if this user have already a conversation

                $checkCon1 = DB::table('conversation')->where('user_one',$myID)
                ->where('user_two',$userToSendId)->get(); // if loggedin user started conversation
                $checkCon2 = DB::table('conversation')->where('user_two',$myID)
                ->where('user_one',$userToSendId)->get(); // if loggedin recviced message first

                $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
                
                      if(count($allCons)!=0)
                      {
                        // old conversation
                        $conID = $allCons[0]->id;
                      }
                      else
                      {
                         $conID = DB::table('conversation')->insertGetId(
                          [
                            'user_one' => $myID,
                            'user_two' => $userToSendId,
                            'status' => 1,
                            'last_message' => $msg,
                            'user_one_status' => 1,
                             'user_two_status' => 1
                          ]); 
                      }

                        $time = Carbon::now();
                        $time = $time->toDateTimeString();

                          $conversation =  DB::table('conversation')
                                        ->where('id',$conID)
                                        ->get();

                      if($conversation[0]->user_one == Auth::user()->id)  // we need to update user two id
                      {
                            DB::table('conversation')
                         ->where('id',$conID)
                         ->update(['created_at'=>$time,'last_message'=>$msg,'status'=>1,'user_one_status'=>0,'user_two_status'=>1]) ;
                      } 
                      else // update user one status id
                      {
                           DB::table('conversation')
                        ->where('id',$conID)
                        ->update(['created_at'=>$time,'last_message'=>$msg,'status'=>1,'user_one_status'=>1,'user_two_status'=>0]) ;
                      }                

                       

                       $sendM = DB::table('messages')->insertGetId([
                                  'user_to' => $userToSendId ,
                                  'user_from' => Auth::user()->id,
                                  'msg' => $msg,
                                  'status' => 1,
                                  'conversation_id' => $conID,
                                  'created_at_time' => $time
                                ]);

                                if($sendM>0)
                                {
                                  // $userMsg = DB::table('messages')
                                  // ->join('users', 'users.id','messages.user_from')
                                  // ->where('messages.conversation_id', $conID)
                                  // ->orderBy('created_at_time', 'asc')
                                  // ->get();

                                  $userMsg = DB::table('messages')
                                  ->join('users', 'users.id','messages.user_from')
                                  ->where('messages.id',$sendM)
                                  ->orderBy('created_at_time', 'asc')
                                  ->get();

                                   DB::table('conversation')
                                  ->where('id',$conID)
                                  ->update(['created_at'=>$time,'last_message'=>$msg]);

                                   event(new MessageSent($userToSendId,$userMsg));

                                   return $userMsg;
                                }
                        
                       


                     

             }
             else
             {

                        $time = Carbon::now();
                        $time = $time->toDateTimeString();

                      if($checkUserId[0]->user_from== Auth::user()->id){
                        // fetch user_to
                        $fetch_userTo = DB::table('messages')->where('conversation_id', $conID)
                        ->get();
                          $userTo = $fetch_userTo[0]->user_to;
                      }else
                      {
                      // fetch user_to
                      $fetch_userTo = DB::table('messages')->where('conversation_id', $conID)
                      ->get();
                        $userTo = $fetch_userTo[0]->user_from;
                      }

                      $conversation =  DB::table('conversation')
                                        ->where('id',$conID)
                                        ->get();

                      if($conversation[0]->user_one == Auth::user()->id)  // we need to update user two id
                      {
                            DB::table('conversation')
                         ->where('id',$conID)
                         ->update(['created_at'=>$time,'last_message'=>$msg,'status'=>1,'user_one_status'=>0,'user_two_status'=>1]) ;
                      } 
                      else // update user one status id
                      {
                           DB::table('conversation')
                        ->where('id',$conID)
                        ->update(['created_at'=>$time,'last_message'=>$msg,'status'=>1,'user_one_status'=>1,'user_two_status'=>0]) ;
                      }                

                     
                     
                      // now send message
                        $sendM = DB::table('messages')->insertGetId([
                          'user_to' => $userTo,
                          'user_from' => Auth::user()->id,
                          'msg' => $msg,
                          'status' => 1,
                          'conversation_id' => $conID,
                          'created_at_time' => $time
                        ]);
                        if($sendM>0)
                        {
                         // after event sent i get only the last row messages from db. 
                          $userMsg = DB::table('messages')
                           ->join('users', 'users.id','messages.user_from')
                          ->where('messages.id',$sendM)
                          //->where('messages.conversation_id', $conID)
                          ->orderBy('created_at_time', 'asc')
                          ->get();

                           event(new MessageSent($userTo,$userMsg)); // i send just the last db row
                           
                           return $userMsg;
                        }

                  }
            }


    public function getNewMessageUser()
    {

       $conID = 0;
         

          $userToMessageId = session()->get('user_to_send_message_id');

            $checkCon1 = DB::table('conversation')->where('user_one',$userToMessageId)
                ->where('user_two',Auth::user()->id)->get(); // if loggedin user started conversation
                $checkCon2 = DB::table('conversation')->where('user_one',Auth::user()->id)
                ->where('user_two',$userToMessageId)->get(); // if loggedin recviced message first

                $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
                
                      if(count($allCons)!=0)
                      {
                        // old conversation
                        $conID = $allCons[0]->id;
                      }
                      else
                      {
                          $conID = 0;
                      }




          return  $conID;
    }        


    public function newMessageTo(Request $request)
    {
               
               if(Auth::user()->id ==  $request->user_to_message)
               {
                  
                  return redirect()->action(
                      'TrainerListController@showTrainerList', ['is_error' => true]
                  );
               }
               else
               {
                      $personalData = DB::table('users')
                         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                          ->join('user_options','users.id','=','user_options.user_id')
                          ->leftjoin('user_data','users.id','=','user_data.user_id')
                          ->where('users.id', $request->user_to_message )
                          ->get();
                      
                      $request->session()->put('user_to_send_message_id', $request->user_to_message);
                     
                      //return redirect()->route('messages', ['userToSend' => $request->user_to_message]);
                      return view('messages')->with('personalData',$personalData)->with('id_to_message',$request->user_to_message);
               }
    }


    public function getLoggedInUserId(Request $request)
    {
        return Auth::user()->id;
    }


    public function messages()
    {   
        $personalData = NULL;
        return view('messages')->with('personalData',$personalData);
    }

    public function newMessage(){
      $uid = Auth::user()->id;
      $friends1 = DB::table('friendships')
              ->leftJoin('users', 'users.id', 'friendships.user_requested') // who is not loggedin but send request to
              ->where('status', 1)
              ->where('requester', $uid) // who is loggedin
              ->get();
      $friends2 = DB::table('friendships')
              ->leftJoin('users', 'users.id', 'friendships.requester')
              ->where('status', 1)
              ->where('user_requested', $uid)
              ->get();
      $friends = array_merge($friends1->toArray(), $friends2->toArray());
      return view('newMessage', compact('friends', $friends));
    }
    public function sendNewMessage(Request $request){
        $msg = $request->msg;
        $friend_id = $request->friend_id;
        $myID = Auth::user()->id;
        //check if conversation already started or not
        $checkCon1 = DB::table('conversation')->where('user_one',$myID)
        ->where('user_two',$friend_id)->get(); // if loggedin user started conversation
        $checkCon2 = DB::table('conversation')->where('user_two',$myID)
        ->where('user_one',$friend_id)->get(); // if loggedin recviced message first
        $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
        if(count($allCons)!=0){
          // old conversation
          $conID_old = $allCons[0]->id;
          //insert data into messages table
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conversation_id' =>  $conID_old,
            'status' => 1
          ]);
        }else {
          // new conversation
          $conID_new = DB::table('conversation')->insertGetId([
            'user_one' => $myID,
            'user_two' => $friend_id
          ]);
          
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conversation_id' =>  $conID_new,
            'status' => 1
          ]);
        }
    }

    public function sendNewMessageFunction($message,$friend_id)
    {
       $msg = $request->msg;
        $friend_id = $request->friend_id;
        $myID = Auth::user()->id;
        //check if conversation already started or not
        $checkCon1 = DB::table('conversation')->where('user_one',$myID)
        ->where('user_two',$friend_id)->get(); // if loggedin user started conversation
        $checkCon2 = DB::table('conversation')->where('user_two',$myID)
        ->where('user_one',$friend_id)->get(); // if loggedin recviced message first
        $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
        if(count($allCons)!=0){
          // old conversation
          $conID_old = $allCons[0]->id;
          //insert data into messages table
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conversation_id' =>  $conID_old,
            'status' => 1
          ]);
        }else {
          // new conversation
          $conID_new = DB::table('conversation')->insertGetId([
            'user_one' => $myID,
            'user_two' => $friend_id
          ]);
          echo $conID_new;
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conversation_id' =>  $conID_new,
            'status' => 1
          ]);
        }
    }

    public function getNotifications()
    {
         $coachingData = DB::table('online_coachings')
                  
                  ->select('online_coachings.id as coaching_id', 'user_customer_id','lastname','start_period','end_period','read_status','name','lastname','profile_picture')
                  ->join('users','users.id','online_coachings.user_customer_id')
                  ->where('online_coachings.coach_id',Auth::user()->id)
                  ->orderBy('start_period', 'asc')
                  ->get();

          return $coachingData;
    }

    public function jobs(){
      $jobs = DB::table('users')
      ->Join('jobs','users.id','jobs.company_id')
      ->get();
      return view('profile.jobs', compact('jobs'));
    }
    public function job($id){
      $jobs = DB::table('users')
      ->leftJoin('jobs','users.id','jobs.company_id')
      ->where('jobs.id',$id)
      ->get();
      return view('profile.job', compact('jobs'));
    }
}