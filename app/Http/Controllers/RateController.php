<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\CommercialTraining;


use DateTime;
use Auth;

class RateController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   


  public  function uploadTrainingIndex()
   {
        return view('upload_training');

   }


  public function rateTrainer(Request $request)
  {
            $trainerId = $request->userToRate;
            $rateValue = $request->rateValue;


            $result =  DB::table('rates')
             ->where('user_rated_id',$trainerId)
             ->where('from_user_id',Auth::user()->id)
             ->update(['rate_value'=>$rateValue]);

            if($result == 0)
            {
                  DB::table('rates')->insert(['user_rated_id'=>$trainerId,'from_user_id'=>Auth::user()->id,'rate_value'=>$rateValue]);

            }

             $numberOfReviews = DB::table('rates')->where('user_rated_id',$trainerId)->count();
             

             if($numberOfReviews > 1)
             {
                $averageRate = DB::table('rates')->where('user_rated_id',$trainerId)->avg();
             }
             else
             {
                  $averageRate = $rateValue;
             }

           $userDateResult = DB::table('user_data')->where('user_id',$trainerId)
             ->update(['number_of_reviews'=>$numberOfReviews,'rate'=>$averageRate]);


            if($userDateResult == 0)
            {
                  DB::table('user_data')->insert(['user_id' => $trainerId, 'rate'=>$averageRate,'number_of_reviews'=>$numberOfReviews]);
            }

             return $result;
  }

    public function rateTraining(Request $request)
  {
            $trainingId = $request->trainingToRate;
            $rateValue = $request->rateValue;
            
             $numberOfReviews = 0;
             $averageRate = 0;


            $result =  DB::table('training_rates')
             ->where('training_rated_id',$trainingId)
             ->where('from_user_id',Auth::user()->id)
             ->update(['rate_value'=>$rateValue]);

             

            if($result == 0)
            {
                  DB::table('training_rates')->insert(['training_rated_id'=>$trainingId,'from_user_id'=>Auth::user()->id,'rate_value'=>$rateValue]);

            }

            $numberOfReviews = DB::table('training_rates')->where('training_rated_id',$trainingId)->count();
           
           if($numberOfReviews > 1)
           {
              $averageRate = DB::table('training_rates')->where('training_rated_id',$trainingId)->avg();

           }
           else
           {
                $averageRate = $rateValue;
           }

             DB::table('commercial_training')->where('id',$trainingId)
             ->update(['number_of_reviews'=>$numberOfReviews,'rate'=>$averageRate]);
             


             return $result;
  }

  
  public function showTrainingRateModal(Request $request)
  {
        $rateResult = DB::table('training_rates')
        ->where('training_rated_id',$request->training_id)
        ->where('from_user_id',Auth::user()->id)
        ->get();

        $rateValue  = 0;

        if(count($rateResult)>0)
        {
            $rateValue  = $rateResult[0]->rate_value;
        }

        return view('inc.modals.training_rate_modal')->with('rateValue',$rateValue);
  }


    
}