<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\CommercialTraining;


class TrainerListController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showTrainerList()
    {
        
         $detailTrainingData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.type','trainer')
          ->where('users.status','1')


          ->get();


            if(isset($_GET['is_error']))
            {
                return view('trainer_list')->with('trainerData',$detailTrainingData)->with('is_error',true)->with('isAthlete',false);

            }
            else
            {
                 return view('trainer_list')->with('trainerData',$detailTrainingData)->with('is_error',false)->with('isAthlete',false);

            }

    }


 public function filterTrainer()
    {
       $searchBar = $_GET['searchInputValue'];
       $orderBy = $_GET['orderBy'];
       $searchFor = $_GET['searchFor'];

        $detailTrainingData = DB::table('users')
         ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
          ->join('user_options','users.id','=','user_options.user_id')
          ->leftjoin('user_data','users.id','=','user_data.user_id')
          ->where('users.type','trainer')
           ->where('users.status','1')
          ->get();


         if($searchFor == 'athlete')
         {

           if($searchBar != '')
           {

                         
                           $splitSearchBar = explode(' ',$searchBar);

                          for ($i = 0; $i<count($splitSearchBar); $i++)
                          {
                              $detailTrainingData = DB::table('users')
                             ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                              ->join('user_options','users.id','=','user_options.user_id')
                            ->leftjoin('user_data','users.id','=','user_data.user_id')
                            ->where('users.type','athlete')
                            ->where('users.name','like','%' . $splitSearchBar[$i] . '%')
                             ->where('users.status','1')
                           ->orWhere('users.lastname','like','%' .$splitSearchBar[$i] . '%')
                            ->get();

                             if(count($detailTrainingData) > 0)
                            {
                                 break;
                            }

                          }

                return view('trainer_list')->with('trainerData',$detailTrainingData)->with('is_error',false)->with('isAthlete',true);


            }
            else
            {

          

                $detailTrainingData = DB::table('users')
                 ->select('users.id as trainer_id', 'name','lastname','profile_picture','about')
                  ->join('user_options','users.id','=','user_options.user_id')
                  ->leftjoin('user_data','users.id','=','user_data.user_id')
                  ->where('users.type','athlete')
                   ->where('users.status','1')
                  ->get();
                   return view('trainer_list')->with('trainerData',$detailTrainingData)->with('is_error',false)->with('isAthlete',true);
             }
         }
         else
         {



                     if($searchBar != '')
                     {

                         
                           $splitSearchBar = explode(' ',$searchBar);

                          for ($i = 0; $i<count($splitSearchBar); $i++)
                          {
                              $detailTrainingData = DB::table('users')
                           ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                            ->join('user_options','users.id','=','user_options.user_id')
                            ->leftjoin('user_data','users.id','=','user_data.user_id')
                            ->where('users.type','trainer')
                            ->where('users.name','like','%' . $splitSearchBar[$i] . '%')
                             ->where('users.status','1')
                           ->orWhere('users.lastname','like','%' .$splitSearchBar[$i] . '%')
                            ->get();

                            if(count($detailTrainingData) > 0)
                            {
                                 break;
                            }
                          }

                           

                     }


                    

                    

                     if($orderBy == 'lowest_first') // price
                     {
                        

                              $detailTrainingData = DB::table('users')
                           ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                            ->join('user_options','users.id','=','user_options.user_id')
                            ->leftjoin('user_data','users.id','=','user_data.user_id')
                            ->where('users.type','trainer')
                             ->where('users.status','1')
                            ->orderBy('user_options.coaching_price', 'asc')
                            ->get();
                             
                     }

                     if($orderBy == 'highest_first') //price
                     {
                          
                              $detailTrainingData = DB::table('users')
                           ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                            ->join('user_options','users.id','=','user_options.user_id')
                            ->leftjoin('user_data','users.id','=','user_data.user_id')
                            ->where('users.type','trainer')
                             ->where('users.status','1')
                            ->orderBy('user_options.coaching_price', 'desc')
                            ->get();
                     }

                     if($orderBy == 'highest_rate')
                     {
                          
                              $detailTrainingData = DB::table('users')
                           ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                            ->join('user_options','users.id','=','user_options.user_id')
                            ->leftjoin('user_data','users.id','=','user_data.user_id')
                            ->where('users.type','trainer')
                             ->where('users.status','1')
                            ->orderBy('user_data.rate', 'desc')
                            ->get();
                     }

                     if($orderBy == 'number_of_rates')
                     {
                         
                              $detailTrainingData = DB::table('users')
                           ->select('users.id as trainer_id', 'name','lastname','profile_picture','coaching_price','online_coaching_period','onlineCoaching','currency','about','rate','number_of_reviews','type')
                            ->join('user_options','users.id','=','user_options.user_id')
                            ->leftjoin('user_data','users.id','=','user_data.user_id')
                            ->where('users.type','trainer')
                             ->where('users.status','1')
                            ->orderBy('user_data.rate', 'desc')
                            ->get();
                     }


                         
                   
                       return view('trainer_list')->with('trainerData',$detailTrainingData)->with('is_error',false)->with('isAthlete',false);


                  }
          }

   

       
}

