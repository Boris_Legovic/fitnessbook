<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\CommercialTraining;


use DateTime;
use Auth;

class TrainingDetailsDescripton extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   


 


    public function loadDetailTrainingDescription(Request $request)
    {
                
                $id = $request->training_id;
                // $ownerId =  $request->owner_id;
               
                 $detailTrainingData = DB::table('commercial_training') 
                  ->where('commercial_training.id', '=', $id)
                  ->join('users','commercial_training.user_id','=','users.id')
                 ->get();
                // echo "idtest " .  $detailTrainingData[0]->user_id;
                


                $request->session()->put('stripe_id', $detailTrainingData[0]->stripe_id);
                $request->session()->put('training_id', $id);
                
                $userHasBoughtTraining = DB::table('training_boughts')
                ->where('user_id',Auth::user()->id)
                 ->where('training_id',$id)->get();

                $trainingRate = DB::table('training_rates')
                ->where('training_rated_id',$id)
                ->where('rate_value','>',0)
                ->avg('rate_value');

                 $numberOfRates = DB::table('training_rates')
                ->where('training_rated_id',$id)
                ->where('rate_value','>',0)
                ->count();
               

                
                 $userAlreadyBought = false;

                 if(count($userHasBoughtTraining)>0)
                 {
                     $userAlreadyBought = true;

                 }

                 

                 return view('details_training_view')->with('trainingData',$detailTrainingData)->with('userAlreadyBought',$userAlreadyBought)->with('trainingId',$id)->with('rate',$trainingRate)->with('numberOfRates',$numberOfRates);

    }
}