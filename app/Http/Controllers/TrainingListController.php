<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\CommercialTraining;
use Auth;


class TrainingListController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->get();
         
         
         return view('training_list')->with('trainingData',$detailTrainingData)->with('rateEnabled',false);
    }

    public function filterTraining()
    {
       $searchBar = $_GET['searchInputValue'];
       $searchType = $_GET['searchType'];
       $orderBy = $_GET['orderBy'];
       $filterBy = $_GET['filterBy'];

       $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->get();


       if($searchBar != '')
       {

                $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
             ->join('users','commercial_training.user_id','=','users.id')
             ->where('commercial_training.title','like',$searchBar)
             ->get();
       }


       if($searchType == 'owner')
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->where('users.lastname','like',$searchBar)
          ->orWhere('users.name','like',$searchBar)
          ->get();


       }

       if($searchType == 'training')
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->where('commercial_training.title','like',$searchBar)
          ->get();
       }

       if($searchType == '') // training is default search
       {
           if($searchBar != '')
           { 

                $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
              ->join('users','commercial_training.user_id','=','users.id')
              ->where('commercial_training.title','like',$searchBar)
              ->get();
          }
       }
// free_and_paid
       if($filterBy == 'paid')
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->where('commercial_training.price','>',0.0)
          ->get();
       }

        if($filterBy == 'free')
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->where('commercial_training.price',0.0)
          ->get();
       }

        if($filterBy == 'free_and_paid')
       {
              $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
             ->join('users','commercial_training.user_id','=','users.id')
          
             ->get();
       }

       if($orderBy == 'lowest_first') // price
       {
           
               $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
               ->join('users','commercial_training.user_id','=','users.id')
                ->where('commercial_training.price','>',0.0)
               ->orderBy('commercial_training.price', 'asc')
               ->get();
               
       }

       if($orderBy == 'highest_first') //price
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
               ->join('users','commercial_training.user_id','=','users.id')
               ->where('commercial_training.price','>',0.0)
               ->orderBy('commercial_training.price', 'desc')
               ->get();
       }

       if($orderBy == 'highest_rate')
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
               ->join('users','commercial_training.user_id','=','users.id')
               ->orderBy('commercial_training.rate', 'desc')
               ->get();
       }

       if($orderBy == 'number_of_rates')
       {
             $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
               ->join('users','commercial_training.user_id','=','users.id')
               ->orderBy('commercial_training.number_of_reviews', 'desc')
               ->get();
       }


           
     
        return view('training_list')->with('trainingData',$detailTrainingData)->with('rateEnabled',false);


    }

        public function trainingsBought()
    {
        $detailTrainingData = DB::table('commercial_training')
         ->select('commercial_training.id as id_training', 'strength','muscle_mass','burn_kcal','image','number_of_reviews','rate','title','name','lastname','price','currency')
          ->join('users','commercial_training.user_id','=','users.id')
          ->join('training_boughts','commercial_training.id','=','training_boughts.training_id')
          ->where('training_boughts.user_id',Auth::user()->id)
          ->get();
         
         
         return view('training_list')->with('trainingData',$detailTrainingData)->with('rateEnabled',true);
    }
}