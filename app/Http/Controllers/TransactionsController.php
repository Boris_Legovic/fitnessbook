<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\CommercialTraining;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use Carbon\Carbon;
use App\Events\MessageSent;
use App\OnlineCoachings;
use App\trainingBought;




use DateTime;
use Auth;

class TransactionsController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   

  function aproveFreeCoaching(Request $request)
  {
                   $id = $request->id;
               
                        $dataTraining = DB::table('user_options')->where('user_id',Auth::user()->id)->get();


                   $timeEnd;

                   $timeEnd = Carbon::now();

                   $timeStart = Carbon::now();
                   $timeStart = $timeStart->toDateTimeString();

                     for($i=0; $i<$dataTraining[0]->online_coaching_period;$i++)
                     {
                         $timeEnd = $timeEnd->addMonth();
                     }

                    
                    $timeEnd = $timeEnd->toDateTimeString();
                      
                    $onlineCoachings = new OnlineCoachings;
                   
                    $onlineCoachings->user_customer_id=$id;
                    $onlineCoachings->coach_id=Auth::user()->id;
                    $onlineCoachings->start_period=$timeStart;
                    $onlineCoachings->end_period=$timeEnd;
                    $onlineCoachings->read_status=0;

                    $onlineCoachings->save();


                    DB::table('request_coach')
                    ->where('trainer_to_recive_request_id',Auth::user()->id)
                    ->where('user_that_requested_id',$id)
                    ->update(['aprove_status'=>1,'read_status'=>1]);
                    
                    

                    return 'success';

  }

 function declineUserCoachRequest()
 {

 }



  function goToFreeCoachingRequest(Request $request)
  {
            $coachId = 0;
            $user_id = 0;
          
            $coachId =  $request->coachId;
            $userData  = DB::table('users')->where('id',$coachId)->get();

            $dataTraining = DB::table('user_options')->where('user_id',$coachId)->get();
            $user_id =  Auth::user()->id; // this is loged in user that downloaded training, we must add the training to his training list

         
                 //  $timeEnd;
                 //  $timeEnd = Carbon::now();

                 //   $timeStart = Carbon::now();
                 //   $timeStart = $timeStart->toDateTimeString();

                 //   for($i=0; $i<$dataTraining[0]->online_coaching_period;$i++)
                 //   {
                 //       $timeEnd = $timeEnd->addMonth();
                 //   }

                  
                 //  $timeEnd = $timeEnd->toDateTimeString();
                    
                 //  $onlineCoachings = new OnlineCoachings;
                 
                 //  $onlineCoachings->user_customer_id=$user_id;
                 //  $onlineCoachings->coach_id=$coachId;
                 //  $onlineCoachings->start_period=$timeStart;
                 //  $onlineCoachings->end_period=$timeEnd;
                 //  $onlineCoachings->read_status=0;


                  
                 //  $onlineCoachings->save();

                



                 // event(new MessageSent($coachId,$coachingData));

                  DB::table('request_coach')->insert(['user_that_requested_id'=>Auth::user()->id,'trainer_to_recive_request_id'=>$coachId, 'aprove_status'=>0,'read_status'=>0]);

                    $coachingRequest = DB::table('request_coach')
                  ->where('trainer_to_recive_request_id',$coachId)
                  ->where('user_that_requested_id',Auth::user()->id)
                  ->get();
                  
                   event(new MessageSent($coachId,$coachingRequest));

                   return view('coach_bought_sucess')->with('state','sucess');    
              
            
  }



   function checkoutCoaching(Request $request)
   {

      $coachId = 0;
      $user_id = 0;

     
      $coachId =  $request->coachId;

       $userData  = DB::table('users')->where('id',$coachId)->get();

       $stripeAccountOfSeller =  $userData[0]->stripe_id; // this is the trainer who sold program 
      
      $dataTraining = DB::table('user_options')->where('user_id',$coachId)->get();

     $user_id =  Auth::user()->id; // this is loged in user that downloaded training, we must add the training to his training list

           $currency = 'USD';
          
          if($dataTraining[0]->currency == '$')
          {
              $currency = 'USD';
          }
          else
          {
             $currency = 'EUR';
          }

          $price = $dataTraining[0]->coaching_price;
          $toSend = ($price * 0.91) * 100 ;



              try
              {
                  $charge = Stripe::charges()->create([
                  'amount' => $price, // dolars
                  'currency' =>$currency,
                  'source' =>$request->stripeToken, //ovo je token kartice koje sam poslao, znaci da bi trebao poslati 
                  'description'=>'here goes desc',
                  'destination' => [
                      "amount" => $toSend, //cents
                      "account" =>  $stripeAccountOfSeller,
                      ]
                    ]);

                  $timeEnd;
                  $timeEnd = Carbon::now();

                   $timeStart = Carbon::now();
                   $timeStart = $timeStart->toDateTimeString();

                   for($i=0; $i<$dataTraining[0]->online_coaching_period;$i++)
                   {
                       $timeEnd = $timeEnd->addMonth();
                   }

                  
                  $timeEnd = $timeEnd->toDateTimeString();
                    
                  $onlineCoachings = new OnlineCoachings;
                 
                  $onlineCoachings->user_customer_id=$user_id;
                  $onlineCoachings->coach_id=$coachId;
                  $onlineCoachings->start_period=$timeStart;
                  $onlineCoachings->end_period=$timeEnd;
                  $onlineCoachings->read_status=0;


                  
                  $onlineCoachings->save();

                  $coachingData = DB::table('online_coachings')
                  ->join('users', 'users.id','online_coachings.user_customer_id')
                  ->where('online_coachings.user_customer_id',$user_id)
                  ->orderBy('start_period', 'asc')
                  ->get();


                

                  event(new MessageSent($coachId,$coachingData));

                   return view('coach_bought_sucess')->with('state','sucess');    
              
             } 

             catch (\Cartalyst\Stripe\Exception\CardErrorException $e) 
             {
                 $message = $e->getMessage();
                 return view('coach_bought_sucess')->with('state',$message);    
             }
             catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
                 $message = $e->getMessage();
                 return view('coach_bought_sucess')->with('state',$message);    

             }
             catch (\Cartalyst\Stripe\Exception\Exception\InvalidRequestException $e) {
                 $message = $e->getMessage();
                 return view('coach_bought_sucess')->with('state',$message);    
             }
             catch (\Cartalyst\Stripe\Exception\Exception\CardErrorException $e) {
                  $message = $e->getMessage();
                 return view('coach_bought_sucess')->with('state',$message);    
             }
             catch (\Cartalyst\Stripe\Exception\Exception\NotFoundException $e) {
                 $message = $e->getMessage();
                 return view('coach_bought_sucess')->with('state',$message);    
             }
   }


   function checkout(Request $request)
   {

     $training_id = 0;
     $user_id = 0;

     $stripeAccountOfSeller =  $request->session()->get('stripe_id'); // this is the trainer who sold program 
     $trainingId =  $request->session()->get('training_id');
    

     // echo "CHECK " . $trainingId;
     // echo "seller " . $stripeAccountOfSeller;

     // echo "user_token " . $request->stripeToken;

     $user_id =  Auth::user()->id; // this is loged in user that downloaded training, we must add the training to his training list
    
     $trainingFile = DB::table('commercial_training') 
      ->where('id','=',$trainingId)->get();
   
         // na cijeli iznos se daje porez, a onda se klijentu prosljedjuje tocan iznos,a  meni ostatak
          $dataTraining = $trainingFile[0];
          $currency = 'USD';
          
          if($dataTraining->currency == '$')
          {
              $currency = 'USD';
          }
          else
          {
             $currency = 'EUR';
          }

          $price = $dataTraining->price;
          $toSend = ($price * 0.91) * 100 ;

              
              Stripe::setApiKey("sk_live_hsZnm3tXB1vFB3T4MyVLmRyH");
              
              try
              {
                  $charge = Stripe::charges()->create([
                  'amount' => $price, // dolars
                  'currency' =>$currency,
                  'source' =>$request->stripeToken, //ovo je token kartice koje sam poslao, znaci da bi trebao poslati 
                  'description'=>'here goes desc',
                  'destination' => [
                      "amount" => $toSend, //cents
                      "account" =>  $stripeAccountOfSeller,
                      ]
                    ]);

                  $trainingBought = new TrainingBought;
                  $trainingBought->user_id=$user_id;
                  $trainingBought->training_id=$trainingId;
                   $trainingBought->status=0;
                  $trainingBought->save();

                   $file = $trainingFile[0]->file;
                   $link =  Storage::url($file);
                   $request->session()->put('fileName', $dataTraining->title);
                   $request->session()->put('file', $file);

                  $request->session()->put('stripe_id', '');
                  $request->session()->put('training_id', '');  

                   return view('training_download')->with('state','sucess');    
              
             } 

             catch (\Cartalyst\Stripe\Exception\CardErrorException $e) 
             {
                 $message = $e->getMessage();
                 return view('training_download')->with('state',$message);    
             }
             catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
                 $message = $e->getMessage();
                 return view('training_download')->with('state',$message);    

             }
             catch (\Cartalyst\Stripe\Exception\Exception\InvalidRequestException $e) {
                 $message = $e->getMessage();
                 return view('training_download')->with('state',$message);    
             }
             catch (\Cartalyst\Stripe\Exception\Exception\CardErrorException $e) {
                  $message = $e->getMessage();
                 return view('training_download')->with('state',$message);    
             }
             catch (\Cartalyst\Stripe\Exception\Exception\NotFoundException $e) {
                 $message = $e->getMessage();
                 return view('training_download')->with('state',$message);    
             }

       

            // return response()->download($pathToFile);
            // $response =  response()->download(storage_path("app/{$file}"),$dataTraining->title);
            // ob_end_clean();

               
            // return $response;
            // remove session here
            // echo "download " . $result;
            // Session::flash('downloadRequest', $response);
            // DB::table('training_boughts')->insert(['user_id'=>Auth::user()->id,'training_id'=>$trainingId]);

               
        

       
   }

   public function payPictures(Request $request)
   {
    

         

        
              $user_id =  Auth::user()->id; // this is loged in user that downloaded training, we must add the training to his training list
        
        
       
             // na cijeli iznos se daje porez, a onda se klijentu prosljedjuje tocan iznos,a  meni ostatak
             
                $currency = 'USD';

                $numberOfPics = $request->picturesNumber;

                $payAmount = 0;

                  if($numberOfPics == 30)
                  {
                    $payAmount = 48;
                  }
                  else if($numberOfPics == 75)
                  {
                      $payAmount = 96;
                  }
                   else if($numberOfPics == 150)
                  {
                      $payAmount = 168;
                  }
            
                  try
                  {
                      // $charge = Stripe::charges()->create([
                      // 'amount' => $price, // dolars
                      // 'currency' =>$currency,
                      // 'source' =>$request->stripeToken, //ovo je token kartice koje sam poslao, znaci da bi trebao poslati 
                      // 'description'=>'here goes desc',
                      // 'destination' => [
                      //     "amount" => $toSend, //cents
                      //     "account" =>  $stripeAccountOfSeller,
                      //     ]
                      //   ]);

                   // Stripe::setApiKey("sk_test_TpyHJ2L9w0Ya21GkcpAoZibV");

                   $token = $_POST['stripeToken'];

                    $charge = Stripe::charges()->create([
                        'amount' => $payAmount,
                        'currency' => 'usd',
                        'description' => 'Customer pay picture',
                        'source' => $request->stripeToken,
                        'statement_descriptor' => 'Customer pay pictures',
                    ]);


                      DB::table('user_options')->where('user_id', $user_id)->update(['max_number_of_pictures' => $numberOfPics ]);
                      
                    

                       return view('pictures_bought_sucess')->with('state','sucess');    
                  
                 } 

                 catch (\Cartalyst\Stripe\Exception\CardErrorException $e) 
                 {
                     $message = $e->getMessage();
                     return view('training_download')->with('state',$message);    
                 }
                 catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
                     $message = $e->getMessage();
                     return view('training_download')->with('state',$message);    

                 }
                 catch (\Cartalyst\Stripe\Exception\Exception\InvalidRequestException $e) {
                     $message = $e->getMessage();
                     return view('training_download')->with('state',$message);    
                 }
                 catch (\Cartalyst\Stripe\Exception\Exception\CardErrorException $e) {
                      $message = $e->getMessage();
                     return view('training_download')->with('state',$message);    
                 }
                 catch (\Cartalyst\Stripe\Exception\Exception\NotFoundException $e) {
                     $message = $e->getMessage();
                     return view('training_download')->with('state',$message);    
                 }

           

                // return response()->download($pathToFile);
                // $response =  response()->download(storage_path("app/{$file}"),$dataTraining->title);
                // ob_end_clean();

                   
                // return $response;
                // remove session here
                // echo "download " . $result;
                // Session::flash('downloadRequest', $response);
                // DB::table('training_boughts')->insert(['user_id'=>Auth::user()->id,'training_id'=>$trainingId]);

                   
            

           
       }
   



   public function downloadAlreadyBoughtFile(Request $request)
   {
                  // $trainingFile[0]->file;
                   $request->session()->put('fileName', $request->title);
                   $request->session()->put('file', $request->file);
   }


   public function downloadFile(Request $request)
   {
      $fileName = $request->session()->get('fileName');
      $file =  $request->session()->get('file');
     
          // $response =  response()->download(Storage::disk('do_spaces')->url($file),$fileName);
          //   ob_end_clean();

      // $file = Storage::disk('do_spaces')->get($file);
      // $storagePath = path(Storage::disk('do_spaces')->url($file));

      // $storagePath = Storage::disk('do_spaces')->path($file);

      $url = Storage::disk('do_spaces')->url($file);
      //$path = public_path($url);
     // $response =  response()->download(public_path($url),$fileName);
      //       ob_end_clean();

      // $headers = ['Content-Type'=>'application/pdf',

      //             ]; 
   
       $request->session()->put('fileName', '');
       $request->session()->put('file', '');      

       // return $response;

       return Storage::disk('do_spaces')->download($file, $fileName);
       // return response($file,200,$headers);
       // response()->file($file);
   }

   public function stripeUserConfirmation()
   {
                  $auth_code = $_GET['code'];
                  $ch = curl_init();

              curl_setopt($ch, CURLOPT_URL, "https://connect.stripe.com/oauth/token");
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "client_secret=sk_live_hsZnm3tXB1vFB3T4MyVLmRyH&code=$auth_code&grant_type=authorization_code");
              curl_setopt($ch, CURLOPT_POST, 1);

              $headers = array();
              $headers[] = "Content-Type: application/x-www-form-urlencoded";
              curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
               $stripe_id = 0;
              $result = curl_exec($ch);
              

              if (curl_errno($ch)) {
                  echo 'Error:' . curl_error($ch);
                      return view('stripe_register_message')->with('sucesss',false);

              }
              else
              {
                 $res = json_decode($result, true);;
                
                 $stripe_id =  $res['stripe_user_id'];
                  $id =  Auth::user()->id;
               
                          DB::table('users')
                          ->where('id', $id)
                          ->update(['stripe_id' =>  $stripe_id]);
                          return view('stripe_register_message')->with('sucesss',true);
              }
              curl_close ($ch);

            
           

        // curl https://connect.stripe.com/oauth/token \
        // -d client_secret=sk_test_TpyHJ2L9w0Ya21GkcpAoZibV \
        // -d code="{AUTHORIZATION_CODE}" \
        // -d grant_type=authorization_code

   }

   public function logoutFromStripe() // dont use this
   {
            $api_key = 'sk_test_TpyHJ2L9w0Ya21GkcpAoZibV';
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://connect.stripe.com/oauth/deauthorize',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_HTTPHEADER => array("Authorization: Bearer $api_key"),
              CURLOPT_POST => true,
              CURLOPT_POSTFIELDS => http_build_query(array(
                'client_id' => 'ca_CIc4spDBSBgusI8DbRhiJlJ8rQnEdBs1',
                'stripe_user_id' => 'acct_beDyHMDhUd5PXS',
              ))
            ));
            curl_exec($curl);
   }


   public function requestOnlineCoaching(Request $request)
   {
      $coachId = $request->id;
      $id =  Auth::user()->id;
   }

   
}

// metadata  - data that want to store on server
       //  try
       //  {
       // echo 'ses1';

       //      $charge = Stripe::charges()->create([
       //          'amount' => 100,
       //          'currency' =>'USD',
       //          'source' =>$request->stripeToken, //ovo je token kartice koje sam poslao, znaci da bi trebao poslati 
       //          'description'=>'here goes desc',
       //          'receipt_email'=>'bodi_bodileg@yahoo.com',
       //          'metadata'=>[
       //              'data1'=>'metadata 1',
       //              'data2'=>'metadata 2',
       //              'data3'=>'metadata 3'
       //          ]



       //      ]);
       //  echo 'sucess';
       //      //return back()->with('sucess_message','Thank you!');

       //  }
       //  catch(Exception $e)
       //  {
       //      echo 'eror' . $e;
       //  }

        // $validator = Validator::make($request->all(), [
        //     'card_no' => 'required',
        //     'ccExpiryMonth' => 'required',
        //     'ccExpiryYear' => 'required',
        //     'cvvNumber' => 'required',
        //     'amount' => 'required',
        // ]);
        
        // $input = $request->all();
        // if ($validator->passes()) 
        // {           
        //     $input = array_except($input,array('_token'));            
        //     $stripe = Stripe::make('sk_test_TpyHJ2L9w0Ya21GkcpAoZibV');
            
        //     try {
        //         $token = $stripe->tokens()->create([
        //             'card' => [
        //                 'number'    => $request->get('card_no'),
        //                 'exp_month' => $request->get('ccExpiryMonth'),
        //                 'exp_year'  => $request->get('ccExpiryYear'),
        //                 'cvc'       => $request->get('cvvNumber'),
        //             ],
        //         ]);
        //         if (!isset($token['id'])) {
        //             \Session::put('error','The Stripe Token was not generated correctly');
        //              echo 'ss1';
        //            // return redirect()->route('stripform');

        //         }
        //         $charge = $stripe->charges()->create([
        //             'card' => $token['id'],
        //             'currency' => 'USD',
        //             'amount'   => $request->get('amount'),
        //             'description' => 'Add in wallet',
        //         ]);
        //         if($charge['status'] == 'succeeded') {
        //             /**
        //             * Write Here Your Database insert logic.
        //             */
        //             Session::put('success','Money add successfully in wallet');
        //            // return redirect()->route('stripform');
        //                                  echo 'ss2';

        //         } else {
        //             \Session::put('error','Money not add in wallet!!');
        //            // return redirect()->route('stripform');
        //                                  echo 'ss3';

        //         }
        //     } catch (Exception $e) {
        //         \Session::put('error',$e->getMessage());
        //       //  return redirect()->route('stripform');
        //                              echo 'ss4';

        //     } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
        //         \Session::put('error',$e->getMessage());
        //        // return redirect()->route('stripform');
        //                              echo 'ss5';

        //     } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
        //         \Session::put('error',$e->getMessage());
        //                              echo 'ss6';

        //        // return redirect()->route('stripform');
        //     }
        // }
  // $charge = Stripe::charges()->create([
                // 'amount' => 100,
                // 'currency' =>'USD',
                // 'source' =>$request->stripeToken, //ovo je token kartice koje sam poslao, znaci da bi trebao poslati 
                //  "application_fee" => 2,
                // ],[ "stripe_account" =>  $stripeAccount
                //     ]
                //   );



                // Stripe::setApiKey("sk_test_TpyHJ2L9w0Ya21GkcpAoZibV");

                // $charge = \Stripe\Charge::create(array(
                //   "amount" => 1000,
                //   "currency" => "usd",
                //   "source" => "tok_visa",
                //   "destination" => array(
                //     "amount" => 877,
                //     "account" => "{CONNECTED_STRIPE_ACCOUNT_ID}",
                //   ),
                // ));

               // $user_id =  Auth::user()->id;

               //  $id = DB::table('users')->select('stripe_id')
               //  ->where('id', $user_id);
               //  ;

//*****************THIS WORK FOR DIRECT PAYMENT*******
               // echo "FAJL " . $file;



