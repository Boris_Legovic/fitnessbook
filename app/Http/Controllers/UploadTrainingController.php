<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\CommercialTraining;


use DateTime;
use Auth;

class UploadTrainingController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   


  public  function uploadTrainingIndex()
   {
        return view('upload_training');

   }


  public function setDataToDB(Request $request)
   {

        $file = $request->file('file');
        $image = $request->file('image');
        $title =  $request->title;
        $description = $request->description;
        $price = $request->price;
        $currency = $request->curencyValue;
        $owner_or_uploader_id =  Auth::user()->id;

        if(filesize($file)>20971520)
        {
            return view('upload_training')->with('fileToBig',true);;
        }
        else if(filesize($image)>10485760)
        {
            return view('upload_training')->with('pictureToBig',true);
        }
        else
        {    
 

            $imagePath = Storage::disk('do_spaces')->putFile('public/training_images',$image,'public');
            $filePath = Storage::disk('do_spaces')->putFile('public/training_files',$file,'public');
            
            

            $training = new CommercialTraining;
            $training->user_id = $owner_or_uploader_id;
            $training->description = $description;
            $training->title = $title;
            $training->price = $price;
            $training->currency = $currency;
            $training->file = $filePath;
            $training->image = $imagePath;
            
            if(isset($request->typeStrength))
            {
                $training->strength = $request->typeStrength;

            }
            else
            {
                $training->strength = 0;

            }

            if(isset($request->typeMuscle))
            {
                $training->muscle_mass = $request->typeMuscle;

            }
            else
            {
                $training->muscle_mass = 0;

            }

             if(isset($request->typeCardio))
            {
                $training->burn_kcal = $request->typeCardio;

            }
            else
            {
                $training->burn_kcal = 0;

            }

            




           $training->save();

          //  echo 'str ' . $request->typeStrength ;
           

           return view('upload_training')->with('sucess',true);
        }

       


   }


    
}





