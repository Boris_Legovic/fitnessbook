<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\CommercialTraining;
use DateTime;
use Auth;

class UserOptionsController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
public function settings()
{

     $id = Auth::user()->id;

        $detailUserData = DB::table('users') ->join('user_options','users.id','=','user_options.user_id')
                            ->where('users.id','=',$id)->get();

                            
    return view('setup')->with('userData',$detailUserData[0]);
}

public function updateName(Request $request)
{
     $id = Auth::user()->id;
     DB::table('users')
            ->where('id', $id)
            ->update(['name' => $request->value]);

            return "Sucess!";
}

public function updateLastname(Request $request)
{
     $id = Auth::user()->id;
     DB::table('users')
            ->where('id', $id)
            ->update(['lastname' => $request->value]);

            return "Sucess!";
}

public function updatePictures(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['max_number_of_pictures' => $request->value]);

            return "Sucess!";
}

public function coachingPeriod(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['online_coaching_period' => $request->value]);

            return "Sucess!";
}



public function onlineCoaching(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['onlineCoaching' => $request->value]);

            return "Sucess!";
}

public function updateCoachingPrice(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['coaching_price' => $request->value]);

            return "Sucess!";
}


public function priceCurrency(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['currency' => $request->value]);

            return "Sucess!";
}

public function weightUnit(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['weight_unit' => $request->value]);

            return "Sucess!";
}

public function sizeUnit(Request $request)
{
     $id = Auth::user()->id;
     DB::table('user_options')
            ->where('user_id', $id)
            ->update(['size_unit' => $request->value]);

            return "Sucess!";
}










 



  public function loadUserOptions()
   {

         return view('setup')->with('userData',$detailUserData[0]);

       


   }


    
}
