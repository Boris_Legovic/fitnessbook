<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
	protected $table = 'messages';
	protected $fillable = ['msg','created_at'];
    protected $timestamps = true;

    // public function user()
    // {
    //   return $this->belongsTo(User::class);
    //}
}
