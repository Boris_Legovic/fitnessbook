<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOptions extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'birthdate', 'max_number_of_pictures','online_coaching_period','coaching_price','currency','weight_unit','size_unit','onlineCoaching'
    ];

}
