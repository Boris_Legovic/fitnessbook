<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeassurementDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meassurement_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('date_of_meassure');
            $table->float('size_cm_or_kg');
            $table->string('exercize_name_or_weight');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meassurement_data');
    }
}
