<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberOfReviewsToCommercialTraining extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commercial_training', function($table) {
        $table->integer('number_of_reviews')->nullable();
        $table->float('rate')->nullable();

    });

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commercial_training', function($table) 
        {
            $table->dropColumn('rate');
            $table->dropColumn('number_of_reviews');
        });
    }
}
