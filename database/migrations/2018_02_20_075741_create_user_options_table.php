<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('birthdate')->nullable();
            $table->integer('max_number_of_pictures');
            $table->integer('online_coaching_period'); // months
            $table->float('coaching_price');
            $table->string('currency');
            $table->string('weight_unit'); // kg or lbs
            $table->string('size_unit'); // cm or inches
            $table->string('onlineCoaching');
            $table->string('about')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_options');
    }
}
