<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_rated_id')->nullable();
            $table->integer('from_user_id');
            $table->float('rate_value');
            $table->integer('training_rated_id')->nullable();

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_rates');
    }
}
