<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineCoachingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_coachings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coach_id');
            $table->integer('user_customer_id');
            $table->date('start_period');
            $table->date('end_period');
            $table->boolean('read_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_coachings');
    }
}
