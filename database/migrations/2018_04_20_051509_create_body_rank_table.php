<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyRankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_rank', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->float('body_fat')->nullable();
            $table->float('lean_mass')->nullable();
            $table->float('muscle_percentage')->nullable();
            $table->float('BMI')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_rank');
    }
}
