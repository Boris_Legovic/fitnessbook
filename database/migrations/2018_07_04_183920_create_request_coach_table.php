<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestCoachTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_coach', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('user_that_requested_id');
             $table->integer('trainer_to_recive_request_id');
             $table->integer('aprove_status');
             $table->integer('read_status');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_coach');
    }
}
