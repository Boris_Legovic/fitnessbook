<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesToCommercialTraining extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('commercial_training', function($table) {
               $table->boolean('strength');
               $table->boolean('muscle_mass');
               $table->boolean('burn_kcal');
               $table->boolean('endurance');
               


           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('commercial_training', function($table) {
        $table->dropColumn('paid');
    });
    }
}
