





   var trainingName = '';
   var exercizeNames = [];
   var sets = [];
   var reps = [];
   var comments=[];

      var i=1;
     
    

    function addRow()
    {
      $('#1addr'+i).html("<td>"+ (i+1) +"</td><td><input id = 'exercize"+i+"' name='exercize"+i+"' type='text' placeholder='Exercize' class='form-control input-md'  /></td><td><input onfocus='addListener(this,"+i.toString()+")' id='sets"+i+"' name='sets"+i+"' type='text' placeholder='0'  class='form-control input-md'  style='width: 40px;margin: auto;'></td><td id='repsColumn"+i+"'></td><td><input id='comments"+i+"' name='comments"+i+"' type='text' placeholder='Comment'  class='form-control input-md'></td>");

       $('#1tab_logic').append('<tr id="1addr'+(i+1)+'"></tr>');
       i++; 
    }

    function deleteRow()
    {
      if(i>1)
      {
         $("#1addr"+(i-1)).html('');
         i--;
      }
    }


      function saveNewTraining()
     {
          if(validateOk())
          {
              addNewTrainingToDB();
          }
        
     }

     function validateOk()
     {
        var validateOk = true;

        if( $('#trainingName').val() == '')
        {
            alert('Insert training name!');
            validateOk = false;
            return validateOk;
        }

        for(var m = 0; m<i; ++m)
        {
            var exercizeName = $('#exercize' + m).val();
            
            if(exercizeName == '')
            {
                 alert('Insert exercize name!');
                 validateOk = false;
                 break;
            }

            var set = $('#sets' + m ).val();

            if(set == '')
            {
               alert('Insert set number!');
               validateOk = false;
                 break;
            }
          
           for(var d = 0; d<set; ++d)
            {
               var rep = $('#' + m + 'reps' + d ).val();

               if(rep == '')
            {
               alert('Insert reps number!');
               validateOk = false;
                 break;
               }
            }
        }

        return validateOk;

     }



     function addNewTrainingToDB()
     {
           var trainingName= $('#trainingName').val();
  
        for(var m = 0; m<i; ++m)
        {
            exercizeNames.push($('#exercize' + m).val());
            sets.push($('#sets' + m ).val());
            var numberOfSets = $('#sets' + m ).val(); 
            var repsRange = []; 
            
            for(var d = 0; d<numberOfSets; ++d)
            {
                repsRange.push($('#' + m + 'reps' + d ).val());
            }
            
            reps.push(repsRange);
            comments.push($('#comments' + m ).val());
            

        }

        $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

       var id = $('#app').val();
       
        console.log(sets);
        
           $.ajax({
           type:'POST',
           url:'/addNewTraining',
           data:{exercizeNames:exercizeNames, sets:sets, reps:reps,comments:comments,trainingName:trainingName,id:app_id},
           success:function(data)
           {
             
               $('#modalWriteData').modal('toggle');
               alert(data);
           }
        });
     }




$( ".target" ).change(function() {
});

function addListener(element,numberOfId)
{


   /* $( '#'+ element.id ).change(function() {
         addMoreInputs(element.value,numberOfId);
     }); */

     $('#'+ element.id).keyup(function(){
       addMoreInputs(element.value,numberOfId);
    })
} 
   

function addMoreInputs(numberOfBoxes,numberOfId)
{

    $( '#repsColumn'+numberOfId ).empty();
  
  for(var m  = 0; m<numberOfBoxes;m++)
  {
     $( '#repsColumn'+numberOfId).append("<input id='" + numberOfId + "reps"+m+"' name='reps"+m+"' type='text' placeholder='Reps'  class='form-control input-md' style='width: 60px;margin: auto; margin-bottom:5px;'>");
  }
}


function clearModal()
{
    trainingName = '';
    exercizeNames = [];
    sets = [];
    reps = [];
    comments=[];

     i=1;
    $('#showTrainingListModal').modal('toggle');
    $('#modalWriteData').remove();
    

    
}

function loadNewTrainingModal()
{


     clearModal();
    
      $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
       
         
           $.ajax({
           type:'POST',
           url:'/loadAddTrainingModal',
           data:{id:'id'},
           success:function(data){
             // alert(data);
                $( 'body' ).append( data );
                $('#modalWriteData').modal('show');
              
           }
        });
    }





