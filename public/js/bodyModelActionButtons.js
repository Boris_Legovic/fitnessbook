
var isBodyDrawn = false;
var selectedBodyPartClass = 'showUnselectedBody';
var selectedBodyPart = 'no_data';
var weightUnit = '';
var sizeUnit = '';
var dataPositionForPhones = '';
var modalIsShown = false;
var meassurementType = '';

function setBodyHeight()
{
   selectedBodyPartClass = 'showHeight';
   selectedBodyPart = 'height';

             openNav('height');
}


function showNeck()
{
 
   // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
   // $("#bodyPictureContainer").addClass('showNeck');
   dataPositionForPhones = 'Neck';
    $("#body_image").attr('src', '/images/human_body_neck.png');

        selectedBodyPartClass = 'showNeck';
        selectedBodyPart = 'neck';

    if ( $(window).width() > 739) 
    {      
      //Add your javascript for large screens here 
     
         openNav('Neck');
    }
    else
    {     
        $('#weightSelectId').css('background','rgb(193, 198, 200)');

           showBodyDataPhones();
        
        
    }


}

function setWeightSelectedPhone()
{
  $('#weightSelectId').css('background','gray');

      selectedBodyPart = 'weight';
      dataPositionForPhones = 'Body weight';
     $("#body_image").attr('src', '/images/human_body.png');

      showBodyDataPhones();
}


function showShoulder()
{
     
     // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
     // $("#bodyPictureContainer").addClass('showShoulder');
  
    $("#body_image").attr('src', '/images/human_body_shoulder_red.png');

     selectedBodyPartClass = 'showShoulder';
     selectedBodyPart = 'shoulders';
     dataPositionForPhones = 'Shoulders';

        

    if ( $(window).width() > 739) 
    {      
      //Add your javascript for large screens here 
     
         openNav('Shoulders');
    }
    else
    {
            $('#weightSelectId').css('background','rgb(193, 198, 200)');
             showBodyDataPhones();
             if(modalIsShown)
             {
                openNavPhones('Shoulders');
             }
         //openNavPhones('Neck');
    }



}

function showChest()
{
     // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
     // $("#bodyPictureContainer").addClass('showChest');

     $("#body_image").attr('src', '/images/human_body_chest_red.png');

     selectedBodyPartClass = 'showChest';
     selectedBodyPart = 'chest';
     dataPositionForPhones = 'Chest';

        

           if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
               openNav('Chest');
          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }



}

function showHip()
{
    // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
    //  $("#bodyPictureContainer").addClass('showHip');

    $("#body_image").attr('src', '/images/human_body_hip_red.png');


     selectedBodyPartClass = 'showHip';
     selectedBodyPart = 'hip';
  dataPositionForPhones = 'Hip';
       

           if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                openNav('Hip');
          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showWaist()
{
         // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
         // $("#bodyPictureContainer").addClass('showWaist');
          $("#body_image").attr('src', '/images/human_body_waist_red.png');

         selectedBodyPartClass = 'showWaist';
                      selectedBodyPart = 'waist';
         dataPositionForPhones = 'Waist';

             // openNav('Waist');

              if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                openNav('Waist');
          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showLeftBiceps()
{
         // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
         // $("#bodyPictureContainer").addClass('showLeftBiceps');
        
 $("#body_image").attr('src', '/images/human_body_left_biceps_red.png');

         selectedBodyPartClass = 'showLeftBiceps';
                      selectedBodyPart = 'left_biceps';
                    dataPositionForPhones = 'Left biceps';
      
            


              if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                openNav('Left biceps');
          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
                 if(modalIsShown)
                 {
                      openNavPhones('Neck');
                 }
          }

}

function showRightBiceps()
{
      // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
      //    $("#bodyPictureContainer").addClass('showRightBiceps');

       $("#body_image").attr('src', '/images/human_body_right_biceps_red.png');

         selectedBodyPartClass = 'showRightBiceps';
                      selectedBodyPart = 'right_biceps';
                    dataPositionForPhones = 'Right biceps';


                if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                             openNav('Right biceps');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showLeftLeg()
{
        // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
        //  $("#bodyPictureContainer").addClass('showLeftLeg');

         $("#body_image").attr('src', '/images/human_body_left_leg_red.png');

         selectedBodyPartClass = 'showLeftLeg';
                      selectedBodyPart = 'left_glut';
                    dataPositionForPhones = 'Left glut';

            

                     if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                              openNav('Left glut');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showRightLeg()
{
         // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
         // $("#bodyPictureContainer").addClass('showRightLeg');
                  $("#body_image").attr('src', '/images/human_body_right_leg_red.png');

         selectedBodyPartClass = 'showRightLeg';
                      selectedBodyPart = 'right_glut';
                    dataPositionForPhones = 'Right glut';

            

                           if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                              openNav('Right glut');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }


}

function showLeftCalf()
{
         //  $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
         // $("#bodyPictureContainer").addClass('showLeftCalf');

                  $("#body_image").attr('src', '/images/human_body_left_calf_red.png');

         selectedBodyPartClass = 'showLeftCalf';
                      selectedBodyPart = 'left_calf';
                    dataPositionForPhones = 'Left calv';

            

          if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                             openNav('Left calv');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showRightCalf()
 {
//       $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
//          $("#bodyPictureContainer").addClass('showRightCalf');

                  $("#body_image").attr('src', '/images/human_body_right_calf_red.png');

         selectedBodyPartClass = 'showRightCalf';
                      selectedBodyPart = 'right_calf';
                    dataPositionForPhones = 'Right calv';

            

             if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                             openNav('Right calv');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showRightForearm()
{

   // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
   //       $("#bodyPictureContainer").addClass('showRightForeArm1');

                     $("#body_image").attr('src', '/images/human_body_right_forearm.png');

         selectedBodyPartClass = 'showRightForeArm1'; 
                      selectedBodyPart = 'right_forearm';
                    dataPositionForPhones = 'Right forearm';

            

                  if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                              openNav('Right forearm');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

function showLeftForearm()
{
     // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
     //     $("#bodyPictureContainer").addClass('showLeftForeArm1');

   $("#body_image").attr('src', '/images/human_body_left_forearm.png');

         selectedBodyPartClass = 'showLeftForeArm1'; 
                      selectedBodyPart = 'left_forearm';
                    dataPositionForPhones = 'Left forearm';

            

                        if ( $(window).width() > 739) 
          {      
            //Add your javascript for large screens here 
           
                               openNav('Left forearm');

          }
          else
          {
                  $('#weightSelectId').css('background','rgb(193, 198, 200)');
                 showBodyDataPhones();
               //openNavPhones('Neck');
          }

}

// other body actions

function changeGraphType(element)
{
      
    console.log('elementType ', element.value);

     $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

           $.ajax({
                 type:'POST',
                 url:'/loadMeassurementData',
                 data:{userId:app_id},
                 success:function(data){
                     console.log('allData ',data);
                     drawChart(data[0],element.value,data[2]);
                      //alert('Sucess!');
                  sizeUnit = data[2].size_unit;
                  weightUnit = data[2].weight_unit;
                    
               }
            }); 


   if(!isBodyDrawn)
   {
     // drawChart();
      isBodyDrawn = true;
   }
  
}



function showBodyDataPhones()
{

  if(modalIsShown)
         {
            openNavPhones();
         }
   
     $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

           $.ajax({
                 type:'POST',
                 url:'/loadMeassurementData',
                 data:{userId:app_id},
                 success:function(data)
                 {
                    
                       console.log('allData ',data[0]);
                       

                       meassurementType = data[2].weight_unit;

                       // get also the inch or cm from user ptions
                        drawChart(data[0],'line',data[2],dataPositionForPhones,true);
                       
                        if(data[1][0] != 0 && data[1][1] != 0)
                        {
                            loadMuscleMassLvl(data[1][0],data[1][1],true,meassurementType);
                        }
                        else
                        {
                            loadMuscleMassLvlEmpty(true);
                        }
                      //alert('Sucess!');

                       sizeUnit = data[2].size_unit;
                       weightUnit = data[2].weight_unit;
                
                    
               }
            }); 


             $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

           $.ajax({
                 type:'GET',
                 url:'/loadBodyRank',
                 success:function(data){
                     
                    if(data[2])
                    {
                        fitnessLevel(data[0],data[1], true); // true for phones
                    }
                    else
                    {
                      fitnessLevelEmpty(data[1], true); // true for phones
                    }
                     
                      //alert(data);
                
                    
               }
            }); 



          


   if(!isBodyDrawn)
   {
     // drawChart();
      isBodyDrawn = true;
   }
  
}




function showBodyData()
{

   //loadCaloriesView();
  if ( $(window).width() > 739) 
    {      
      //Add your javascript for large screens here 
          hideAllBodySelection();
       
       $("#graphsHolder").addClass('hidden');
       $("#bodyDataContainer").removeClass('hidden');
      // $("#kcalContainer").addClass('hidden');
       $("#mainId").addClass('hidden');

       

      
       $.ajaxSetup({
              headers: 
              {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

               $.ajax({
                     type:'POST',
                     url:'/loadMeassurementData',
                     data:{userId:app_id},
                     success:function(data)
                     {
                            console.log('allData ',data[0]);
                            // get also the inch or cm from user ptions
                            drawChart(data[0],'line',data[2]);

                             meassurementType = data[2].weight_unit;
                           
                            if(data[1][0] != 0 && data[1][1] != 0)
                            {
                                loadMuscleMassLvl(data[1][0],data[1][1],false,meassurementType);
                            }
                            else
                            {
                                loadMuscleMassLvlEmpty(false);
                            }
                          //alert('Sucess!');

                           sizeUnit = data[2].size_unit;
                           weightUnit = data[2].weight_unit;
                    
                        
                   }
                }); 


                 $.ajaxSetup({
              headers: 
              {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

               $.ajax({
                     type:'GET',
                     url:'/loadBodyRank',
                     success:function(data){
                         
                        if(data[2])
                        {
                            fitnessLevel(data[0],data[1],false); // false for desktop
                        }
                        else
                        {
                            fitnessLevelEmpty(data[1],false); // false for desktop
                        }
                         
                          //alert(data);
                    
                        
                   }
                }); 



              


       if(!isBodyDrawn)
       {
         // drawChart();
          isBodyDrawn = true;
       }
       // showBodyDataPhones();
    }
    else
    {


         showBodyDataPhones();
  
  }
}

function hideAllBodySelection()
{
       $("#body_chest").addClass('hidden');
       $("#body_neck").addClass('hidden');
       $("#body_shoulder").addClass('hidden');
       $("#body_hip").addClass('hidden');
       $("#body_left_biceps").addClass('hidden');
       $("#body_right_biceps").addClass('hidden');
       $("#body_waist").addClass('hidden');
       $("#body_left_leg").addClass('hidden');
       $("#body_right_leg").addClass('hidden');
       $("#body_left_calf").addClass('hidden');
       $("#body_right_calf").addClass('hidden');
       $("#body_hip").addClass('hidden');

  }

  function addBodyWeightPhones()
  {
      modalIsShown = true;
      selectedBodyPart = 'weight';
      dataPositionForPhones = 'Weight';
      openNavPhones();
  } 


  function openNavPhones()
  {
     modalIsShown = true;
     //  showBodyDataPhones();
     if(selectedBodyPart != 'no_data')
     {

      //document.getElementById("footer").hide();
     $("#footer").hide();


      document.getElementById("bottomNav").style.height = "300px";

     $("#bodyInputNameM").text(dataPositionForPhones);
     
     if( selectedBodyPart == 'weight')
     {

          if(weightUnit == "Kg")
        {
            $("#firstUnitIdM").prop("checked", true);
         
            
        }

        if(weightUnit == "Lbs")
        {
            $("#secondUnitIdM").prop("checked", true);

        }



         $("#unitValueIdM").text('Weight:');
         $("#firstUnitIdLabelM").text('Kg');
         $("#secondUnitIdLabelM").text('Lbs');

     }
     else
     {
        if(sizeUnit == "Cm")
        {
            $("#firstUnitIdM").prop("checked", true);
         
            
        }

        if(sizeUnit == "Inch")
        {
            $("#secondUnitIdM").prop("checked", true);

        }



         $("#unitValueIdM").text('Size:');
         $("#firstUnitIdLabelM").text('Cm');
         $("#secondUnitIdLabelM").text('Inch');
      }
    }
    else
    {
        alert('Select body part!');
    }
  }

  function openNav(position)
   {

     // document.getElementById("sidenavLeft").style.width = "0";
    // document.getElementById("main-left").style.marginRight= "0";
    
     document.getElementById("mySidenav").style.width = "250px";
    // document.getElementById("main").style.marginLeft = "-250px";

     
     $("#bodyInputName").text(position);
     if( selectedBodyPart == 'weight')
     {

          if(weightUnit == "Kg")
        {
            $("#firstUnitId").prop("checked", true);
         
            
        }

        if(weightUnit == "Lbs")
        {
            $("#secondUnitId").prop("checked", true);

        }



         $("#unitValueId").text('Weight:');
         $("#firstUnitIdLabel").text('Kg');
         $("#secondUnitIdLabel").text('Lbs');

     }
     else
     {
        if(sizeUnit == "Cm")
        {
            $("#firstUnitId").prop("checked", true);
         
            
        }

        if(sizeUnit == "Inch")
        {
            $("#secondUnitId").prop("checked", true);

        }



         $("#unitValueId").text('Size:');
         $("#firstUnitIdLabel").text('Cm');
         $("#secondUnitIdLabel").text('Inch');
     }
   }

function closeNav() 
{
     document.getElementById("mySidenav").style.width = "0";
     document.getElementById("main").style.marginLeft = "0px";
    // document.getElementById("sidenavLeft").style.width = "210px";

     $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
     $("#bodyPictureContainer").addClass('showUnselectedBody');

     selectedBodyPartClass = 'no_data';
}

function closeNavPhones() 
{
           modalIsShown = false;

     document.getElementById("bottomNav").style.height = "0";
   
    $("#footer").show();
    // document.getElementById("sidenavLeft").style.width = "210px";

    // $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
     //$("#bodyPictureContainer").addClass('showUnselectedBody');

   //  selectedBodyPartClass = 'no_data';
}


function saveBodyMeassureToDBPhone()
{
    var isError = false; 
    var date = $('#datePickerDataForBodyMeassureM').val();
   
    var size = $('#sizeIdM').val();
    var unit = $("input[name=typeMeassureM]:checked").val();
    
    console.log('UNIT ', unit);

    if(!isValidDate(date))
    {
      isError = true;
      alert('Wrong date format!');
    }

    if(!$.isNumeric(size))
    {
      isError = true;
      alert('Insert number only for body meassure!');
    }

    if(!isError)
    {
        $.ajaxSetup({
                  headers: 
                  {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

        if(selectedBodyPart == 'weight')
        {
            
            if(unit == 'second') // he selected lbs and we must transform in kg
            {
                size = Math.round(size * 0.45359237); // in kg
            }
        }
        else
        {
            if(unit == 'second') // we need to transform in inches 
            {
                size = Math.round(size * 2.54); // in kg
            }
        }

       



         $.ajax({
                   type:'POST',
                         url:'/addNewMeassurementToDB',
                         data:{userId:app_id,date:date,size:size,selectedBodyPart:selectedBodyPart,unit:unit},
                         success:function(data){
                         
                          alert(data);
                         if(data == 'Sucess!')
                         {
                              showBodyDataPhones();
                              closeNavPhones();
                              setTimeout("dataSuccesInsertedInDB()", 520);

                             
                         }
                         else
                         {
                            alert('Error. Try again!');
                         }
                       }
                    }); 
     }

}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};




function saveBodyMeassureToDB()
{
    var isError = false; 
    var date = $('#datePickerDataForBodyMeassure').val();
   
    var size = $('#sizeId').val();
    var unit = $("input[name=typeMeassure]:checked").val();
    
    console.log('UNIT ', unit);

    if(!isValidDate(date))
    {
      isError = true;
      alert('Wrong date format!');
    }

    if(!$.isNumeric(size))
    {
      isError = true;
      alert('Insert number only for body meassure!');
    }

    if(!isError)
    {
        $.ajaxSetup({
                  headers: 
                  {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

        if(selectedBodyPart == 'weight')
        {
            
            if(unit == 'second') // he selected lbs and we must transform in kg
            {
                size = Math.round(size * 0.45359237); // in kg
            }
        }
        else
        {
            if(unit == 'second') // we need to transform in inches 
            {
                size = Math.round(size * 2.54); // in kg
            }
        }

       



         $.ajax({
                   type:'POST',
                         url:'/addNewMeassurementToDB',
                         data:{userId:app_id,date:date,size:size,selectedBodyPart:selectedBodyPart,unit:unit},
                         success:function(data){
                         
                          alert(data);
                         if(data == 'Sucess!')
                         {
                              showBodyData();
                              closeNav();
                              setTimeout("dataSuccesInsertedInDB()", 520);

                             
                         }
                         else
                         {
                            alert('Error. Try again!');
                         }
                       }
                    }); 
     }

}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function dataSuccesInsertedInDB()
{

   if ( $(window).width() > 739) 
    {      
     showBodyData();
    }
    else
    {     
       

           showBodyDataPhones();
         
    }
     
     
    
}

function addBodyWeight()
{
              $("#bodyPictureContainer").removeClass(selectedBodyPartClass);
              $("#bodyPictureContainer").addClass('showUnselectedBody');


      selectedBodyPartClass = 'no_data';
      selectedBodyPart = 'weight';

      openNav('Body Weight');
}

function isValidDate(dateString)
   {
      var regEx = /^\d{4}-\d{2}-\d{2}$/;
      if(!dateString.match(regEx)) return false;  // Invalid format
      var d = new Date(dateString);
      if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
      return d.toISOString().slice(0,10) === dateString;
   }

