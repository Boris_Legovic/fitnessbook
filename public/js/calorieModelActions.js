
function loadCaloriesView()
{
	  $("#graphsHolder").addClass('hidden');
	  $("#bodyDataContainer").addClass('hidden');
       $("#mainId").addClass('hidden');


     $("#kcalContainer").removeClass('hidden');

$.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


		$.ajax({
           type:'POST',
           url:'/loadKcal',
           data:{id:app_id},
           success:function(data){

               var dataFrom = [];
               dataFrom = JSON.parse(data);
           		console.log('DFROM ',dataFrom);

           		lineChartKcal(dataFrom);
          
 
  			 }
        });
              
        
}

function closeKcalNav()
{
 	   document.getElementById("sideNavKcal").style.width = "0";
     document.getElementById("main").style.marginLeft = "0px";
     document.getElementById("sidenavLeft").style.width = "210px";
}

function runKcalInputNavbar()
{   
	 document.getElementById("sidenavLeft").style.width = "0";
 	 document.getElementById("sideNavKcal").style.width = "250px";
   document.getElementById("main").style.marginLeft = "-250px";

     
}

function saveKcal()
{
	$.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

         var kcalDate = $('#kcalDate').val();
         var kcalValue = $('#kcalVal').val();
         console.log('kcalValue ',kcalValue);

         if(isNumeric(kcalValue))
         {
         
		        	$.ajax(
					{
			           type:'POST',
			           url:'/insertKcal',
			           data:{id:app_id,kcalDate:kcalDate,kcalVal:kcalValue},
			           success:function(data)
			           {
			                
			           		if(data == "Error")
			           		{
			           			alert("Wrong date format!");
			           		}
			           		else
			           		{
			           			alert("Sucess!");
			           			closeKcalNav();
			           			setTimeout("loadCaloriesView()", 520);

			           		}
			                
			           	   
			            }
			        });
		}
		else
			{
				alert("Kcal value is not a number!");
			}
}

function isNumeric(value) {
    
    var isNumeric =  /^-{0,1}\d+$/.test(value);
   
   expr = /-/;  // no quotes here
   var isNotNegative =  !expr.test(value);

   if(isNumeric == true)
   {
   		if(isNotNegative == true)
   		{
   			return true;
   		}
   		else
   		{
   			return false;
   		}
   }
   else
   {
   	return false;
   }

 

}


