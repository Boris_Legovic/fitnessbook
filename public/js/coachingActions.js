

function showCoachingModal()
{


   var coachId = document.getElementById('coachId').value;
    
    $.ajaxSetup({
              headers: 
              {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });



 $('#onlineCoachingModal').remove();

        $.ajax({
               type:'POST',
               url:'/showCoachingModal',
               data:{coachId:coachId},
               success:function(data){

              $( 'body' ).append( data );
              $('#onlineCoachingModal').modal('show');
     
             }
            });


     
}

function acceptUserCoachRequest(id)
{

   $.ajaxSetup({
              headers: 
              {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

      $.ajax({
               type:'POST',
               url:'/aproveFreeCoaching',
               data:{id:id},
               success:function(data)
               {

                  alert('Sucess! Client is added to online coaching.');
                  location.reload();
     
             }
            });
}
