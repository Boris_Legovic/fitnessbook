

 function drawChart(dataArrayFromDB,graphType,meassurements,bodypart,isMobile) 
 {
       
       var dataArray = [];
       var exercizeName = ['Chest','Right biceps','Right forearm', 'Waist','Right glut','Right calv','Shouders','Left biceps','Left forearm','Hip', 'Left glut','Left calv','Neck','Body weight','Height'] ;
       var optionsArray = [];
 
       var doc1 = document.getElementById('bodyData1');
       var doc2 = document.getElementById('bodyData2');
       var doc3 = document.getElementById('bodyData3');
       var doc4 = document.getElementById('bodyData4');
       var doc_phones = document.getElementById('graphToShow');

      while (doc_phones.firstChild)
      {
         doc_phones.removeChild(doc_phones.firstChild);
      }

     
     while (doc1.firstChild)
      {
         doc1.removeChild(doc1.firstChild);
      }

      while (doc2.firstChild) 
      {
         doc2.removeChild(doc2.firstChild);
      } 

      while (doc3.firstChild) 
      {
          doc3.removeChild(doc3.firstChild);
      }

      while (doc4.firstChild)
      {
          doc4.removeChild(doc4.firstChild);
      }
   
       console.log('DATACHECK ', dataArrayFromDB);
       
      var arrayForData = [];

for(var d = 0; d<14; d++)
 {

       var objectArray = dataArrayFromDB[d]; 

          var sizeOf = 0;
         


          if(objectArray != null)
          {
              sizeOf =  objectArray.length;
          }

       if(sizeOf == 0)
       {
            arrayForData.push([["No data"],[]]);
          
       }
       else
       {

       

              

                 var size_unit = 1;
                 var weight_unit = 1;
                 
                 if(meassurements.size_unit == 'Inch')
                 {
                      size_unit = 0.393700787;
                 }  

                 if(meassurements.weight_unit == 'Lbs')
                 {
                     weight_unit = 2.20462262;
                 } 
        
                  var date = [];
                  var size = [];     
          
                  for(var i = 0; i<sizeOf; i++)
                  {
                        date.push(objectArray[i].date_of_meassure);
                       
                       if(d == 13)
                       {
                       
                         
                          size.push((objectArray[i].size_cm_or_kg * weight_unit).toFixed(1));  // here is body weight and we need to multiply by weight
                       }
                       else
                       {  

                           size.push((objectArray[i].size_cm_or_kg * size_unit).toFixed(1));  /// we multiply by size because we have body meassures
                       }




                       
                     
                  }

                    arrayForData.push([date,size]);
          }

        
      }

      var height = 0;



      if(dataArrayFromDB[14].length > 0) 
      {
          console.log('enterIn');
          var objectArrayHeight = dataArrayFromDB[14];
          var size =  objectArrayHeight.length-1;
          height = objectArrayHeight[size].size_cm_or_kg;

      }

       if(meassurements.size_unit == 'Inch')
       {
          height = (height * 0.393700787).toFixed(1);
          $('#heightVal').html(height + '\'');
       }
       else
       {
        $('#heightVal').html(height + ' Cm');
       }


      



        for(var m = 0; m<14; m++)
        {

            var divChartHolder;
           
            if(m<6)
            {
                divChartHolder = document.getElementById('bodyData1');
            }
             else
            {
                if(m ==12)
                {
                   divChartHolder = document.getElementById('bodyData3');
                    // top
                }
                else if(m == 13)
                {
                   divChartHolder = document.getElementById('bodyData4');
                     // bottom
                }
                else
                {
                  divChartHolder = document.getElementById('bodyData2');
                }
            }
           
          var chartDiv = document.createElement('canvas');

          if(isMobile)
          {
            divChartHolder = document.getElementById('graphToShow');
                              console.log(' HUHU ',exercizeName[m],' === ',' bodypart ',bodypart);

             if(exercizeName[m] == bodypart)
             {
               
               chartDiv.id = 'mobile_graph';
               chartDiv.style.backgroundColor = 'rgba(43, 38, 38, 0.05)';
               chartDiv.style.marginBottom = '5px';
               chartDiv.style.padding = '15px';
               chartDiv.style.borderRadius = '10px';

                divChartHolder.appendChild(chartDiv);
               

                var ctx = chartDiv.getContext("2d");


                
               loadGraphMobile(ctx,arrayForData[m]);

               break;
             }
          }
          else
          { 
           
               chartDiv.id = 'chart_div_body' + m;
               chartDiv.style.backgroundColor = 'rgba(43, 38, 38, 0.05)';
               chartDiv.style.marginBottom = '5px';
               chartDiv.style.padding = '15px';
               chartDiv.style.borderRadius = '10px';
           
         
          if(graphType == null)
          {
             graphType = 'line';
          }

 //       chartDiv.style.height = '100px';
        
              divChartHolder.appendChild(chartDiv);
               

            var ctx = chartDiv.getContext("2d");

              var gradientStroke = ctx.createLinearGradient(0, 0, 0, 300);
            gradientStroke.addColorStop(0, "#68EFAD");

            gradientStroke.addColorStop(1, "#ffffff");

            var myChart = new Chart(ctx, 
            {
            type: graphType,
            data: {
                labels: arrayForData[m][0],
                datasets: [{
                    label: exercizeName[m],
                    borderColor: gradientStroke,
                    pointBorderColor: gradientStroke,
                    pointBackgroundColor: gradientStroke,
                    pointHoverBackgroundColor: gradientStroke,
                    pointHoverBorderColor: gradientStroke,
                    pointBorderWidth: 10,
                    pointHoverRadius: 10,
                    pointHoverBorderWidth: 1,
                    pointRadius: 1,
                    fill: true,
                    backgroundColor: gradientStroke,
                    borderWidth: 4,
                    data: arrayForData[m][1]
                }]
            },
            options: {
                legend: {
                    position: "bottom"
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "rgba(0,0,0,0.5)",
                            fontStyle: "bold",
                           
                            maxTicksLimit: 5,
                            padding: 20
                            // suggestedMin: 120,
                            // suggestedMax: 125
                        },
                        gridLines: {
                            drawTicks: false,
                            display: false
                        }
        }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent"
        },
                        ticks: {
                            padding: 20,
                            fontColor: "rgba(0,0,0,0.5)",
                            fontStyle: "bold"
                        }
                    }]
                }
            }
          });
}
}
}
       

 function loadMuscleMassLvl(FM,LM,isPhone,meassurementType)
 {
    
    
        if(meassurementType != "Kg")
        {
            FM = FM * 2.20462262;

            LM = LM * 2.20462262;

            FM = Math.round(FM * 10)/10;

            LM = Math.round(LM * 10)/10;



        }


    if(isPhone)
    {
     
        if(meassurementType == "Kg")
        {
             new Chart(document.getElementById("bodyRatioM"),{"type":"pie","data":
        {labels: ["Fat kg", "Muscle kg"],"datasets":[{"label":"","data":[FM,LM],"backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});
        }
        else
        {
           new Chart(document.getElementById("bodyRatioM"),{"type":"pie","data":
        {labels: ["Fat lbs", "Muscle lbs"],"datasets":[{"label":"","data":[FM,LM],"backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});
        }


       
    
    }
    else
    {
      
      
       if(meassurementType == "Kg")
        {
             new Chart(document.getElementById("bodyRatio"),{"type":"pie","data":
            {labels: ["Fat kg", "Muscle kg"],"datasets":[{"label":"","data":[FM,LM],"backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});
        }
        else
        {

             new Chart(document.getElementById("bodyRatio"),{"type":"pie","data":
            {labels: ["Fat lbs", "Muscle lbs"],"datasets":[{"label":"","data":[FM,LM],"backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});
        }

    }


 }     

 function loadMuscleMassLvlEmpty (isPhone)
 {
    if(isPhone)
    {
     
       new Chart(document.getElementById("bodyRatioM"),{"type":"pie","data":
      {labels: ["Fat - no data", "Muscle - no data"],"datasets":[{"label":"","data":[50,50],"backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});
    }
    else
    {
     
         new Chart(document.getElementById("bodyRatio"),{"type":"pie","data":
      {labels: ["Fat - no data", "Muscle - no data"],"datasets":[{"label":"","data":[50,50],"backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});
    }

 }
      

function loadGraphMobile(ctx,arrayForData)
{


              var gradientStroke = ctx.createLinearGradient(0, 0, 0, 300);
            gradientStroke.addColorStop(0, "#68EFAD");

            gradientStroke.addColorStop(1, "#ffffff");

            var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: arrayForData[0],
        datasets: [{
            label: '',
            borderColor: gradientStroke,
            pointBorderColor: gradientStroke,
            pointBackgroundColor: gradientStroke,
            pointHoverBackgroundColor: gradientStroke,
            pointHoverBorderColor: gradientStroke,
            pointBorderWidth: 10,
            pointHoverRadius: 10,
            pointHoverBorderWidth: 1,
            pointRadius: 1,
            fill: true,
            backgroundColor: gradientStroke,
            borderWidth: 4,
            data: arrayForData[1]
        }]
    },
    options: {
        legend: {
            position: "bottom"
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "rgba(0,0,0,0.5)",
                    fontStyle: "bold",
                    maxTicksLimit: 5,
                    padding: 20
                },
                gridLines: {
                    drawTicks: false,
                    display: false
                }
}],
            xAxes: [{
                gridLines: {
                    zeroLineColor: "transparent"
},
                ticks: {
                    padding: 20,
                    fontColor: "rgba(0,0,0,0.5)",
                    fontStyle: "bold"
                }
            }]
        }
    }
  });
}


 function fitnessLevel(actualPositon, sum, viewIsForPhones)
    {
     
        var actual = Math.abs((actualPositon - 1)-sum);

        var positionPerc = (actual/sum) * 100;

        positionPerc = Math.round(positionPerc);
        
        // var actualPositionCalc = 100 - positionPerc;

        // var rez = sum - actualPositon + 1;

        // new Chart(document.getElementById("fitnessLevel"),{"type":"pie","data":
        // {labels: ["Rank", "of Athletes"],"datasets":[{"label":"","data":[rez,sum],
        // "backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});

        if(viewIsForPhones)
        {
            $('#percentageIdPhone').text( positionPerc + ' % ');

            $('#progressBarIdPhone').css('width',positionPerc + '%');

            $('#graphLvlValuePhone').text('Rank: ' + actualPositon + ' of ' + sum + ' athletes');
        }
        else
        {
            $('#percentageId').text( positionPerc + ' % ');

            $('#progressBarId').css('width',positionPerc + '%');

            $('#graphLvlValue').text('Rank: ' + actualPositon + ' of ' + sum + ' athletes');
        } 
          
       


    }   

     function fitnessLevelEmpty(sum)
    {


        new Chart(document.getElementById("fitnessLevel"),{"type":"pie","data":
        {labels: ["Rank - no data", "of Athletes"],"datasets":[{"label":"","data":[0,sum],
        "backgroundColor":["rgb(255, 99, 132)","rgb(75, 192, 192)"]}]}});


    } 
       
           
           
           
        

