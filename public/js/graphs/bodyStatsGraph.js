function loadBodyStatsGraph()
{
	 var chartDiv = document.getElementById('bodyStatsCanvas');
   
     var dates = [];
     var kcalData = [];


  var male = 495/(1.0324 - 0.19077 * Math.log10(80-25)  + 0.15456*Math.log10(178)) - 450;

  var female = 495/(1.29579 - 0.35004 * Math.log10(80+85-25)  + 0.22100*Math.log10(178)) - 450;

  var kg = 0;

  var height_cm = 0;

   


  var bmi = kg/(height_cm/100);

  

// waist-neck
// waist+hip- neck




console.log("BODYFAT ", male);


var arrayForData = [];

      if(arrayForData == null)
      {
        arrayForData = [[0,0]];
      }
      else
      {

        var size = arrayForData.length;
           for (var m = 0; m<size;m++)
           {
                var object = arrayForData[m];
               
                dates.push(object.date);
                kcalData.push(object.kcal);
           }

           arrayForData = [[dates],[kcalData]];
      }
            
                   
            console.log('arrayForData0 ',arrayForData[0][0]);
                        console.log('arrayForData1 ',arrayForData[1][0]);

                var ctx = chartDiv.getContext("2d");

                var gradientStroke = ctx.createLinearGradient(0, 0,0, 500);
                gradientStroke.addColorStop(0, "#FF0000");
                gradientStroke.addColorStop(1, "#ffffff");

                  var gradientStrokeMuscle = ctx.createLinearGradient(0, 0,0, 500);
                gradientStroke.addColorStop(0,"#00BFFF");
                gradientStroke.addColorStop(1, "#ffffff");


                var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                labels: ['Body fat','Muscle'],
                datasets: [{
                label: 'Kcal',
                
                fill: true,
                backgroundColor: ['#FF0000','#00BFFF'],
                borderWidth: 0,
                data: [90,30]
            }]

        },
        options: {
            legend: {
                position: "bottom"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 20
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
    }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent"
    },
                    ticks: {
                        padding: 20,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });






}