     

        var clientName = '';

        var firstWeight = null;
        var weightDifference = 0;

        var firstShoulder = null;
        var shoulderDifference = 0;

        var firstChest = null;
        var chestDifference = 0;

        var firstArms = null;
       
        var armDifference = 0;

        var firstWaist = null;
        var waistDifference = 0;

        var firstHip = null;
        var hipDifference = 0;

        var firstLegs = null;
        var legsDifference = 0;

        var firstRightBiceps = null;
        var rightBicepsDifference = 0;

        var firstLeftBiceps = null;
        var leftBicepsDifference = 0;


        var firstRightGlut = null;
        var rightGlutDifference = 0;

        var firstLeftGlut = null;
        var leftGlutDifference = 0;

        var glutDifference = 0;

        var clientHolderNumber = 1;



function resetValues()
{
        clientName = '';

       this.firstWeight = null;
       this.weightDifference = 0;

       this.firstShoulder = null;
       this.shoulderDifference = 0;

       this.firstChest = null;
       this.chestDifference = 0;

       this.firstArms = null;
       
       this.armDifference = 0;

       this.firstWaist = null;
       this.waistDifference = 0;

       this.firstHip = null;
       this.hipDifference = 0;

       this.firstLegs = null;
       this.legsDifference = 0;

       this.firstRightBiceps = null;
       this.rightBicepsDifference = 0;

       this.firstLeftBiceps = null;
       this.leftBicepsDifference = 0;


       this.firstRightGlut = null;
       this.rightGlutDifference = 0;

       this.firstLeftGlut = null;
       this.leftGlutDifference = 0;

       this.glutDifference = 0;


}

function fillGraphs()
{
                 var finalData = [shoulderDifference,chestDifference,armDifference,waistDifference,hipDifference,glutDifference,weightDifference];
                
                 var finalColors = [];

                 console.log('testclient ','client' + clientHolderNumber );
                 var chartDiv = document.getElementById('client' + clientHolderNumber);


                var ctx = chartDiv.getContext("2d");

                var gradientStrokeRed = ctx.createLinearGradient(0, 0,0, 600);
                gradientStrokeRed.addColorStop(0, "#ff8000");
                gradientStrokeRed.addColorStop(1, "#ffffff");


                var gradientStrokeBlue = ctx.createLinearGradient(0, 0,0, 600);
                gradientStrokeBlue.addColorStop(0, "#00BFFF");
                gradientStrokeBlue.addColorStop(1, "#ffffff");

                  for (var m = 0; m<7;m++)
                  {
                        if(finalData[m] < 0)
                        {
                            finalColors.push(gradientStrokeRed);
                            finalData[m] = Math.abs( finalData[m]);
                        }
                        else
                        {
                            finalColors.push(gradientStrokeBlue);
                        }

                  }


                

                var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                labels: ['Shoulder','Chest','Arms','Waist','Hip','Legs','Weight'],
                datasets: [{
                label: clientName,
               
                fill: true,
                backgroundColor: finalColors,
                data: finalData
            }]
        },
        options: {
            legend: {
                position: "top",
                display:false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 20
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
    }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent"
    },
                    ticks: {
                        padding: 20,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
}


function addEmptyGraph(number)
{
            var finalData = [0,0,0,0,0,0,0];
            var finalColors = [];

                 console.log('testclient ','client' + number );
                 var chartDiv = document.getElementById('emptyClient' + number);
                 var ctx = chartDiv.getContext("2d");

                var gradientStrokeRed = ctx.createLinearGradient(0, 0,0, 600);
                gradientStrokeRed.addColorStop(0, "#ff8000");
                gradientStrokeRed.addColorStop(1, "#ffffff");


                var gradientStrokeBlue = ctx.createLinearGradient(0, 0,0, 600);
                gradientStrokeBlue.addColorStop(0, "#00BFFF");
                gradientStrokeBlue.addColorStop(1, "#ffffff");

                  for (var m = 0; m<7;m++)
                  {
                        if(finalData[m] < 0)
                        {
                            finalColors.push(gradientStrokeRed);
                            finalData[m] = Math.abs( finalData[m]);
                        }
                        else
                        {
                            finalColors.push(gradientStrokeBlue);
                        }

                  }

                var myChart = new Chart(ctx,
               {
                type: 'bar',
                data: {
                labels: ['Shoulder','Chest','Arms','Waist','Hip','Legs','Weight'],
                datasets: [{
                label: clientName,
               
                fill: true,
                backgroundColor: finalColors,
                data: finalData
            }]
        },
        options: {
            legend: {
                position: "top",
                display:false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 20
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
    }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent"
    },
                    ticks: {
                        padding: 20,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
}


function showMeassuresGraphs(arrayForData)
{

     console.log("ArrayForData ",arrayForData); 

      var size = arrayForData.length;

      var weight = 0;

      if(arrayForData == null)
      {
        arrayForData = [[0,0]];
      }
      else
      {

            var currentUserId = arrayForData[0].user_customer_id;
            clientName =  arrayForData[0].name + ' ' +  arrayForData[0].lastname;

          
           for (var m = 0; m<size;m++)
           {
                var object = arrayForData[m];
                
                if(currentUserId != object.user_customer_id)
                {
                   // we switched user, must update new graph
                   currentUserId = object.user_customer_id;
                   fillGraphs();
                   resetValues();
                   clientName = object.name + ' ' + object.lastname;

                   clientHolderNumber ++;
                }
               

                



              if(object.exercize_name_or_weight == 'weight')  
              {
                    if(firstWeight == null)
                    {
                       firstWeight = object.size_cm_or_kg;  
                       console.log('times');  
                    }

                    else
                    {
                        weightDifference = object.size_cm_or_kg - firstWeight;

                    }
               }

               if(object.exercize_name_or_weight == 'chest')  
              {
                    if(firstChest == null)
                    {
                       firstChest = object.size_cm_or_kg; 
                        console.log('firstChestNULL ', firstChest);
   
                    }

                    else
                    {
                      console.log('firstChestInElse ', firstChest);
                      console.log('object.size_cm_or_kgInElse ', object.size_cm_or_kg);

                        
                        chestDifference = (object.size_cm_or_kg - firstChest);
                        console.log('chestDifferenceInElse ', chestDifference);
                      //  fillGraphs();

                    }
               }

                if(object.exercize_name_or_weight == 'waist')  
               {
                     if(firstWaist == null)
                     {
                        firstWaist = object.size_cm_or_kg;    
                     }

                     else
                     {
                         waistDifference = object.size_cm_or_kg - firstWaist;

                     }
                }

                 if(object.exercize_name_or_weight == 'hip')  
                {
                      if(firstHip == null)
                      {
                         firstHip = object.size_cm_or_kg;    
                      }

                      else
                      {
                          hipDifference = object.size_cm_or_kg - firstHip;

                      }
                 }

                  if(object.exercize_name_or_weight == 'shoulders')  
                 {
                       if(firstShoulder == null)
                       {
                          firstShoulder = object.size_cm_or_kg;    
                       }

                       else
                       {
                           shoulderDifference = object.size_cm_or_kg - firstShoulder;

                       }
                  }

                 

                 

                   if(object.exercize_name_or_weight == 'left_biceps')  
                   {
                         if(firstLeftBiceps == null)
                         {
                            firstLeftBiceps = object.size_cm_or_kg;    
                         }

                         else
                         {
                             leftBicepsDifference = object.size_cm_or_kg - firstLeftBiceps;
                             armDifference = rightBicepsDifference + leftBicepsDifference;


                         }
                    }

                   if(object.exercize_name_or_weight == 'right_biceps')  
                   {
                         if(firstRightBiceps == null)
                         {
                            firstRightBiceps = object.size_cm_or_kg;    
                         }

                         else
                         {
                             rightBicepsDifference = object.size_cm_or_kg - firstRightBiceps;
                             armDifference = rightBicepsDifference + leftBicepsDifference;

                         }
                    }


                    if(object.exercize_name_or_weight == 'left_glut')  
                    {
                          if(firstLeftGlut == null)
                          {
                             firstLeftGlut = object.size_cm_or_kg;    
                          }

                          else
                          {
                              leftGlutDifference = object.size_cm_or_kg - firstLeftGlut;
                              glutDifference = rightGlutDifference + leftGlutDifference;


                          }
                     }

                    if(object.exercize_name_or_weight == 'right_glut')  
                    {
                          if(firstRightGlut == null)
                          {
                             firstRightGlut = object.size_cm_or_kg;    
                          }

                          else
                          {
                              rightGlutDifference = object.size_cm_or_kg - firstRightGlut;
                              glutDifference = rightGlutDifference + leftGlutDifference;

                          }
                     }
                     

                    
                      if((m+1) == size) // last value
                      {   
                           clientName = object.name + ' ' + object.lastname;
                           fillGraphs();
                      }
                  } // here end the for loop


           
          
      }
            
                   
               
}
