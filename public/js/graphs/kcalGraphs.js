     
function lineChartKcal(arrayForData)
{

     console.log("ArrayForData ",arrayForData); 

     var chartDiv = document.getElementById('kcalCanvas');
   
     var dates = [];
     var kcalData = [];

      if(arrayForData == null)
      {
        arrayForData = [[0,0]];
      }
      else
      {

        var size = arrayForData.length;
           for (var m = 0; m<size;m++)
           {
                var object = arrayForData[m];
               
                dates.push(object.date);
                kcalData.push(object.kcal);
           }

           arrayForData = [[dates],[kcalData]];
      }
            
                   
            console.log('arrayForData0 ',arrayForData[0][0]);
                        console.log('arrayForData1 ',arrayForData[1][0]);

                var ctx = chartDiv.getContext("2d");

                var gradientStroke = ctx.createLinearGradient(0, 0,0, 600);
                gradientStroke.addColorStop(0, "#ff8000");
                gradientStroke.addColorStop(1, "#ffffff");

                var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                labels: arrayForData[0][0],
                datasets: [{
                label: 'Kcal',
                borderColor: gradientStroke,
                pointBorderColor: gradientStroke,
                pointBackgroundColor: gradientStroke,
                pointHoverBackgroundColor: gradientStroke,
                pointHoverBorderColor: gradientStroke,
                pointBorderWidth: 10,
                pointHoverRadius: 10,
                pointHoverBorderWidth: 1,
                pointRadius: 1,
                fill: true,
                backgroundColor: gradientStroke,
                borderWidth: 4,
                data: arrayForData[1][0]
            }]
        },
        options: {
            legend: {
                position: "bottom"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 20
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
    }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent"
    },
                    ticks: {
                        padding: 20,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });

}