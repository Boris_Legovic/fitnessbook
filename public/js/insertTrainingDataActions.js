
 var allData = [];
	
  // we check for validation of empty fields
  function setTrainingDataToDatabase(dataArray,trainingId)
  {
    
       var isError = false;
     
       var date = $('#datePickerData').val();

      

      for(var i = 0; i<dataArray.length; i++ )
    {
     
          for(var j = 0; j<dataArray[i][1]; j++ )
           {
             var kg = $( "#" + dataArray[i][0] + 'kg' + j  ).val();
             var rep = $( "#" + dataArray[i][0] + 'rep' + j  ).val();
          
             if(kg == '')
             {
                alert('Kg input empty or invalid!'); // TODO check if this value is number also
                isError = true;
                break;
             }

              if(rep == '')
             {
                alert('Rep input empty or invalid!');
                isError = true;
                break;
             }
  
           }

          
    }  

     if(!isError)
           {
              insertTrainingDataToDBInside(dataArray,trainingId,date);
           }

  }



 // call this funcion inside this blade only
  function insertTrainingDataToDBInside(dataArray,trainingId,date)
	{
		
    allData = [];
    
    for(var i = 0; i<dataArray.length; i++ )
    {
     
            var kg_reps = [];
            var repsAndKgContainer = [];
           for(var j = 0; j<dataArray[i][1]; j++ )
           {
             kg_reps.push($( "#" + dataArray[i][0] + 'kg' + j  ).val());
             kg_reps.push($( "#" + dataArray[i][0] + 'rep' + j  ).val());
             repsAndKgContainer.push(kg_reps);
             kg_reps = [];


           }
            var insiderDataArray = [];
            insiderDataArray.push(dataArray[i][0]);
            insiderDataArray.push(repsAndKgContainer);
            repsAndKgContainer = [];
            

            allData.push(insiderDataArray);

                         // example  bench, 20 kg, 3 reps.. [1,[20,3],[22,5]]
           
          
    }

          

           $.ajax({
           type:'POST',
           url:'/addNewTrainingDataToDB',
           data:{trainingData:allData,trainingDate:date,trainingId:trainingId},
           success:function(data){
           if(data == 'Sucess!')
           {
               
                $('#modalDataInsertion').modal('toggle');
           }
           else
           {
              alert('Error. Try again!');
           }
           

            
             
           }
        }); 

	}

 

