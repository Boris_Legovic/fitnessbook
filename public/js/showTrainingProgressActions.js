

	var lengthOfHeader = 0;
	var chartDivIds = [];
  var trainingListSelectedId = 0;
  var clientId = 0;

function setSelectedIdClient(id)
{
   

     var docPrev = document.getElementById(clientId);
   
    if(docPrev != null)
    {
      docPrev.style.backgroundColor = "white";
    }

    var doc = document.getElementById(id);
    doc.style.backgroundColor = "#68EFAD";
    clientId = id;

}






function loadTrainingListFromDB(buttonPressed)
{


  clientId = 0;
  trainingListSelectedId = 0;

  if(buttonPressed > 0)
  {
     clientId = buttonPressed;
  }
     
     $('#showTrainingListModal').remove();
     $('#modalDataInsertion').remove();
      var url = '/loadTrainingList';

      $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
          
           $.ajax({
           type:'POST',
           url:url,
           data:{id:app_id,buttonPressedId:buttonPressed},
           success:function(data){
                $( 'body' ).append( data );
                $('#showTrainingListModal').modal('show');
              
           }
        });
}



function showModalForTrainingIputData()
{

   $('#showTrainingListModal').modal('toggle');


  $('#modalDataInsertion').remove();

    
    $.ajax({
           type:'POST',
           url:'/loadTrainingDataInsert',
           data:{id:trainingListSelectedId},
           success:function(data){
              $( 'body' ).append( data );
              $('#modalDataInsertion').modal('show');
           }
        });
}

function deleteTraining()
{
     if(trainingListSelectedId == 0)
     {
         alert('Please select training!');
     }
     else
     {  

         $.ajax({
               type:'POST',
               url:'/deleteTraining',
               data:{id:trainingListSelectedId},
               success:function(data){
                 
                  $('#'+trainingListSelectedId).remove();
                  trainingListSelectedId = 0;
               }
            });
      }
}

function sendTraining()
{
     $('#showClientList').modal('toggle');
     $('#showTrainingListModal').modal('toggle');
      

    $.ajax({
               type:'POST',
               url:'/sendTraining',
               data:{id:trainingListSelectedId, customerId:clientId},
               success:function(data)
               {
                   
                   alert('Sucess!');
                  
               }
            });
}

function showProgressView()
{
  if(trainingListSelectedId > 0)
  {

    var i = document.createElement("input"); //input element, text
    i.setAttribute('type',"text");
    i.setAttribute('name',"idOfTraining");
    i.setAttribute('value',trainingListSelectedId);

    var f = document.getElementById('visualizeTrainingId');

    f.appendChild(i);

    f.submit();
  }
  else
  {
    alert('Please select training to visualise.');
  }
}

function setSelectedId(id)
{

     
    var docPrev = document.getElementById(trainingListSelectedId);
   
    if(docPrev != null)
    {
      docPrev.style.backgroundColor = "white";
    }

    trainingListSelectedId = id;
    var doc = document.getElementById(id);
    doc.style.backgroundColor = "#68EFAD";
}

function openClientList()
{
      $('#showClientList').remove();
      if(trainingListSelectedId > 0)
      {

      $('#showTrainingListModal').modal('toggle');
      $('#showClientList').remove();

        
        $.ajax({
               type:'POST',
               url:'/loadClientList',
               data:{id:trainingListSelectedId},
               success:function(data){
                  $( 'body' ).append( data );
                  $('#showClientList').modal('show');
               }
            });
      }
      else
      {
        alert('Select training first!');
      }  
} 


function showTrainingProgress()
{

	   if(trainingListSelectedId > 0)
     {
       //hideBodyData();
        $.ajaxSetup({
          headers: 
          {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

	// $('#showTrainingListModal').modal('toggle');
   
    $.ajax({
           type:'POST',
           url:'/loadTrainingProgress',
           data:{id:trainingListSelectedId},
           success:function(data){
             var dataFormated =  JSON.parse(data[0]); 
             var weightUnit = data[1];
            
             //console.log('DATAJSON-- ' + JSON.parse(data));

             clearGraphs();

            for(var pointer = 0; pointer<dataFormated.length;++pointer)
            {

                           console.log('DATAFROMDBTRAINING-- ' +dataFormated);

                var singleData = addDataForSingleGraph(dataFormated,pointer,weightUnit);
                 
             //   console.log('singleDataForDraw ',singleData); 

                drawStuff(pointer,singleData, dataFormated[pointer][0],weightUnit);
            } 
 
  			 }
        });
  }
  else
  {
    alert('Select training to show progress.');
  }
}

function addDataForSingleGraph(data,pointer,weightUnit)
{
      var arrayOfOneRow = [];

      var dataRowGraph = [];

      var dataLength = data[pointer][1].length;

      console.log('dataLength ', dataLength) ;

       console.log('testFirst', data[0]) ;
    
      console.log('data[pointer][1] ', dataLength) ;

      for(var i = 0; i<dataLength; ++i)
      {

            var dateOfEx = data[pointer][1][i][0];
            var setofEx = parseInt(data[pointer][1][i][2]);     
            var weightofEx =  parseInt(data[pointer][1][i][1]);
       
        
           if(i>0) // ako je smo na bilo kojoj poziciji koja nije prvi element
           {
                               //   console.log('checkCrash1 ', data[1][1][i-1][0]);

              //    console.log('checkCrash ', data[1][1][i-1]);
                 
                  if(data[pointer][1][i-1][0] == dateOfEx) // ako si mi sadasnji i prethodni isti datum
                  {
                      arrayOfOneRow.push(setofEx);
                      arrayOfOneRow.push(weightofEx);
                      console.log("prvi ", weightofEx);
                      
                       if(i == (dataLength-1)) // doso do kraja petlje
                       {
                              console.log("zadnji");

                            if(lengthOfHeader < arrayOfOneRow.length)
                            {
                              lengthOfHeader = arrayOfOneRow.length;
                            }
                            dataRowGraph.push(arrayOfOneRow);
                           console.log("zanji_row ", arrayOfOneRow);

                            arrayOfOneRow = [];
                       }
                        
                  }
                  else
                  { // ako datumi nisu isti onda je drugi trening
                    if(lengthOfHeader < arrayOfOneRow.length)
                    {
                      lengthOfHeader = arrayOfOneRow.length;
                    }
                   
                    dataRowGraph.push(arrayOfOneRow);
                    arrayOfOneRow = [];
                                 


                     arrayOfOneRow.push(dateOfEx);
                     arrayOfOneRow.push(setofEx);
                     arrayOfOneRow.push(weightofEx);

                    if((i+1) == dataLength) // we are at the end of array
                    {
                      dataRowGraph.push(arrayOfOneRow);
                      arrayOfOneRow = [];
                        console.log('zadnji');
                    }
                  }
        }
        else if(i==0)
        {  // ako je prvi podatak sve dodajem 
            arrayOfOneRow.push(dateOfEx);
            arrayOfOneRow.push(setofEx);
            arrayOfOneRow.push(weightofEx);
            if((i+1) == dataLength) // only one data, we check if its end
            {
               dataRowGraph.push(arrayOfOneRow);
                          console.log('neverEnter1');

               //we have only one data and we need to add to main array
            }  
            // if(only one data)
             lengthOfHeader = arrayOfOneRow.length; // dodao 09.01.2018

                        

        }

      } 

      var headerArray = [];
      headerArray.push('date');
        
        console.log("headerLen0 " + lengthOfHeader);


      var forLoopIteratorEnd = (lengthOfHeader - 1)/2;

      for(var m = 0; m<forLoopIteratorEnd; m++)
      {
        console.log("headerLen " + lengthOfHeader);

           var unit = 'kg ';
           if(weightUnit == 'Lbs')
            {
              unit = 'lbs ';
            }


         
            var numb = m + 1;
            console.log("headerLe1 " + lengthOfHeader);
             headerArray.push(unit + numb);
             headerArray.push('rep ' + numb);
          
          
  
      }

      console.log('headerCheck ',headerArray );

      dataRowGraph.unshift(headerArray);
     
      return dataRowGraph;
}

function hideBodyData()
{
    //var element = document.getElementById('bodyDataContainer');
      $("#bodyDataContainer").addClass('hidden');
      $("#kcalContainer").addClass('hidden');


      $("#graphsHolder").removeClass('hidden');


}
  
