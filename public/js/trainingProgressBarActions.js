

function drawStuff(chartNumber,exercizeDataRow, exercizeName,weightUnit) {

        var divChartHolder = document.getElementById('graphsHolder');
        var chartDiv = document.createElement('div');
        chartDiv.id = 'chart_div' + chartNumber;
        chartDivIds.push(chartDiv.id);
        chartDiv.style.width = '100%';
        chartDiv.style.height = '200px';

       
  
         divChartHolder.append(chartDiv);
//        var chartDiv = document.getElementById('chart_div' + chartNumber);
        console.log('exercizeDataRow ' + exercizeDataRow +  ' exercizeName ' + exercizeName);
        var data = google.visualization.arrayToDataTable(exercizeDataRow);
        console.log('dataTest ' + data);

        var colorRed = '#ff8000';
        var colorBlue = '#00BFFF';
       
       // alert(this.weightUnit);

        var axisTitle = 'Kg';

        if(weightUnit == 'Lbs')
        {
            axisTitle = 'Lbs';
        }

       
        var classicOptions = {
          width: '100%',
          legend: {textStyle: {color: '#FFA500',fontSize:11}},
          backgroundColor: "transparent",titleTextStyle: {
    color: '#4169E1'
},




          series: {
            0: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            1: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            2: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            3: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            4: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            5: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            6: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            7: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            8: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            9: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            10: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            11: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            12: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            13: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            14: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            15: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            16: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            17: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            18:{targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            19: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            20: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            21: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            22: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            23: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            24: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            25: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true},
            26: {targetAxisIndex: 0,color: colorRed, visibleInLegend: true},
            27: {targetAxisIndex: 1,color: colorBlue, visibleInLegend: true}
            
            
          },
          title: exercizeName,
          vAxes: {

            // Adds titles to each axis.
            0: {title: axisTitle, titleTextStyle: {color: '#FF4500',fontSize:11},textStyle:{color:'#FF4500',fontSize:11}},
            1: {title: 'Reps',titleTextStyle: {color: '#4169E1',fontSize:11},baselineColor:'transparent',textStyle:{color:'#4169E1',fontSize:11}}
          },
           hAxis: {textStyle:{color:'#FF7F50',fontSize:11}}
        };



        function drawClassicChart() 
        {
          var classicChart = new google.visualization.ColumnChart(chartDiv);
          classicChart.draw(data, classicOptions);
         
        }

        drawClassicChart();
    }

function clearGraphs()
{
  var graphLength =  chartDivIds.length;
  for(var i = 0; i<graphLength;++i)
  {
     var element = document.getElementById(chartDivIds[i]);
     element.parentNode.removeChild(element);
  }

   chartDivIds = [];
}    


// function drawStuff(chartNumber,exercizeDataRow, exercizeName) 
// {

//     var divChartHolder = document.getElementById('graphsHolder');
//         var chartDiv = document.createElement('canvas');
//         chartDiv.id = 'chart_div' + chartNumber;
//         var id = 'chart_div' + chartNumber;
//         chartDivIds.push(chartDiv.id);
      
// console.log('exercizeDataRow',exercizeDataRow);
// console.log('name',exercizeName);


//  var ctx = chartDiv.getContext("2d");

//           var gradientStroke = ctx.createLinearGradient(0, 0,0, 5000);
//              gradientStroke.addColorStop(0, "#80b6f4");
//              gradientStroke.addColorStop(1, "#ffffff");

//          var gradient = ctx.createLinearGradient(0, 0, 0, 30);
// gradient.addColorStop(1, 'rgba(250,174,50,1)');   
// gradient.addColorStop(0, 'rgba(250,174,50,0)');
  
//          divChartHolder.append(chartDiv);





  
// new Chart(chartDiv, {
//   type: 'bar',
//   data: {
//     labels: ['7-1-2011','8-3-2022'],
//     datasets: [{
//       label: 'A',
//       backgroundColor:gradientStroke,
//       yAxisID: 'A',
     
//       data: [200, 100, 50, 30,20]
//     }, {
//       label: 'B',
//       yAxisID: 'B',
//       data: [20, 10, 5, 3, 2]
//     },{
//       label: 'A',
//       yAxisID: 'A',
//       data: [300, 350, 150, 130, 120]
//     },{
//       label: 'B',
//       yAxisID: 'B',
//       data: [30, 35, 15, 13, 12]
//     }]
//   },
//   options: {
//     scales: {
//       yAxes: [{
//         id: 'A',
//         type: 'linear',
//         position: 'left',
//       }, {
//         id: 'B',
//         type: 'linear',
//         position: 'right',
//         ticks: {
         
//         }
//       }]
//     }
//   }
// });







// }

// function clearGraphs()
// {
//   var graphLength =  chartDivIds.length;
//   for(var i = 0; i<graphLength;++i)
//   {
//      var element = document.getElementById(chartDivIds[i]);
//      element.parentNode.removeChild(element);
//   }

//    chartDivIds = [];
// }    

//        