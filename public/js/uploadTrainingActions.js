
var isFree = true;


$( document ).ready(function() {

$("#trainingImageId").click(function() {
    $("#fileImageId").click();
    //$('#imageNameId').html($("#fileImageId").val());


});

$("#fileImageId").change(function(e) {
		   /// $('#imageNameId').html($("#fileImageId").val());
		    // readURL(this);


});



$("#fileTrainingButtonId").click(function() {
    $("#fileTrainingId").click();
    //$('#imageNameId').html($("#fileImageId").val());


});

$("#fileTrainingId").change(function(e) {
		    $('#upladTrainingTextId').html($("#fileTrainingId").val());


});


(function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else 
                {
                    options.success();

                }

            });

        });
    };

})(jQuery);


// check image type //
$(function() {
    $('#fileImageId').checkFileType({
        allowedExtensions: ['jpg', 'jpeg','png'],
        success: function() {
            //alert('Success');
            var element = document.getElementById("fileImageId");
            readURL(element);
             $('#imageNameId').html($("#fileImageId").val());

        },
        error: function() {
            alert('Error');
        }
    });

});

});


// check image type end 


function readURL(input)
{
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) 
            {
                $('#trainingImageId').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
}

        isError = true;

function uploadComercialTraining()
{

  
    var price = document.getElementById("price").value;
    var title = document.getElementById("title").value;
    var description = document.getElementById("descriptionId").value;
    var file = document.getElementById("fileTrainingId").value;
     var image = document.getElementById("fileImageId").value;
     var isError = false;

       price = price.replace(",",".");

     console.log("title ", title);
     console.log("description ", description);
     console.log("file ", file);
     console.log("image ", image);


    if(title == "")
    {
        alert('Enter title!');
        isError = true;

    }

    if(description == "")
    {
        alert('Enter description!');
        isError = true;


    }

    if(image == "")
    {
        alert('Upload image!');
        isError = true;


    }

    if(file == "")
    {
        alert('File is not uploaded!');
        isError = true;


    }
  
    if(!isFree)
    {

            if(!isNumeric(price))
            {
                alert('Price is not a valid number!');
            }
            else
            {
                if(!isError)
                {   


                   if(parseFloat(price) == 0 || parseFloat(price) < 0)
                   {
                      alert('Price is too low.');
                   } 
                   else
                   {
                        console.log('formTraining');
                         $( "#formTraining" ).submit();
                   }
                }
            }
    }
    else
    {
        $("#price").val(0);
         if(!isError)
         { 
            $( "#formTraining" ).submit();
         }
    }
   
}

// $( "#formTraining" ).submit(function( event ) {
//     event.preventDefault();
//    // uploadComercialTraining();

// });


    var price = document.getElementById("price");


price.addEventListener("input", function (e) {
     var price = this.value.replace(",",".");
      $( "#price" ).val(price);
});

// $( "#price" ).change(function() {
//          var price = document.getElementById("price").value;
//                 price = price.replace(",",".");
//                 $( "#price" ).val(price);
//                 alert();

// });


function isNumeric(value) {
    

    var isNumeric =  parseFloat(value);
   
   if(isNumeric >= 0)
   {
       isNotNegative = true;
       return true;
   }
   else
   {
       isNotNegative = false;
       return false;
   }
  
  
    
}

function showPaidOptions()
{

     var divStripe = document.getElementById('stripeState').getAttribute('value');

     if(divStripe != 'false')
     {
         $("#priceRow").removeClass('hidden');
         isFree = false;
     }
     else
     {
         document.getElementById("firstUnitId").checked = true;
        alert('Go to profile settings and connect with stripe account to recive payments!');
     }
}

function hidePaidOptions()
{
     $("#priceRow").addClass('hidden');
     $("#price").val(0);
     isFree = true;
}
    
    

