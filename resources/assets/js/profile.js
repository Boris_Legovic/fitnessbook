require('./bootstrap');
window.Vue = require('vue');




const app = new Vue({
    el: '#app1',
    data: {
   msg: 'Click on user from left side:',
   content: '',
   privsteMsgs: [],
   singleMsgs: [],
   userToData: [],
   msgFrom: '',
   conID: '',
   friend_id: '',
   seen: false,
   newMsgFrom: '',
   idOfUserToRecive:'',
   loggedInUserId:'',
   messageNotificationsData:'0',
   notifications:'0',
   notificationsRequest:'0',

   userBoughtCoachingData:[]

 },

 ready: function()
 {
    
  // this.created(); 
 },

 created()
 {
          console.log('createdTest'); // show if success

    // get logged in user id
      axios.post('/getLoggedInUserId')
        .then(response => {
          // console.log('getUser',response.data); // show if success

          app.loggedInUserId = response.data; 


                    // console.log('**app.loggedInUserId**',app.loggedInUserId);

          //we are putting data into our posts array
            //Echo.private('message.' + app.loggedInUserId)

            Echo.private('message.' + app.loggedInUserId)

          .listen('MessageSent', (e) => {
             // console.log('listenerMSG',e.messageData[0]);
             // console.log('TMTT',app.conID," is equal ",e.messageData[0].coach_id);

             // console.log('app.conID',app.conID," e.messageData[0].coach_id ",e.messageData[0].coach_id);
           
             // console.log('**app.loggedInUserId',app.loggedInUserId,"**e.messageData[0].conversation_id ",e.messageData[0].conversation_id);


             //   console.log('@@app.conID',app.conID," == ",e.messageData[0].conversation_id);
             //   console.log('@@app.loggedInUserId',app.conID," == ",e.messageData[0].coach_id);
               console.log('messageTest ', e.messageData[0]);
              
              if(e.messageData[0].hasOwnProperty("trainer_to_recive_request_id"))
              { 
                //this is when user requested a free coaching
                 if(app.loggedInUserId == e.messageData[0].trainer_to_recive_request_id)
                    {
                         this.updateNotificationRequest();
                        // this.updateNotification(e.messageData[0].trainer_to_recive_request_id);
                       //  app.userBoughtCoachingData.push(e.messageData[0]);

                    }
                 // check 
              }
              else
              {

           // here i revice the single message from 
               if(app.conID == e.messageData[0].conversation_id )
               {
                   
                   console.log('update_message ');
                   app.singleMsgs.push(e.messageData[0]); 
                   this.scrollToEnd();
                   this.reorderListOfChats();
                }

                else if(app.loggedInUserId == e.messageData[0].coach_id)
              // else if(app.conID == e.messageData[0].coach_id)
                {
                    console.log('update_notifcation ',e.messageData[0].coach_id);
                    this.updateNotification(e.messageData[0].coach_id);
                    app.userBoughtCoachingData.push(e.messageData[0]);
                }
               
                else
                {
                     console.log('update_notification_only');
                     this.updateMessageNotification(e.messageData[0].conversation_id);
                     this.reorderListOfChats();
                }
              }

           });
        })
        .catch(function (error)
         {
            console.log(error); // run if we have error
        });

        // get all notifications

         axios.get('/getNotifications')
        .then(response => {
          //console.log(response.data); // show if success
          app.userBoughtCoachingData = response.data;
                    console.log('app.userBoughtCoachingData  -- reps', response.data);

          console.log('app.userBoughtCoachingData ',app.userBoughtCoachingData);
           ///we are putting data into our posts array
        })
        .catch(function (error)
         {
            console.log('some_error ',error); // run if we have error
        });
    

       axios.get('/getNewMessageUser')
        .then(response => {
          //console.log(response.data); // show if success
          app.conID = response.data;
          console.log('responseDATAID', app.conID  );
                    
           ///we are putting data into our posts array
        })
        .catch(function (error)
         {
            console.log('some_error ',error); // run if we have error
        });
    
   




        axios.get('/getMessages')
        .then(response => {

          console.log('fillInitMsg',response.data); // show if success
          app.singleMsgs = response.data; //we are putting data into our posts array
        })
        .catch(function (error)
         {
            console.log('EROR ',error); // run if we have error
        });

         axios.post('/idOfUserToRecive')
        
        .then(response => {
         // console.log('responseFromUsetToRecive' ,response.data); // show if success
          //we are putting data into our posts array
          if(response.data != '')
          {
            this.messages(response.data);
          }

        })
        .catch(function (error)
         {
            console.log(error); // run if we have error
        });

        
        this.reorderListOfChats();

        this.updateNotificationRequest();


 },


 methods:{


     updateNotificationRequest:function()
    {
           axios.post('/updateNotificationRequest', {
                 updateNotificationsOfUserId:'0'
            })
            .then( (response) => {              
            //  console.log(response.data); // show if success
              if(response.status===200)
              {
                  
                  app.notificationsRequest = response.data;
                  console.log('notificationReq ', response.data);

              }

            })
            .catch(function (error) {
              console.log('errorNotification ',error); // run if we have error
            });
    },

    
    getUserConversationPicture:function(id)
    {
         axios.get('/getUserConversationPicture/' + id)
          .then(response => {
            console.log("testisi " + response.data); // show if success
            app.userToData = response.data; //we are putting data into our posts array
            this.scrollToEnd();
          })
          .catch(function (error) {
            console.log(error); // run if we have error
          });
    },

    updateNotification:function(id)
    {
           axios.post('/updateNotification', {
                 updateNotificationsOfUserId:id
            })
            .then( (response) => {              
            //  console.log(response.data); // show if success
              if(response.status===200)
              {
                  
                  app.notifications = response.data;
                  console.log('notification ',app.notifications);

              }

            })
            .catch(function (error) {
              console.log('errorNotification ',error); // run if we have error
            });
    },

    updateMessageNotification:function(id)
    {
           axios.post('/updateMessageNotification', {
                 updateNotificationsOfUserId:id
            })
            .then( (response) => {              
            //  console.log(response.data); // show if success
              if(response.status===200){
                
                  app.messageNotificationsData = response.data;
                 // console.log('singleMessage ', response.data);

              }

            })
            .catch(function (error) {
              console.log(error); // run if we have error
            });
    },

    scrollToEnd: function() 
    {     
        var container = this.$el.querySelector("#chatScroll");
        container.scrollTop = container.scrollHeight;
    },

     notification: function(id)
   {
    
     
     axios.get('/getMessages/' + id)
          .then(response => {
           // console.log(response.data); // show if success
         

          })
          .catch(function (error) {
            console.log(error); // run if we have error
          });
   },

   messages: function(id)
   {
    
     
     axios.get('/getMessages/' + id)
          .then(response => {
           // console.log(response.data); // show if success
           app.singleMsgs = response.data; //we are putting data into our posts array
           console.log('checkConsoleDataOnGET ', app.singleMsgs);
           app.conID = id;
           this.getUserConversationPicture(id);
           this.updateMessageNotification(id);
            this.reorderListOfChats();


          })
          .catch(function (error) {
            console.log(error); // run if we have error
          });
   },

   inputHandler(e){
     if(e.keyCode ===13 && !e.shiftKey){
       e.preventDefault();
        this.sendMsg();
     }
   },
   sendMsg()
   {
     if(this.msgFrom){
       
       // if(this.conID == '')
       // {
       //    alert('Select user to message!');
       // }
       // else
       // {
            console.log('**this.conID',this.conID);
                 console.log('**this.msgFrom',this.msgFrom);
               axios.post('/sendMessage', {
              conID: this.conID,
              msg: this.msgFrom
                


            })
            .then( (response) => {              
            //  console.log(response.data); // show if success
              if(response.status===200){
                this.msgFrom = ''; // clean the input field
                 app.singleMsgs.push(response.data[0]);
                 // console.log('singleMessage ', response.data);

               // this.created();
               
               this.scrollToEnd();
               this.reorderListOfChats();
              }

            })
            .catch(function (error) {
              console.log(error); // run if we have error
            });

     }
   // }
   },

   setReciverAndMsg(e)
   {
       
       if(e.keyCode ===13 && !e.shiftKey)
       {
           e.preventDefault();
          
       }


      

   },

   reorderListOfChats() // the final message must always be first
 {
   axios.get('/getMessages')
        .then(response => {
           console.log('prvate msgs', response.data); // show if success
           app.privsteMsgs = [];
           app.privsteMsgs = response.data;

            console.log('singles** ',app.singleMsgs);
            //we are putting data into our posts array
        })
        .catch(function (error)
         {
            console.log(error); // run if we have error
        });
},




   friendID: function(id){
     app.friend_id = id;
   },
   sendNewMsg(){
     axios.post('/sendNewMessage', {
             friend_id: this.friend_id,
             msg: this.newMsgFrom,
          })
          .then(function (response) {
         //   console.log(response.data); // show if success
            if(response.status===200){
              window.location.replace('/messages');
              app.msg = 'your message has been sent successfully';
            }

          })
          .catch(function (error) {
            console.log(error); // run if we have error
          });
   }

 }

});