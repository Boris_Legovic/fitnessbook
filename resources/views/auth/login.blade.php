<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>




<!--       <script src="https://js.stripe.com/v3/"></script>
 -->
<!--        <script src="https://js.stripe.com/v3/"></script>
 -->
    <script>  // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

  </script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">




    <!-- CSRF Token
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Fitnessbook') }}</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

   <style type="text/css">
       body
       {
            background-image: url('/images/gym_clean_white_black.png'); 
             background-position: center center;
            background-repeat:  no-repeat;
            background-attachment: fixed;
            background-size:  cover;
            background-color: #999;
            background-size: 100% cover

        
       }



   </style>


</head>
 
<body>

  
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1859536504342207',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=1859536504342207&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript">



</script>


<script type="text/javascript">
  
 function loginFB()
{
  
  FB.login(function(response) {
    if (response.status === 'connected') 
    {
       // response.authResponse.last_name;
       // response.authResponse.email;
       // response.authResponse.gender;    
        FB.api('me?fields=id,first_name,last_name,email,picture.type(large)',function(userData)
        {



                 // var i = document.createElement("input"); //input element, text
                 //               i.setAttribute('type',"text");
                 //               i.setAttribute('name',"name");
                 //               i.setAttribute('value',userData.first_name);

                 //                var j = document.createElement("input"); //input element, text
                 //               j.setAttribute('type',"text");
                 //               j.setAttribute('name',"lastname");
                 //               j.setAttribute('value',userData.last_name);

                 //                var k = document.createElement("input"); //input element, text
                 //               k.setAttribute('type',"text");
                 //               k.setAttribute('name',"email");
                 //               k.setAttribute('value',userData.email);

                 //                var o = document.createElement("input"); //input element, text
                 //               o.setAttribute('type',"text");
                 //               o.setAttribute('name',"picture");
                 //               o.setAttribute('value',userData.picture.data.url);

                               



                 //               var f = document.getElementById('testForm');
                 //               f.appendChild(i);
                 //               f.appendChild(j);
                 //               f.appendChild(k);
                 //               f.appendChild(o);

                              



                 //               f.submit();
            
              $.ajaxSetup({
                   headers: 
                   {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
                 });

                   

                    $.ajax({
                          type:'POST',
                          url:'/loginUserWithFacebook',
                          data:{name:userData.first_name,lastname:userData.last_name,email:userData.email,picture:userData.picture.data.url},
                          success:function(data){
                             
                             console.log('dataResult ', data);
                             
                             if(data == 'display_options')
                             {
                                
                                
                                var i = document.createElement("input"); //input element, text
                               i.setAttribute('type',"text");
                               i.setAttribute('name',"user");
                               i.setAttribute('value',data);

                               var f = document.getElementById('goToDetailFacebookSelectionForm');
                               f.appendChild(i);
                               f.submit();

                               
                             }
                             else
                             {  

                               console.log('submitForm ', data);

                               var i = document.createElement("input"); //input element, text
                               i.setAttribute('type',"text");
                               i.setAttribute('name',"user");
                               i.setAttribute('value',data);

                               var f = document.getElementById('goLogin');
                               f.appendChild(i);
                               f.submit();
                             }
                             
                        }
                     }); 

         });
    
   


       // Logged into your app and Facebook.
     } else {
       // The person is not logged into this app or we are unable to tell. 
     }


  }, {scope: 'public_profile,email'});
}
  
</script>

<div class="container">




  

 
  
 <form id='goLogin' type = "hidden" method="get" action='enterApp'>
                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
</form>

 <form id='testForm' type = "hidden" method="post" action='loginUserWithFacebook'>
                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                </form>

<form id='goToDetailFacebookSelectionForm' type = "hidden" method="get" action='goToDetailFacebookSelection'>
                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                </form>





 <div class="row">
        <div class = 'col-md-2'>
                           
                            
                            
                             
                        

        </div>

         <div class = 'col-md-9'>

        </div>

     </div>
 <div class="row" style="padding:15%; padding-bottom:25%;">
         <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="background-color:rgba(0, 0, 0, 0.72); border-color:transparent; ">

                @if(session('status'))
                    {{session('status')}}

                       <span class="help-block">
                                        <strong>Please go to Your email and confirm registration!</strong>
                      </span>

                @endif


                <div class=" text-center" style="background-color: transparent; border-color: transparent; color: white; padding: 10%;"  >
                     
                              <a href="{{ url('/') }}">
                                <!-- {{ config('app.name', 'Fitnessbook') }} -->
                                 <img width="100%"; src="/images/logo_letters.png"   alt="Cinque Terre" >
                               <!--  <h2 style="color: gray;">Fitnessbook</h2> -->
                                
                           
                              </a>
                      <div class ='col-md-12' style="padding: 10px;">
                          <h style='margin-top:5px;'><b>Login</b></h>
                    </div>
                     <div class ='col-md-12' style="padding: 10px;">

                                   
                      </div>              

                      @if(isset($verification))

                           <div class  = 'row'>
                                 <div class = 'text-center col-md-12'>
                                    <div class="col-md-12 alert alert-success">
                                     <p> Verification sucess! </p>
                                    </div>
                                 </div>
                           </div> 

                      @endif

                       @if(isset($registrationDone))

                           <div class  = 'row'>
                                 <div class = 'text-center col-md-12'>
                                    <div class="col-md-12 alert alert-success">
                                     <p> Registration sucess! Please go to Your email and confirm Your account.
                                     </p>
                                    </div>
                                 </div>
                           </div> 

                      @endif
                     

                </div>

                <div class="panel-body">
                       

                    <form id = 'form_login' class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label" style="color: white;">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label" style="color: white;">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label style="color: white;">
                                        <input  type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class=" text-center">
                            <div class="col-md-12 col-md-offset-4">
                              
                                


                               
                            
                           </div>
                          </div> 

                    </form>

                      <div class = 'row'>
                         <div class = 'col-md-12 text-center'>
                         
                               
                               <div style="width: 90%; margin-bottom:10px; " onclick="checkIfUserConfirmedAcc()" type="submit" class="btn btn-primary">
                                    Login
                                </div>


                            </div>
                              <div class = 'col-md-12 text-center'>    

                                 <div style="width: 90%; margin-bottom:10px; " onclick="loginFB()" type="submit" class="btn btn-primary">
                                    Login with Facebook
                                </div>
                              </div>

                                 <!--  <div style="width: 100%;" class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div> -->

                                  

                          </div>  
                          <div class = 'text-center'>
                           <a style="color: white;" class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                              </div>

                      </div>  

                  

                      <div class = 'row'>
                          <div class = 'text-center'>
                              <small style="color: gray;">This is a beta version. It's still in development mode. Use at Your own responsibility. 
                               </small>
                          </div>  

                      </div>  

                </div>
            </div>
        </div>
    </div>
</div>
</body>

@include('inc.footer');

    <script type="text/javascript">

    function checkIfUserConfirmedAcc()
    {

       $("form_login").submit(function(e){
        e.preventDefault();
        });

        var email = document.getElementById('email').value;
        
        $.ajaxSetup({
                  headers: 
                  {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
          
           $.ajax({
                   type:'POST',
                   url:'/checkIfUserHaveStatusVerified',
                   data:{email:email},
                   success:function(data)
                   {

                       if(data == '0')
                       {
                           alert('Please go to Your email and confirm account!')
                       }
                       else
                       {
                           $("#form_login").submit();
                       }
                    }
                });
      }




</script>




</html>





       
