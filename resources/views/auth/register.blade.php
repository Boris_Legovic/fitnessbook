<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
<!--       <script src="https://js.stripe.com/v3/"></script>
 -->
<!--        <script src="https://js.stripe.com/v3/"></script>
 -->
    <script>  // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

  </script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    <!-- CSRF Token
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Fitnessbook') }}</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style type="text/css">
         body
       {
                
          background-image: url('/images/gym_clean_white_black.png'); 
       

            background-position: center center;
            background-repeat:  no-repeat;
            background-attachment: fixed;
            background-size:  cover;
            background-color: #999;
            background-size: 100% cover
        
       }

       label
       {
          color:white;
       }

       strong
       {
        color:white;
       }

    </style>


</head>
<body >
<div class="container">

   

    <div class="row" style="padding:10%; padding-bottom:15%;">
        <div class="col-md-7 col-md-offset-3">
            <div class="panel panel-default" style="background-color:rgba(0, 0, 0, 0.72); border-color:transparent;">
               <div class=" text-center" style="background-color: transparent; border-color: transparent; color: white; padding: 10%;"  >
                     
                              <a href="{{ url('/') }}">
                                <!-- {{ config('app.name', 'Fitnessbook') }} -->
                                 <img width="100%"; src="/images/logo_letters.png"   alt="Cinque Terre" >
                               <!--  <h2 style="color: gray;">Fitnessbook</h2> -->
                                
                           
                              </a>
                      <div class ='col-md-12' style="padding: 10px;">
                          <h style='margin-top:5px;'><b>Register</b></h>
                    </div>

                     

                </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
               
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong >{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                   
                          <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">Lastname</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                         
                         <div class = "form-group">
                                <label for="type" class="col-md-4 control-label">Type</label>
                                <div class="col-md-6">

                                 <label class="radio-inline">
                                      <input type="radio" name="type" value = 'athlete'>Athlete
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="type" value = 'trainer'>Trainer
                                    </label>
                                   
                                </div>
                           </div>    

                                  <div class = "form-group">
                                <label for="gender" class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">

                                 <label class="radio-inline">
                                      <input type="radio" name="gender" value = 'male'>Male
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="gender" value = 'female'>Female
                                    </label>
                                    
                                </div>
                           </div>    
                        
                        <div class = 'text-center' style="padding: 5%;">
                             <small style="color:white;">  By clicking Sign Up, you agree to our <a href='/terms_of_service'> Terms</a>. Learn how we collect, use and share your data in our <a href='/privacyPolicy'>Data Policy </a> and how we use cookies and similar technology in our <a href='/cookies'>Cookies Policy</a>. 
                             </small>
                         </div>    

                          <div class = 'text-center' style="padding: 5%;">
                        <div class="form-group">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary" style="width: 100%;">
                                    Register
                                </button>
                            </div>
                        </div>
                      </div>  
                    </form>
                       <div class = 'row'>
                          <div class = 'text-center'>
                              <small style="color: gray;">This is a beta version. It's still in development mode. Use at Your own responsibility.
                               </small>
                          </div>  

                      </div>  
                </div>
            </div>
        </div>
    </div>

  </div>

</body>
@include('inc.footer');


</html>

