

   <div  v-for="privsteMsg in privsteMsgs" style="margin-left: 15px; margin-right: 15px;">
     




 <div v-if="privsteMsg.user_one=={{Auth::user()->id}}" >
   

     <li  v-if="privsteMsg.user_one_status==1"  @click="messages(privsteMsg.id)" style="list-style:none;
      margin-top:10px; background-color:#f2f2f2;" class="row">

        <div class="col-xs-3 col-md-3 pull-left">
              <div v-if="privsteMsg.profile_picture == null ">
                          
                          <img src= "/images/empty_trainer_icon_with_background.png"
                            style="width:100%;  border-radius:100%; margin:5px;">

                </div>

                <div v-else>
                         <img :src= "privsteMsg.profile_picture"
                            style="width:100%;   border-radius:100%; margin:5px;">
                </div>       
         </div>

        <div class="col-xs-9 col-md-9 pull-left" style="margin-top:5px;">
          <b> @{{privsteMsg.name}}</b><br>
          <small>@{{privsteMsg.last_message}}</small>
       </div>
     </li>

     <li v-else  @click="messages(privsteMsg.id)" style="list-style:none;
      margin-top:10px; background-color:#fff;" class="row">

        <div class="col-xs-3 col-md-3 pull-left" >
             <div v-if="privsteMsg.profile_picture == null ">
                          
                          <img src= "/images/empty_trainer_icon_with_background.png"
                            style="width:100%;  border-radius:100%; margin:5px;">

                </div>

                <div v-else>

                         <img :src= "privsteMsg.profile_picture"
                            style="width:100%;  border-radius:100%; margin:5px;">  <!--  this is ok -->
                </div>        
         </div>

        <div class="col-xs-9 col-md-9 pull-left" style="margin-top:5px;">
          <b> @{{privsteMsg.name}}</b><br>
          <small>@{{privsteMsg.last_message}}</small>
       </div>
     </li>
   </div>
 <div v-else>


        <li v-if="privsteMsg.user_two_status==1"  @click="messages(privsteMsg.id)" style="list-style:none;
      margin-top:10px; background-color:#f2f2f2;" class="row">

        <div class="col-xs-3 col-md-3 pull-left">
              <div v-if="privsteMsg.profile_picture == null ">
                          
                          <img src= "/images/empty_trainer_icon_with_background.png"
                            style="width:100%;  border-radius:100%; margin:5px;">

                </div>

                <div v-else>
                         <img :src= "privsteMsg.profile_picture"
                            style="width:100%;   border-radius:100%; margin:5px;">
                </div>       
         </div>

        <div class="col-xs-9 col-md-9 pull-left" style="margin-top:5px;">
          <b> @{{privsteMsg.name}}</b><br>
          <small>@{{privsteMsg.last_message}}</small>
       </div>
     </li>

     <li v-else  @click="messages(privsteMsg.id)" style="list-style:none;
      margin-top:10px; background-color:#fff;" class="row">

        <div class="col-xs-3 col-md-3 pull-left">
             <div v-if="privsteMsg.profile_picture == null ">
                          
                          <img src= "/images/empty_trainer_icon_with_background.png"
                            style="width:100%;  border-radius:100%; margin:5px;">

                </div>

                <div v-else>

                         <img :src= "privsteMsg.profile_picture"
                            style="width:100%; border-radius:100%; margin:5px;">  <!--  this is ok -->
                </div>              
         </div>

        <div class="col-xs-9 col-md-9 pull-left" style="margin-top:5px;">
          <b> @{{privsteMsg.name}}</b><br>
          <small>@{{privsteMsg.last_message}}</small>
       </div>
     </li>

</div>


   </div>

<div v-if='privsteMsgs.length == 0'>

     <small style="margin-left: 5px;">    No messages  </small>
 </div> 
