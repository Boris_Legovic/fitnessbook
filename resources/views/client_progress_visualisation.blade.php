@extends('layouts.app')

@section('content')

 <div id = 'main'>
  <script src='/js/graphs/clientProgressChart.js'></script>

<div  class="container" style="padding-left: 0;" >
   

 
    <div class="row" >
 
  

   <?php
        $id = 0;
        $counter = 1;
   ?>     


  @foreach($data as $rec)

       
                  
          @if($rec->user_customer_id != $id)
      <div class="col-md-3">
            <div class='row backgroundNeutralDiv'  style="margin: 0px; padding: 10px; border-radius: 10px;">  
                     

                   
                              
                                <div class = 'col-md-12'>
                                    <?php if($rec->profile_picture == NULL)
                                      {
                                        $imageLink = '/images/empty_trainer_icon_with_background.png'; 
                                      }
                                      else
                                      {
                                        $imageLink = Storage::url($rec->profile_picture); 
                                      }

                                      ?>
 
                                       <div id = 'profilePicId' class = 'profile_empty col-md-12'  style=" height: 210px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 20px;">  
                                      </div>
                                 </div>
                              
                                                             <div class = 'col-md-12'>       
                                  <form   method="post"  action = "{{URL::to('newMessageTo')}}" enctype="multipart/form-data">
                                                                  
                                       <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                                       <input type = "hidden" name = "user_to_message" value = "{{$rec->user_customer_id}}" />
                                      
                                     
                                         
                                                            <button class="btn  btn-primary" style="width: 100%;">
                                                                    <div class = 'row'>
                                                                        <div class = 'col-md-3'>
                                                                            <div class="pull-left">
                                                                                   <img src="/images/message_green.png">
                                                                             </div>      
                                                                         </div> 
                                                                         <div class = 'col-md-9'>
                                                                              <div class =' pull-left'>Message</div>
                                                                         </div> 
                                                                     </div> 
                                                            </button>

                                                            <div style="height: 10px;">
                                                              
                                                            </div>

                                                             <div id='{{$rec->user_customer_id}}' onclick="loadTrainingListFromDB(this.id)"  class="btn  btn-primary" style="width: 100%;">
                                                                    <div class = 'row'>
                                                                        <div class = 'col-md-3'>
                                                                            <div class="pull-left">
                                                                                   <img src="/images/send_training.png">
                                                                             </div>      
                                                                         </div> 
                                                                         <div class = 'col-md-9'>
                                                                              <div class =' pull-left'>Send training</div>
                                                                         </div> 
                                                                     </div> 
                                                            </div>
                                                             <div style="height: 10px;">
                                                              
                                                            </div>

                                                       
                                          
                                      </div>       
                             
                         
                      
                    

                    <div class = 'col-md-12'>
                       <div class = 'text-center'>
                        <div class = 'row'>
                                  <small><b>{{$rec->name}}</b></small> <small><b>{{$rec->lastname}}</b></small>
                        </div>
                        <div class = 'row'>
                                  <small> Start period : {{$rec->start_period}}</small> 
                        </div>
                        <div class = 'row'>
                                   <small>End period : {{$rec->end_period}}</small> 
                        </div>
                      </div>
                          <canvas id='client{{$counter}}' height="250"  >
                         </canvas>

                    </div>


              </div>

              <?php
                   $id = $rec->user_customer_id;
                   $counter ++;
              ?>   

               </div>   
          @endif
               

   

       
   @endforeach

   

 @foreach($emptyData as $rec)
     @if($rec->user_customer_id != $id)
      <div class="col-md-3">
            <div class='row backgroundNeutralDiv'  style="margin: 0px; padding: 10px; border-radius: 10px;">  
                     

                   
                              
                                <div class = 'col-md-12'>
                                    <?php if($rec->profile_picture == NULL)
                                      {
                                        $imageLink = '/images/empty_trainer_icon_with_background.png'; 
                                      }
                                      else
                                      {
                                        $imageLink = Storage::url($rec->profile_picture); 
                                      }

                                      ?>
 
                                       <div id = 'profilePicId' class = 'profile_empty col-md-12'  style=" height: 210px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 20px;">  
                                      </div>
                                 </div>
                              
                               <div class = 'col-md-12'>       
                                  <form   method="post"  action = "{{URL::to('newMessageTo')}}" enctype="multipart/form-data">
                                                                  
                                       <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                                       <input type = "hidden" name = "user_to_message" value = "{{$rec->user_customer_id}}" />
                                      
                                     
                                         
                                                            <button class="btn  btn-primary" style="width: 100%;">
                                                                    <div class = 'row'>
                                                                        <div class = 'col-md-3'>
                                                                            <div class="pull-left">
                                                                                   <img src="/images/message_green.png">
                                                                             </div>      
                                                                         </div> 
                                                                         <div class = 'col-md-9'>
                                                                              <div class =' pull-left'>Message</div>
                                                                         </div> 
                                                                     </div> 
                                                            </button>

                                                            <div style="height: 10px;">
                                                              
                                                            </div>

                                                     <div id='{{$rec->user_customer_id}}' onclick="loadTrainingListFromDB(this.id)"  class="btn  btn-primary" style="width: 100%;">
                                                            <div class = 'row'>
                                                                <div class = 'col-md-3'>
                                                                    <div class="pull-left">
                                                                           <img src="/images/send_training.png">
                                                                     </div>      
                                                                 </div> 
                                                                 <div class = 'col-md-9'>
                                                                      <div class =' pull-left'>Send training</div>
                                                                 </div> 
                                                             </div> 
                                                    </div>
                                                             <div style="height: 10px;">
                                                              
                                                            </div>

                                                       
                                          
                                      </div>       
                                                               
                                   </form>
                              
                   <div class = 'col-md-12'>
                            
                             <div class = 'text-center'>
                                  <div class = 'row'>
                                            <small><b>{{$rec->name}}</b></small> <small><b>{{$rec->lastname}}</b></small>
                                  </div>
                                  <div class = 'row'>
                                            <small> Start period : {{$rec->start_period}}</small> 
                                  </div>
                                  <div class = 'row'>
                                             <small>End period : {{$rec->end_period}}</small> 
                                  </div>
                            </div>
                           
                          <canvas id='emptyClient{{$counter}}' height="250">
                         
                         </canvas>

                        <?php
                             echo '<script type="text/javascript">   addEmptyGraph(' . $counter . ')  </script>';
                        ?>

                     </div>


              </div>

              <?php
                   $id = $rec->user_customer_id;
                   $counter ++;
              ?>   

               </div>   
          @endif
               

 @endforeach

        </div>
    </div>
</div>



<script type="text/javascript"> 

     var data = '{!!$encoded!!}';
     data = JSON.parse(data);
     console.log('normal',data);
     // console.log('encoded', {{$encoded}});
    // showMeassuresGraphs(data);

    if(data.length>0)
    {
        showMeassuresGraphs(data);
    }


 // var dataEmpty = '{!!$encodedEmpty!!}';
 //     data = JSON.parse(dataEmpty);

 //     showMeassuresGraphs(data);

</script>








@endsection
