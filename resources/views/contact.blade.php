<!doctype html>
    <head>
        <link rel="stylesheet" href="/css/app.css">

    <style type="text/css">
         body
       {
                           background-image: url('/images/gym_clean_white_black.png'); 
             background-position: center center;
            background-repeat:  no-repeat;
            background-attachment: fixed;
            background-size:  cover;
            background-color: #999;


       }

       label
       {
          color:white;
       }

    </style>
    </head>
    
    <body>
                      <div class="container-fluid">
                         
                         </div>

        <div class = 'content'>

          @if(isset($isSent))
            <div class='text-center'>
                <div class="alert alert-success">
                   Message sent!
                </div>
            </div>
          @endif

            <div class = 'row' style="padding: 15%; ">
              <div style="background-color:rgba(0, 0, 0, 0.72); border-color:transparent; ">

                <div class = 'col-md-3'></div>
                <div class='col-md-6' style=" background-color:rgba(0, 0, 0, 0.72); border-color:transparent; border-radius: 5%;  padding: 20px;"> 
            
                   <div class=" text-center" style="background-color: transparent; border-color: transparent; color: white; padding: 10%;"  >
                     
                              <a href="{{ url('/') }}">
                                <!-- {{ config('app.name', 'Fitnessbook') }} -->
                                 <img width="100%"; src="/images/logo_letters.png"   alt="Cinque Terre" >
                               <!--  <h2 style="color: gray;">Fitnessbook</h2> -->
                                
                           
                              </a>
                      </div>        
              {!! Form::open(['url' => 'contact/submit']) !!}
                

                <div class = 'form-group'>
                    {{Form::label('email', '')}}
                    {{Form::text('email', '', ['class' => 'form-control','placeholder'=>'Email'])}}
                </div>

                 <div class = 'form-group'>
                    {{Form::label('message', 'Message')}}
                    {{Form::textarea('message', '', ['class' => 'form-control','placeholder'=>'Message'])}}
                </div>
                {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

                
              {!! Form::close() !!}
              </div>
               <div class = 'col-md-3'></div>
             </div>
          </div>


    </body>

     <footer id='footer' class = 'text-center' style="position: fixed; bottom:0; width: 100%;">
        <p>Copyright 2018 &copy; FitnessBook </p>
    </footer>

</html>