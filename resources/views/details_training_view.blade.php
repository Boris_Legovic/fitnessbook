@extends('layouts.app')

@section('content')

<?php $numberOfData = 0;  
      $max = count($trainingData);
?>


                    
<form  id = 'downloadForm' method="post"  action = "{{URL::to('downloadFile')}}" enctype="multipart/form-data">
                  <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
   </form>
         
         <div id = 'main'>
            
             <div class="container-fluid " style="padding-left:0;" >

                


             <div class = 'row' >

                       

                        <div class = 'col-md-4 col-lg-4'>
                        </div>


                     
                           <div class = 'col-md-4 col-lg-4'>
                                  <div class = 'text-center'>
                                   <?php 

                                           // $imageLink = Storage::url( $trainingData[0]->image); 
                                         $imageLink = Storage::disk('do_spaces')->url( $trainingData[0]->image); 
                                       
                                   ?>
                                   
                                           <!--   <img height="100%;"  src="{{$imageLink}}"  alt="Cinque Terre" style="border-radius: 10px;"> -->

                                            <div id = 'profilePicId' class = 'profile_empty'  style="height: 390px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 5px; margin-left: 10%; margin-right: 10%;  ">  
                                                  </div>

                                         <div class="text-center">
                                              
                                            @if($numberOfRates == 1)
                                                <p> {{ $numberOfRates}} review</p>
                                            @else 
                                                 <p> {{ $numberOfRates}} reviews</p>
                                            @endif

                                         </div>

                                                
                                                           <div style="height: 20px; ">
                                                    

                                                         @for($i=1;$i<11;$i++)

                                                              @if($rate >= $i)

                                                                 <span style="color:orange;">★</span>


                                                              @else


                                                                  <span>★</span>

                                                              @endif



                                                         @endfor

                                                        

                                                   
                                                                </div>
                                                      
                                                      @if( $rate == null)
                                                            <div class="text-center">Not rated yet</p>
                                                           </div>
                                                      @else
                                                          <div class="text-center"><p>Rate : {{ $rate}} / 10</p>
                                                          </div>

                                                      @endif

                                                      
                                     </div> <!-- text center end   -->       
                                  
                                             <div class = 'text-center'>
                                                    <h><b>{{ $trainingData[0]->title}} </b></h>
                                                     <p> Owner: {{ $trainingData[0]->name}}&nbsp;{{ $trainingData[0]->lastname}} </p>
                                                     <p>{{ $trainingData[0]->description}}</p>

                                                      <p>  Total: &nbsp;{{ $trainingData[0]->price}} {{ $trainingData[0]->currency}}</p>

                                             </div>
                                




<!-- <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_CIc4spDBSBgusI8DbRhiJlJ8rQnEdBs1&scope=read_write">Connect with stripe</a> -->




<!-- this works for single payment


 -->
    @if(!$userAlreadyBought) <!-- user did not buy training yet -->
<script src="https://js.stripe.com/v3/"></script>
    
      @if($trainingData[0]->price > 0)
       <form name='stripe-form' action="{{url('checkout')}}" method="post" id="payment-form">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                            <input id = 'trainingId' type = "hidden" name = "trainingId" value = "{{$trainingId}}">
                    <div class="form-row">
                      <label for="card-element">
                        Credit or debit card
                      </label>
                         
                          <div id="card-element">
            <!--             a Stripe Element will be inserted here.
             -->          </div>

                   
                      <div id="card-errors" role="alert"></div>
                    </div>
                   <br>
                     <button class='btn btn-primary' style="width: 100%;">
                          Pay and download
                    </button>
    
         </form> 

              <script src='/js/stripe_functions.js'></script>  

          @else

              
                      <input id = 'trainingId' type = "hidden" name = "trainingId" value = "{{$trainingId}}">

                     <button onclick = 'showTrainingRating()' class='btn btn-primary' style="width: 100%;margin-bottom: 10px;">
                          Rate
                    </button>

                      <div onclick = 'downloadTraining("{{$trainingData[0]->title}}","{{$trainingData[0]->file}}")' class='btn btn-primary' style="width: 100%;">
                          Download
                    </div>

          @endif   

   @else

                   

                      <input id = 'trainingId' type = "hidden" name = "trainingId" value = "{{$trainingId}}">

                     <button onclick = 'showTrainingRating()' class='btn btn-primary' style="width: 100%;margin-bottom: 10px;">
                          Rate
                    </button>

                      <div onclick = 'downloadTraining("{{$trainingData[0]->title}}","{{$trainingData[0]->file}}")' class='btn btn-primary' style="width: 100%;">
                          Download
                    </div>

   @endif  

          </div> <!-- end of col-md-6 -->
 
        <div class = 'col-md-4 col-lg-4'>

         </div> 


        </div> <!-- end of row -->
                </div>
            </div>




 @if(!$userAlreadyBought)
<!--      <script src='/js/stripe_functions.js'></script>  
 --> @endif
     <script src='/js/profileActions.js'></script>  

     @endsection

