<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
<!--       <script src="https://js.stripe.com/v3/"></script>
 -->
<!--        <script src="https://js.stripe.com/v3/"></script>
 -->
    <script>  // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

  </script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    <!-- CSRF Token
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Fitnessbook') }}</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style type="text/css">
         body
       {
                
          background-image: url('/images/gym_clean_white_black.png'); 
       

            background-position: center center;
            background-repeat:  no-repeat;
            background-attachment: fixed;
            background-size:  cover;
            background-color: #999;
            background-size: 100% cover
        
       }

       label
       {
          color:white;
       }

       strong
       {
        color:white;
       }

    </style>


</head>
<body >
<div class="container">

   

    <div class="row" style="padding:10%; padding-bottom:15%;">
        <div class="col-md-7 col-md-offset-3">
            <div class="panel panel-default" style="background-color:rgba(0, 0, 0, 0.72); border-color:transparent;">
               <div class=" text-center" style="background-color: transparent; border-color: transparent; color: white; padding: 10%;"  >
                     
                              <a href="{{ url('/') }}">
                                <!-- {{ config('app.name', 'Fitnessbook') }} -->
                                 <img width="100%"; src="/images/logo_letters.png"   alt="Cinque Terre" >
                               <!--  <h2 style="color: gray;">Fitnessbook</h2> -->
                                
                           
                              </a>
                      <div class ='col-md-12' style="padding: 10px;">
                          <h style='margin-top:5px;'><b>Add personal data</b></h>
                    </div>

                     

                </div>

                <div class="panel-body">
                    <form id='enterAppWithDetailsForm' class="form-horizontal" method="POST" action="enterAppWithDetails">
                        {{ csrf_field() }}
               

                     

                       
                        
                         
                         <div class = "form-group">
                                <label for="type" class="col-md-4 control-label">Type</label>
                                <div class="col-md-6">

                                   <label class="radio-inline">
                                      <input type="radio" name="type" value = 'athlete'>Athlete
                                    </label>
                                   
                                    <label class="radio-inline">
                                      <input type="radio" name="type" value = 'trainer'>Trainer
                                    </label>
                                   
                                </div>
                           </div>    

                                  <div class = "form-group">
                                <label for="gender" class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">

                                 <label class="radio-inline">
                                      <input type="radio" name="gender" value = 'male'>Male
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="gender" value = 'female'>Female
                                    </label>
                                    
                                </div>
                           </div>    
                        
                        <div class = 'text-center' style="padding: 5%;">
                             <small style="color:white;">  By clicking Sign Up, you agree to our <a href='/terms_of_service'> Terms</a>. Learn how we collect, use and share your data in our <a href='/privacyPolicy'>Data Policy </a> and how we use cookies and similar technology in our <a href='/cookies'>Cookies Policy</a>. 
                             </small>
                         </div>    

                          <div class = 'text-center' style="padding: 5%;">
                        <div class="form-group">
                            <div class="col-md-12 ">
                                <div onclick="checkIfAllSelectedAndLogin()" type="submit" class="btn btn-primary" style="width: 100%;">
                                    Login
                                </div>
                            </div>
                        </div>
                      </div>  
                    </form>
                       <div class = 'row'>
                          <div class = 'text-center'>
                              <small style="color: gray;">This is a beta version. It's still in development mode. Use at Your own responsibility.
                               </small>
                          </div>  

                      </div>  
                </div>
            </div>
        </div>
    </div>

  </div>

</body>
@include('inc.footer');

<script type="text/javascript">
  

  function checkIfAllSelectedAndLogin()
  {

      var gender_check = $('input:radio[name=gender]:checked').val();

      var type_check = $('input:radio[name=type]:checked').val();


       if(gender_check && type_check) 
       {
          document.getElementById('enterAppWithDetailsForm').submit();
       }

       


  }

</script>

</html>

