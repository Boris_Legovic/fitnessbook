   
<?php

$imageLink;
?>
   <nav class="navbar navbar-default navbar-static-top"  >

    <div class = 'container-fluid'>

        <div class = 'row'>

            <div class = 'col-xs-5  col-sm-2 col-md-2 '>
                     <a class="navbar-brand" href="{{ url('/home') }}">
                        <!-- {{ config('app.name', 'Fitnessbook') }} -->
                        <img height="20" src="/images/logo_letters.png"   alt="Cinque Terre">
                    
                    </a>
            </div>

            <div class = 'col-xs-7 hidden-md hidden-sm hidden-lg hidden-xl' style="margin-top: 5px;">
                    <button onclick="showPhonesHeaderMenu()" class = 'btn' style="background-color: transparent; border-color: transparent;">
                                 <i class="material-icons">&#xE5D2;</i>
                    </button>
            </div>  
            
              

            <div id='firstLeftThree' class = 'hidden-xs col-md-5  col-sm-5' hidden-xs style="padding-top: 5px;">

                            <div class = 'row'>

                                <div class = 'col-md-4 col-sm-4 col-xs-12'>
                                   <button id='logoutId'  onclick="event.preventDefault();
                                                           document.getElementById('uploadTrainingForm').submit();" class = 'headerButton btn' >
                                                
                                                   <div class = 'row'>
                                                              <div class = 'col-md-3 col-xs-3' >
                                                                     <img  src="/images/upload_icon.png"   alt="Cinque Terre">
                                                              </div>
                                                             
                                                              <div class = "col-md-9 col-xs-9" >
                                                                      <div class ='pull-left'><p>Upload training</p></div>
                                                               </div>         

                                                       </div>

                                       </button>
                              </div>
                          


                    



                              <div class = 'col-md-4 col-sm-4 col-xs-12'>

                                            <button id='logoutId'  onclick="event.preventDefault();
                                                                                      document.getElementById('trainingForm').submit();" class = 'headerButton btn' >
                                                                              <div class = 'row'>
                                                                                         <div class = 'col-md-3 col-xs-3' >
                                                                                                <img  src="/images/dumbell_violet.png"   alt="Cinque Terre">
                                                                                         </div>
                                                                                        
                                                                                         <div class = "col-md-9 col-xs-9" >
                                                                                                 <div class =' pull-left'><p>Find training</p></div>
                                                                                          </div>         

                                                                                  </div>

                                               </button>    
                                      </div>


                                         <form id="findTrainerForm" action="showTrainerList" method="POST" style="display: none;">
                                                     {{ csrf_field() }}
                                           </form>
                    


                   <div class = 'col-md-4 col-sm-4 col-xs-12'>

                       <button id='logoutId'  onclick="event.preventDefault();
                                                              document.getElementById('findTrainerForm').submit();" class = 'headerButton btn' >
                                                      <div class = 'row'>
                                                                 <div class = 'col-md-3 col-xs-3' >
                                                                        <img  src="/images/trainer_blu.png"   alt="Find coach online">
                                                                 </div>
                                                                
                                                                 <div class = "col-md-9 col-xs-9" >
                                                                       <div class = 'pull-left'> <p>Find trainer</p></div>
                                                                  </div>         

                                                          </div>

                       </button>


                   </div>   

                         <form id="profile-form-id" action="showPicturesView" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type = "hidden" name = "id" value = "{{Auth::user()->id}}" />

                          </form>

                           <form id="coachForm" action="findCoachIndex" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>

                           <form id="trainingForm" action="findTrainingIndex" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                           <form id="uploadTrainingForm" action="uploadTrainingIndex" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>

                    </div>  <!--  inside row -->

            </div>  

             <div id='secondRightThree'  class = 'col-md-5 hidden-xs'>
                       <ul class=" navbar-nav navbar-right" style= "margin-right: 10px;">


                           


                             <li class="dropdown" style="margin-top: 10px;  list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                               role="button" aria-expanded="false">
                                                
                                                <div class='row '>
                                                      <div class = 'col-md-3 col-xs-3' style="padding-right: 0px;">
                                                             <img src="/images/notification_main_color.png"  />
                                                       </div>      
                                                       
                                                       <div class='col-md-7 col-xs-7' style="padding-left: 0px;">
                                                             <div class = 'pull-left'>   
                                                                   <div class='row '>
                                                                           <div class = 'col-md-9 col-xs-9'>
                                                                                 <p>Notifications</p> 
                                                                           </div>
                                                                           <div class = 'col-md-3 col-xs-3' style=" padding-left: 5px;">
                                                                                
                                                                                  <span class="badge" style="background:#691A99;">
                                                                      
                                                                                         <small> @{{notifications}} </small>
                                                                
                                                                                    </span> 
                                                                                  
                                                                           </div>      
                                                                  </div>  
                                                                   
                                                           </div> 
                                                     </div>

                                                       <div class = 'col-md-2 col-xs-2' >
                                                              
                                                                  
                                                                   
                                                            
                                                            
                                                       </div>      

                                                 </div>   
                                            </a>
                                           <ul class="dropdown-menu" role="menu" style="width:320px">
                                             @include('notifications')
                                            </ul>
                                     </li>


                                   <li class="dropdown" style="margin-top: 10px; list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                               role="button" aria-expanded="false">
                                               <!--  <img src="/images/message_green.png" />
                                                <span class="badge"
                                                      style="background:red; position: relative; top: -15px;">
                                                   <small> @{{messageNotificationsData}}</small>

                                                     
                                                </span> -->

                                                <div class='row'>
                                                      <div class = 'col-md-3 col-xs-3' style="padding-right: 0px;">
                                                             <img src="/images/message_green.png"  />
                                                       </div>      
                                                       
                                                       <div class='col-md-7 col-xs-7' style="padding-left: 0px;">
                                                            <div class = 'pull-right'>   
                                                                   <div class='row '>
                                                                           <div class = 'col-md-9 col-xs-9'>
                                                                                 <p>Messages</p> 
                                                                           </div>
                                                                           <div class = 'col-md-3 col-xs-3' style=" padding-left: 5px;">
                                                                                
                                                                                  <span class="badge" style="background:#691A99;">
                                                                      
                                                                                         <small> @{{messageNotificationsData}} </small>
                                                                
                                                                                    </span> 
                                                                                  
                                                                           </div>      
                                                                  </div>  
                                                                   
                                                           </div> 
                                                                   
                                                       </div> 

                                                       <div class = 'col-md-2 col-xs-2'>
                                                               
                                                       </div>      

                                                 </div>   
                                            </a>
                                            <ul class="dropdown-menu" role="menu" style="width:350px;">
                                                 <form id='messages_form' action="messages" method="GET" style="display: none;">
                                                            {{ csrf_field() }}
                                                  </form>
                                                
                                               <button onclick="document.getElementById('messages_form').submit()" class = 'text-center' style="background-color: #00000007; border-color: transparent; width: 94%; margin-left:3%;">
                                                  <small><a>Read messages </a></small>
                                               </button> 
                                               
                                               @include('chat.list_of_chats')
                                            </ul>
                                     </li>

                         

                                    <li class="dropdown" style=" list-style: none; margin-top: 5px; margin-right: 10px;" >
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                                   <?php 
                                                    if(Auth::user()->profile_picture == NULL)
                                                    {
                                                      $imageLink = '/images/empty_trainer_icon_with_background.png'; 
                                                    }
                                                    else
                                                    {
                                                      $imageLink = Storage::url(Auth::user()->profile_picture); 
                                                    }

                                                    ?>



<!--                                                     <img src="{{Storage::url(Auth::user()->profile_picture)}}" width="30px" height="30px" class="img-circle"/>
 --><!-- 
                                                    <div id = 'profilePicId' class = 'profile_empty'  style="height: 30px; width: 30px;  background-image: url('{{$imageLink}}');  border-radius: 50%; ">  
                                                      </div> -->

                                                      <div class='row'>
                                                      
                                                         <div class = 'col-md-9 col-xs-9'>
                                                                <div id = 'profilePicId' class = 'profile_empty'  style="height: 30px; width: 30px;  background-image: url('{{$imageLink}}');  border-radius: 50%; ">  
                                                               </div> 
                                                         </div>
  
                                                       </div>   


                                                   
                                                </a>

                                                <ul class="dropdown-menu" role="menu" style="">




                                                    <li> <a href="{{ url('showPicturesView') }}/{{Auth::user()->slug}}" >   Profile  </a> </li>
                                                    <li> <a href="{{ url('settings') }}" >  Edit Profile  </a> </li>

                                                    <li style=" list-style: none;">
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                   document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </li>


                                                </ul>
                                          </li>  <!-- dropdown finish -->






                      </ul>

            </div>  

        </div>  

    </div>  


           
  </nav>