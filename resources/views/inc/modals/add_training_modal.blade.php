<style type="text/css">
  .modal {
  overflow-y:auto;
}
  
</style>

<div class="modal fade" id="modalWriteData" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style= "background-color: rgb(92, 19, 120);">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel" style="color:white;">New training</h4>
           <input type="text" id = 'trainingName' name='trainingName'  placeholder='Training name' class="form-control"/>
      </div>
      <div class="modal-body">
         <form  id = "1addTrainingForm"  action="Model/AddTraining.php" method="post" enctype="multipart/form-data">

            <table id = '1tab_logic' class="table table-bordered table-hover" id="1tab_logic">
                <thead>
                    <tr>
                        <th class="text-center">
                            No.
                        </th>
                        <th class="text-center">
                            Exercize name
                        </th>
                         <th class="text-center">
                            Sets
                        </th>
                        <th class="text-center">
                            Reps
                        </th>
                       
                         <th class="text-center">
                            Comment
                        </th>
                   </tr>
                </thead>
                <tbody>
                    <tr id='1addr0'>
                       <td>
                        1
                        </td>
                        <td>
                        <input type="text" id = 'exercize0' name='exercize0'  placeholder='Exercize' class="form-control"/>
                        </td>
                        <td>
                        <input onfocus="addListener(this,'0')" type="text" id = 'sets0' name='sets0' placeholder='0' class="form-control" style="width: 40px;margin: auto;" />
                        </td>
                        <td id = 'repsColumn0'>
                      <!--  <input type="text" id = 'reps0' name='reps0' placeholder='8-10' class="form-control" style="width: 100px;margin: auto;" /> -->
                        </td>
                         <td>
                        <input type="text" id = 'comments0' name='comments0' placeholder='Comment' class="form-control"/>
                        </td>
                   </tr>
                    <tr id='1addr1'></tr>
                </tbody>
            </table>
          </form> 
      </div>
      <div class="modal-footer" style="background-color: #68EFAD;">
       
        <span class="pull-right">
           <button id = 'delete_row' onclick='deleteRow()' type="button" class="btn btn-primary">Remove</button>
          <button id = '1add_row' onclick='addRow()'  type="button" class="btn btn-primary">
            Add
          </button>
           <button onclick='saveNewTraining()' id = 'saveNewTraining' type="button" class="btn btn-primary">
            Save
          </button>
        </span>
      </div>
    </div>
  </div>
</div>





