<?php $functionToCall = ''; ?>

<div class="modal fade" id="showClientList" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: rgb(92, 19, 120);">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel" style="color: white">Client list</h4>
          
      </div>
      <div class="modal-body">
     <table class="table table-bordered table-hover" id="tab_logic">
                <thead>
                    <tr>
                        <th class="text-center">
                           Client name
                        </th>
                  </tr>

                </thead>
                <tbody>
                  


              

                 @foreach ($clients as $client)
                   
                    <tr><td id = '{{$client->id}}' onclick="setSelectedIdClient(this.id)"> <p1>{{ $client->name }} </p1>   <p1>{{ $client->lastname }} </p1></td> </tr> 
                 
                  @endforeach
                    <tr id='addr0'>
                       
                    </tr>
                    <tr id='addr1'></tr>
                </tbody>
            </table>
              <div class="form-inline" style="margin-bottom: 10px;">
              <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" placeholder="Search training" />
                    <span class="input-group-btn">
                        <button class="btn btn-info" style="background-color:#9B26AF ; "  type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
               </div>
      </div>
      <div class="modal-footer" style="background-color: #68EFAD;">
      
             <button onclick="sendTraining()" class="btn  btn-primary" style="width: 100%;">
                                                                    <div class = 'row'>
                                                                        <div class = 'col-md-3'>
                                                                            <div class="pull-left">
                                                                                   <div class="material-icons">&#xE154;</div>
                                                                             </div>      
                                                                         </div> 
                                                                         <div class = 'col-md-9'>
                                                                              <div class =' pull-left'>Send training</div>
                                                                         </div> 
                                                                     </div> 
             </button>
          
<!--            <button id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE3C9;</div></button>
 -->           


          
       
      </div>
    </div>
  </div>
</div>





