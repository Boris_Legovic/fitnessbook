<?php   $arrayData;   ?>
<div class="modal fade" id="modalDataInsertion" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style= "background-color: rgb(92, 19, 120);">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel" style="color: white;">Track training</h4>
    
       </div>

<style type="text/css">
  
.label {
    background:#68EFAD;
    display:inline-block;
    border-radius: 12px;
    color: white;
    font-weight: bold;
    height: 17px; 
    padding: 2px 3px 2px 3px;
    text-align: center;
    min-width: 16px;
}

table td {
/*    border-top: none !important;
*/}

</style>



      <div class="modal-body" style="padding: 0;">
                      <div  style="margin:10px; ">
                          <div class="form-inline">  Training date: <input type='datetime' name='trainingDate' id='datePickerData' value= <?php echo "'" .  date("Y-m-d H:i:s") . "'"; ?> class='form-control'></div>
                        </div> 

             <table class="table" id="tab_logic" style="margin: 0;">
                
              

                <thead>
                    <tr>
                        <th class="text-center">
                           Set
                        </th>  

                        <th class="text-center">
                           Weight
                        </th>
                        <th  class="text-center">
                           Reps
                        </th>
                  </tr>

                </thead>
                <tbody>

               
                <?php   

                  
               //   echo 'test' . ($data);
                    $arrayOfIdAndSets = array();
                    $arrayOfData = array();
                    $counter = 1;
                    $counterKgReps = 0;
                ?>

                 @foreach ($trainingData as $rec)
                        <?php  
                                array_push($arrayOfData,$rec->id); 
                                array_push($arrayOfData,$rec->sets);
                                array_push($arrayOfIdAndSets,$arrayOfData);
                                $arrayOfData = array();
                                $counter = 1;

                         ?>
                     <tr>
                             <td style="  background: linear-gradient(#6966664a, #cccccc03);">
                               <div >
                                    <h>
                                       <b> {{ $rec->exercize_name }}  </b>
                                    </h>
                                </div>
                             </td> <!-- first row -->

                             <td style="  background: linear-gradient(#6966664a, #cccccc03);">
                                
                             </td> <!-- second row -->

                             <td style="  background: linear-gradient(#6966664a, #cccccc03);">
                                  <!-- third row -->
                             </td>  

                     <tr>    
                   
                    
                       @for ($i = 0; $i<$rec->sets;$i++)
                        <tr>
                            <td id = '{{$rec->id}}'>
                                 
                                 
                                        <span class="label">{{$counter}} </span>
                                          <!-- <div style="margin: auto; background-color: #68EFAD; height: 40px; width: 40px; border-radius: 50%;"> {{$counter}} 
                                          </div> -->
                                         

                                 
                             </td> 
                        
                        <td>
                           
                           @if($data == NULL)
                           
                              <input id = '{{$rec->id}}kg{{$i}}' type="text" name="set" class="form-control text-center" style="width: 100px; margin: auto;" placeholder="{{$weightUnit}}" />
                           
                           @else

                          
                               <input id = '{{$rec->id}}kg{{$i}}' type="text" name="set" class="form-control text-center" style="width: 100px; margin: auto;" placeholder="{{$data[$counterKgReps ]}}  {{$weightUnit}}" />

                              @endif 

                             
                        </td>
                        <td>
                           @if($data == NULL)
                                <input id = '{{$rec->id}}rep{{$i}}' type="text" name="set" class="form-control text-center" style="width: 100px; margin: auto;" placeholder=" Reps" />

                           @else
                                <input id = '{{$rec->id}}rep{{$i}}' type="text" name="set" class="form-control text-center" style="width: 100px; margin: auto;" placeholder="{{$data[$counterKgReps + 1]}} Reps" />

                           @endif     



                        </td> 
                           <?php 

                                   $counter++; 
                                  $counterKgReps += 2;
                            ?>
                          <!--   </div> -->
                           
                           

                        </tr> 

                      @endfor

                  


                   
                  @endforeach
                    <tr id='addr0'>
                       
                    </tr>
                    <tr id='addr1'></tr>
                </tbody>
            </table>
      </div>
      <div class="modal-footer" style="background-color: #68EFAD;">
       
        <span class="pull-right">
           <button id = 'dissmissTrainingModal' type="button" class="btn btn-primary" data-dismiss="modal">Dissmiss</button> 
            <button id = 'd' onclick="setTrainingDataToDatabase(<?php echo json_encode($arrayOfIdAndSets) . ',' . $trainingData[0]->training_id; ?> )" type="button" class="btn btn-primary">
            Save
          </button>
        </span>
      </div>
    </div>
  </div>
</div>





