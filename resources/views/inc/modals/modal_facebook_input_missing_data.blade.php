<div class="modal fade" id="#facebook_missing_data_modal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel">User options</h4>
      </div>
      <div class="modal-body">
         
          <div class = 'text-center'>

                   <div class = "form-group">
                                <label for="type" class="col-md-4 control-label">Type</label>
                                <div class="col-md-6">

                                   <label class="radio-inline">
                                      <input type="radio" name="type" value = 'athlete'>Athlete
                                    </label>
                                   
                                    <label class="radio-inline">
                                      <input type="radio" name="type" value = 'trainer'>Trainer
                                    </label>
                                   
                                </div>
                           </div>    

                           <div class = "form-group">
                                <label for="gender" class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">

                                 <label class="radio-inline">
                                      <input type="radio" name="gender" value = 'male'>Male
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="gender" value = 'female'>Female
                                    </label>
                                    
                                </div>
                           </div>    




           </div> 
       
          

      </div>
      <div class="modal-footer">
       

           

           
           
            <button  type="button" class="btn btn-primary" style="width: 100%; margin-top:10px; " data-dismiss="modal">No</button>
          
        
      </div>
    </div>
  </div>
</div>