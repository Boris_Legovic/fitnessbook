
<div class="modal fade" id="onlineCoachingModal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel">Coaching request</h4>
      </div>
      <div class="modal-body">
         
           <div class='text-center'>



            <p> Price: {{$data[0]->coaching_price}}</p>
            <p> Period: {{$data[0]->online_coaching_period}}</p>

           </div>



            <script src="https://js.stripe.com/v3/"></script>

       <form name='stripe-form' action="{{url('checkoutCoaching')}}" method="post" id="payment-form">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                            <input id = 'trainerId' type = "hidden" name = "trainerId" value = "{{$data[0]->user_id}}">
                    <div class="form-row">
                      <label for="card-element">
                        Credit or debit card
                      </label>
                         
                          <div id="card-element">
            <!--             a Stripe Element will be inserted here.
             -->          </div>

                   
                      <div id="card-errors" role="alert"></div>
                    </div>
                   <br>
                     <button class='btn btn-primary' style="width: 100%;">
                          Pay 
                    </button>
    
         </form> 

              <script src='/js/stripe_functions.js'></script>  

      </div>
      <div class="modal-footer">
       
       
           <button id = 'ok_rate' type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">Dismiss</button>
          
        
      </div>
    </div>
  </div>
</div>