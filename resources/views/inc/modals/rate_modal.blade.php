<div class="modal fade" id="rate_modal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel">Rate</h4>
      </div>
      <div class="modal-body">
         

       
           <div style="height: 40px; ">
                <fieldset class="rating" >
     <input onclick="setTrainerRating(this)" type="radio" id="star10" name="rating" value="10" /><label for="star10" title="Rocks!">5 stars</label>
    <input onclick="setTrainerRating(this)" type="radio" id="star9" name="rating" value="9" /><label for="star9" title="Perfect"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star8" name="rating" value="8" /><label for="star8" title="Super Execellent"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star7" name="rating" value="7" /><label for="star7" title="Excellent"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star6" name="rating" value="6" /><label for="star6" title="Great"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Very good"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Good"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Meh"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Kinda bad"></label>
    <input onclick="setTrainerRating(this)" type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Sucks big time"></label>
</fieldset>
            </div>
            

      </div>
      <div class="modal-footer">
       
       
           <button id = 'ok_rate' type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">Dismiss</button>
          
        
      </div>
    </div>
  </div>
</div>