
<style type="text/css">
  
small
{
  font-size: 10px;
}

</style>
<?php $functionToCall = ''; ?>
<div class="modal fade" id="showTrainingListModal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: rgb(92, 19, 120);">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel" style="color: white;">Training list</h4>
          
      </div>
      <div class="modal-body">
     <table class="table table-bordered table-hover" id="tab_logic">
                <thead>
                    <tr>
                        <th class="text-center">
                            Training name
                        </th>
                  </tr>

                </thead>
                <tbody>
                 


              

                 @foreach ($trainingData as $rec)
                   
                    <tr><td id = '{{$rec->id}}' onclick="setSelectedId(this.id)"> <p1>{{ $rec->training_name }} </p1></td> </tr> 
                  @endforeach
                    <tr id='addr0'>
                       
                    </tr>
                    <tr id='addr1'></tr>
                </tbody>
            </table>
           <!--    <div class="form-inline" style="margin-bottom: 10px;">
              <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" placeholder="Search training" />
                    <span class="input-group-btn">
                        <button class="btn btn-info" style="background-color:#9B26AF ; "  type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
               </div> -->
      </div>
      <div class="modal-footer " style="background-color: #68EFAD;">
        <div class="row">
        
         @if($buttonPressedId == 'mainList')  
            <div class = 'col-xs-1'></div> 
          
          <div class = 'col-xs-2 text-center'> 
               
                 <button onclick="showModalForTrainingIputData()" id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE85D;</div>
                 </button>
        
           <div class="row text-center">
                <small>TRACK DATA</small>
           </div> 

             </div> 
<!--            <button id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE3C9;</div></button>
 -->          
          <div class = 'col-xs-2'> 
           <button onclick="loadNewTrainingModal()" id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE89C;</div></button>

            <div class="row text-center">
                <small>ADD</small>
           </div> 

            </div>



           <div class = 'col-xs-2'> 
              <button onclick="openClientList()" id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE154;</div></button>

               <div class="row text-center">
                <small>SEND</small>
           </div> 

           </div>   

           <div class = 'col-xs-2'>  
               <button onclick='deleteTraining()' id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE872;</div></button>

                <div class="row text-center">
                <small>REMOVE</small>
           </div> 

         </div>  

          <div class = 'col-xs-2'>  
           
           <form id= 'visualizeTrainingId' action="trainingProgressShow" method="GET" style="display: none;">
               {{ csrf_field() }}
           </form>
           

           <button onclick='showProgressView()'
           id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE01D;</div></button>

          <!--   <button onclick='showTrainingProgress()'
           id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 100%;"> <div style="margin-top:5px; " class="material-icons">&#xE01D;</div></button> -->

            <div class="row text-center">
                <small>VISUALISE</small>
           </div> 
         </div>
                     <div class = 'col-xs-1'></div> 

           
           @else

                       <button onclick="sendTraining()" id = 'delete_row' type="button" class="btn btn-primary" style="border-radius: 8px; margin: 10px;">
                            <div class = 'row'>
                                                                        <div class = 'col-md-3'>
                                                                            <div class="pull-left">
                                                                                   <div class="material-icons">&#xE154;</div>
                                                                             </div>      
                                                                         </div> 
                                                                         <div class = 'col-md-9'>
                                                                              <div class =' pull-left'>Send training</div>
                                                                         </div> 
                                                                     </div> 

                       </button>

            
           @endif

        </div>

        <div class = 'row'>
              <div class = 'text-center'>
                   <!--   <small>WORKOUT</small> -->
               </div> 
        </div>  
      </div>
    </div>
  </div>
</div>





