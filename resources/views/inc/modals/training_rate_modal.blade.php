
<div class="modal fade" id="training_rate_modal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel">Rate</h4>
      </div>
      <div class="modal-body">
         

       
           <div style="height: 40px; ">
  <fieldset class="rating" >

     @for($i=10;$i>1;$i--)

          @if($rateValue == $i)

             <input onclick="setTrainingRating(this)" type="radio" id="star{{$i}}" name="rating" value="{{$i}}" checked="checked" /><label for="star{{$i}}" title="Rocks!">{{$i}} stars</label>


          @else

              <input onclick="setTrainingRating(this)" type="radio" id="star{{$i}}" name="rating" value="{{$i}}" /><label for="star{{$i}}" title="Rocks!">{{$i}} stars</label>

          @endif



     @endfor

    

</fieldset>
            </div>
            

      </div>
      <div class="modal-footer">
       
       
           <button id = 'ok_rate' type="button" class="btn btn-primary" style="width: 100%;" data-dismiss="modal">Dismiss</button>
          
        
      </div>
    </div>
  </div>
</div>