
<style type="text/css">
  
.navbar-nav .nav-item .nav-link {
    color: red;
}
.navbar-nav .nav-item.active .nav-link,
.navbar-nav .nav-item:hover .nav-link {
    color: pink;
}
}

</style>
    <nav class="navbar navbar-ct-blue navbar-fixed-top navbar-transparent" role="navigation" >
      <div class="container-fluid" style="padding-left: 10px; background-color: rgb(92, 19, 120);">
      
        <!--  <ul class="nav navbar-nav navbar-left">
                   <a href="{{ url('/home') }}">
                        <img height="14" style="margin-top:25px; margin-left:10px; " src="/images/fitnessbook_logo_green.png"   alt="Cinque Terre">
                   </a>     
         </ul> -->
         
           



          <ul class="nav navbar-nav navbar-right" style="a:hover{background-color:red;}">


            <li  style="list-style: none; padding: 0px;">
                              <a href="{{ url('/home') }}"  style="padding-left: 6px; padding-right: 6px; ">
                                 <div class = 'row' >
                                                 <div class = 'col-md-12 text-center' style="padding: 0px;">
                                                      <div class="material-icons green_main" style="color:#68EFAD;">&#xE84E;</div>
                                                  </div>
                                                  <div class = "col-md-12 text-center" style="margin-top:0px; " >
                                                      <small class="green_main"> Body stats</small>
                                                   </div>     

                                   </div>
                                 </a>   

               </li>

              

               
             @if(Auth::user()->type == 'trainer')
            
             <li  style="list-style: none; padding: 0px;">
                                  <form id='clientProgressId' type = "hidden" method="get" action='clientProgress'>
                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                  <div class = 'row' onclick="document.getElementById('clientProgressId').submit();" >
                                                   <div class = 'col-md-12 text-center' style="padding: 0px;">

                                                        <div class="material-icons green_main" >&#xE85C;</div>
                                                    </div>
                                                    <div class = "col-md-12 text-center" style="margin-top:0px; " >
                                                        <small class="green_main">Client progress</small>
                                                    </div>       

                                                   </div>
                                 </a>   

               </li>

               @endif

               @if(Auth::user()->type == 'trainer')
            
             <li  style="list-style: none; padding: 0px;">
                                  <form id='trainerRequestListId' type = "hidden" method="get" action='trainerRequestList'>
                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                  <div class = 'row' onclick="document.getElementById('trainerRequestListId').submit();" >
                                                   <div class = 'col-md-12 text-center' style="padding: 0px;">

                                                        <!-- <div class="material-icons green_main" >&#xE85C;</div> -->
                                                          <img height = '30' src="/images/request.png">
                                                    </div>
                                                    <div class = "col-md-12 text-center" style="margin-top:0px; " >
                                                        <small class="green_main">Coach request (@{{notificationsRequest}})</small>
                                                    </div>       

                                                   </div>
                                 </a>   

               </li>

               @endif
             




              <li  style="list-style: none;">
                                 
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                        <div id='mainList' class = 'row' onclick='loadTrainingListFromDB(this.id)'>
                                                         <div class = 'col-md-12 text-center' style="padding: 0px;">
                                                              <div class="material-icons green_main" style="padding: 0px;">&#xE85D;</div>
                                                         </div>
                                                          
                                                        <div class = "col-md-12 text-center" style="margin-top:0px; " >
                                                              <small class="green_main">Training list</small>
                                                        </div>    

                                        </div>
                                 </a>   

               </li>


              <li  style="list-style: none; ">
                                  <form id="uploadTrainingForm" action="uploadTrainingIndex" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                 </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                  <div class='row' onclick="event.preventDefault();
                                                           document.getElementById('uploadTrainingForm').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                               
                                               <!--  <div class="material-icons green_main" style="padding: 0px;">&#xE2C6;</div> -->

                                                <img height = '30' src="/images/icons8-upload-to-the-cloud-filled-50.png">
                                               
                                         </div>      
                                         
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:0px; " >
                                              
                                              <small class="green_main">Upload training</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>



               <li  style="list-style: none;">

                          <form id="trainingForm" action="findTrainingIndex" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                                 
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                   <div class='row'  onclick="event.preventDefault();
                                                                                      document.getElementById('trainingForm').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                               
                                                <div class="material-icons green_main" style="padding: 0px;">&#xEB43;</div>
                                               
                                         </div>      
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:0px; " >
                                              
                                              <small class="green_main">Find training</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>







                <li  style="list-style: none;">
                                  <form id="findTrainerForm" action="showTrainerList" method="POST" style="display: none;">
                                                     {{ csrf_field() }}
                                           </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                  <div class='row' onclick="event.preventDefault();
                                                              document.getElementById('findTrainerForm').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                               <img height="30" src="/images/icons8-personal-trainer-filled-50.png">
                                         </div>      
                                         
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:0px; " >
                                              
                                              <small class="green_main">Find people</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>

                <li  style="list-style: none;">
                                  <form id="trainingsBought" action="trainingsBought" method="get" style="display: none;">
                                                     {{ csrf_field() }}
                                           </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                               
                                  <div class='row' onclick="event.preventDefault();
                                                              document.getElementById('trainingsBought').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                <div class="material-icons green_main" style="padding: 0px;">&#xE8EF;</div>
                                         </div>      
                                         
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:0px; " >
                                              
                                              <small class="green_main">Trainings bought</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>


                   <li class="dropdown" style=" list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; "
                                               role="button" aria-expanded="false">
                                                
                                                <div class='row'>
                                                      <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                            <img height="30" src="/images/icons8-notification-filled-50.png">
                                                       </div>      
                                                       
                                                       <div class='col-md-12 col-xs-12 text-center' style="margin-top: 0px" >
                                                            
                                                            <small class="green_main">Notifications( @{{notifications}})</small> 
                                                      
                                                       </div>
                                                                         
                                                  </div>  
                                                                   
                                                        
                                                   

                                                     

                                                 
                                            </a>
                                           <ul class="dropdown-menu" role="menu" style="width:320px">
                                             @include('notifications')
                                            </ul>
                                     </li>
                    <li class="dropdown" style=" list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; "
                                               role="button" aria-expanded="false">
                                               <!--  <img src="/images/message_green.png" />
                                                <span class="badge"
                                                      style="background:red; position: relative; top: -15px;">
                                                   <small> @{{messageNotificationsData}}</small>

                                                     
                                                </span> -->
                                                  <div class = 'row'>
                                               
                                                      <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                             <img  height="30" src="/images/icons8-speech-bubble-filled-50.png">
                                                       </div>      
                                                       
                                                       <div class='col-md-12 col-xs-12 text-center'>
                                                              <small class="green_main">Messages (@{{messageNotificationsData}})</small> 
                                                       </div> 

                                                </div>        

                                                
                                            </a>
                                            <ul class="dropdown-menu" role="menu" style="width:350px;">
                                                 <form id='messages_form' action="messages" method="GET" style="display: none;">
                                                            {{ csrf_field() }}
                                                  </form>
                                                
                                               <button onclick="document.getElementById('messages_form').submit()" class = 'text-center' style="background-color: #00000007; border-color: transparent; width: 94%; margin-left:3%;">
                                                  <small ><a>Read messages </a></small>
                                               </button> 
                                               
                                               @include('chat.list_of_chats')
                                            </ul>
                                     </li>
            

             <li class="dropdown" style=" list-style: none;" >
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                                    <?php 
                                                          if(Auth::user()->profile_picture == NULL)
                                                          {
                                                              $imageLink = "/images/icons8-customer-filled-50.png"; 


                                                          }
                                                          else
                                                          {
                                                              

                                                               $imageLink = Auth::user()->profile_picture;

                                                              // if(explode("/", Auth::user()->profile_picture)[0] != 'public')
                                                              // {

                                                              //    $imageLink = Auth::user()->profile_picture;
                                                              // }

                                                              // else
                                                              // {


                                                              //     $imageLink = Storage::disk('do_spaces')->url(Auth::user()->profile_picture);
                                                              //  } 
                                                          }

                                                          ?>




<!--                                                     <img src="{{Storage::url(Auth::user()->profile_picture)}}" width="30px" height="30px" class="img-circle"/>
 --><!-- 
                                                    <div id = 'profilePicId' class = 'profile_empty'  style="height: 30px; width: 30px;  background-image: url('{{$imageLink}}');  border-radius: 50%; ">  
                                                      </div> -->

                                                      <div class='row'>
                                                      
                                                         <div class = 'col-md-9 col-xs-9'>
                                                               
                                                                <div id = 'profilePicId' class = 'profile_empty'  style="height: 50px; width: 50px;  background-image: url('{{$imageLink}}');  border-radius: 50%; ">  
                                                               </div> 
                                                         </div>
  
                                                       </div>   


                                                   
                                                </a>

                                                <ul class="dropdown-menu" role="menu" style="">




                                                    <li> <a href="{{ url('showPicturesView') }}/{{Auth::user()->slug}}" >   Profile  </a> </li>
                                                    <li> <a href="{{ url('settings') }}" >  Edit Profile  </a> </li>

                                                    <li style=" list-style: none;">
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                   document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </li>


                                                </ul>
                                          </li>  <!-- dropdown finish -->
          </ul>
       
      </div><!-- /.container-fluid -->
    </nav>

         