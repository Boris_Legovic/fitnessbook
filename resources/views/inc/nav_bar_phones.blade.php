<style type="text/css">

.dropdown > li {
    float: left;
    width: 25%;
    position: relative;
    padding: 10px;
    box-sizing: border-box;
    background: #EEE;
    cursor: pointer;
}
.dropdown > li ul {
    width: 220px;
    display: none;
    position: absolute;
    top: 100%;
    left: 0;
    background: #DDD;
}
.dropdown > li > ul li {
    float: none;
    display: block;
    width: 100%;
    padding: 5px;
    box-sizing: border-box;
}
.dropdown > li:hover ul {
    display: block;
}
.dropdown > li:last-child:hover ul {
    left: auto;
    right: 0;
}

</style>
<nav class="navbar navbar-ct-blue navbar-fixed-top navbar-transparent" role="navigation" >
<div class = 'container' style="background-color: rgb(92, 19, 120);">
		
					<div class = 'col-xs-4'>			
				       <div class = 'row'>
                  
                  <div class = 'col-xs-6' style="margin-left: -20px;">
                       <li class="dropdown" style=" list-style: none;" >
                                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                                         <?php 
                                                          if(Auth::user()->profile_picture == NULL)
                                                          {
                                                              $imageLink = "/images/icons8-customer-filled-50.png"; 


                                                          }
                                                          else
                                                          {
                                                               // $imageLink = Storage::disk('do_spaces')->url(Auth::user()->profile_picture); 

                                                              $imageLink = Auth::user()->profile_picture;
                                                          }

                                                          ?>



      <!--                                                     <img src="{{Storage::url(Auth::user()->profile_picture)}}" width="30px" height="30px" class="img-circle"/>
       --><!-- 
                                                          <div id = 'profilePicId' class = 'profile_empty'  style="height: 30px; width: 30px;  background-image: url('{{$imageLink}}');  border-radius: 50%; ">  
                                                            </div> -->

                                                            <div class='row'>
                                                            
                                                               <div class = 'col-md-9 col-xs-9'>
                                                                      <div id = 'profilePicId' class = 'profile_empty'  style="height: 40px; width: 40px; margin-top:10px;   background-image: url('{{$imageLink}}');  border-radius: 50%; ">  
                                                                     </div> 
                                                               </div>
        
                                                             </div>   


                                                         
                                                      </a>

                                                      <ul class="dropdown-menu" role="menu">




                                                          <li> <a href="{{ url('showPicturesView') }}/{{Auth::user()->slug}}" >   Profile  </a> </li>
                                                          <li> <a href="{{ url('settings') }}" >  Edit Profile  </a> </li>

                                                          <li style=" list-style: none;">
                                                              <a href="{{ route('logout') }}"
                                                                 onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                                  Logout
                                                              </a>

                                                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                  {{ csrf_field() }}
                                                              </form>
                                                          </li>


                                                      </ul>
                                                </li>  <!-- dropdown finish --> 
                              </div> 
                              
                         <div class = 'col-xs-6'>                      

                <ul class="nav navbar-nav navbar-left">
									 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                          <i style="font-size: 40px; margin-left: 10px; margin-top: 7px; color: white;" class="material-icons">
        									list
        									</i>
                    </a>  
                    
                     <ul class="dropdown-menu" role="menu">




                                                  <li  style="list-style: none; padding: 0px;">
                                                                    <a href="{{ url('/home') }}"  style="padding-left: 6px; padding-right: 6px; ">
                                                                       <div class = 'row' >
                                                                                       <div class = 'col-md-12 text-center' style="padding: 0px;">
                                                                                            <div class="material-icons green_main" style="color:#68EFAD;">&#xE84E;</div>
                                                                                        </div>
                                                                                        <div class = "col-md-12 text-center" style="margin-top:-8px; " >
                                                                                            <small class="green_main"> Body stats</small>
                                                                                         </div>     

                                                                         </div>
                                                                       </a>   

                                                     </li>
                                                   
                                                      @if(Auth::user()->type == 'trainer')
                                                      
                                                      <li  style="list-style: none; padding: 0px;">
                                                                           <form id='clientProgressId' type = "hidden" method="get" action='clientProgress'>
                                                          <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                                                         </form>
                                                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                                                           <div class = 'row' onclick="document.getElementById('clientProgressId').submit();" >
                                                                                            <div class = 'col-md-12 text-center' style="padding: 0px;">

                                                                                                 <div class="material-icons green_main" >&#xE85C;</div>
                                                                                             </div>
                                                                                             <div class = "col-md-12 text-center" style="margin-top:-8px; " >
                                                                                                 <small class="green_main">Client progress</small>
                                                                                             </div>       

                                                                                            </div>
                                                                          </a>   

                                                        </li>

                                                        @endif

                                                          @if(Auth::user()->type == 'trainer')
                                                        
                                                        <li  style="list-style: none; padding: 0px;">
                                                                             <form id='trainerRequestListId' type = "hidden" method="get" action='trainerRequestList'>
                                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                                                           </form>
                                                                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                                                             <div class = 'row' onclick="document.getElementById('trainerRequestListId').submit();" >
                                                                                              <div class = 'col-md-12 text-center' style="padding: 0px;">

                                                                                                   <!-- <div class="material-icons green_main" >&#xE85C;</div> -->
                                                                                                     <img height = '30' src="/images/request.png">
                                                                                               </div>
                                                                                               <div class = "col-md-12 text-center" style="margin-top:0px; " >
                                                                                                   <small class="green_main">Coach request (@{{notificationsRequest}})</small>
                                                                                               </div>       

                                                                                              </div>
                                                                            </a>   

                                                          </li>

                                                          @endif
                                                        

                                                         <li  style="list-style: none;">
                                 
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                        <div id='mainList' class = 'row' onclick='loadTrainingListFromDB(this.id)'>
                                                         <div class = 'col-md-12 text-center' style="padding: 0px;">
                                                              <div class="material-icons green_main" style="padding: 0px;">&#xE85D;</div>
                                                         </div>
                                                          
                                                        <div class = "col-md-12 text-center" style="margin-top:-8px; " >
                                                              <small class="green_main">Training list</small>
                                                        </div>    

                                        </div>
                                 </a>   

               </li>


              <li  style="list-style: none;">
                                  <form id="uploadTrainingForm" action="uploadTrainingIndex" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                 </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                  <div class='row' onclick="event.preventDefault();
                                                           document.getElementById('uploadTrainingForm').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                               
                                               <!--  <div class="material-icons green_main" style="padding: 0px;">&#xE2C6;</div> -->

                                                <img height = '30' src="/images/icons8-upload-to-the-cloud-filled-50.png">
                                               
                                         </div>      
                                         
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:-8px; " >
                                              
                                              <small class="green_main">Upload training</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>



               <li  style="list-style: none;">

                          <form id="trainingForm" action="findTrainingIndex" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                                 
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                   <div class='row'  onclick="event.preventDefault();
                                                                                      document.getElementById('trainingForm').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                               
                                                <div class="material-icons green_main" style="padding: 0px;">&#xEB43;</div>
                                               
                                         </div>      
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:-8px; " >
                                              
                                              <small class="green_main">Find training</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>





                <li  style="list-style: none;">
                                  <form id="findTrainerForm" action="showTrainerList" method="POST" style="display: none;">
                                                     {{ csrf_field() }}
                                           </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                                  <div class='row' onclick="event.preventDefault();
                                                              document.getElementById('findTrainerForm').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                               <img height="22" src="/images/icons8-personal-trainer-filled-50.png">
                                         </div>      
                                         
                                         <div class='col-md-12 col-xs-12 text-center' >
                                              
                                              <small class="green_main">Find trainer</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>

                <li  style="list-style: none;">
                                  <form id="trainingsBought" action="trainingsBought" method="get" style="display: none;">
                                                     {{ csrf_field() }}
                                           </form>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 6px; padding-right: 6px; ">
                               
                                  <div class='row' onclick="event.preventDefault();
                                                              document.getElementById('trainingsBought').submit();">
                                        <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                <div class="material-icons green_main" style="padding: 0px;">&#xE8EF;</div>
                                         </div>      
                                         
                                         <div class='col-md-12 col-xs-12 text-center' style="margin-top:-8px; " >
                                              
                                              <small class="green_main">Trainings bought</small> 
                                        
                                         </div>
                                                           
                                    </div>  
                                 </a>   

               </li>


                                                   

                     </ul>

						      </ul>
              </div> 
            </div>  
				</div>		
				<div class = 'col-xs-4'>
           <a href="{{ url('/home') }}" style="text-decoration: none;">
              <h3 style="color: white; font-size: 15px;">Fitnessbook</h3>
           </a>
				</div>   

					<div class = 'col-xs-4'>
              <div class = 'row'>
                  <div class  ='col-xs-6'>

                 <li class="dropdown" style=" list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 2px; padding-right: 2px; "
                                               role="button" aria-expanded="false">
                                                
                                                <!-- <div class='row'>
                                                      <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                             <img src="/images/notification_main_color.png"  />
                                                       </div>      
                                                       
                                                       <div class='col-md-12 col-xs-12 text-center' >
                                                            
                                                            <small class="green_main">Notifications( @{{notifications}})</small> 
                                                      
                                                       </div>
                                                                         
                                                  </div>   -->

                                                    <div class = 'row'>
                                               
                                                      <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                             <!-- <img src="/images/message_green.png"  /> -->
                                                             <!-- <small class="green_main">(@{{messageNotificationsData}})</small>  -->
                                                                <img height="25" src="/images/icons8-notification-filled-50.png">
                                                                  <span class="badge"
                                                                        style="background:red; position: relative; top: -15px;">
                                                                     <small> @{{notifications}} </small>

                                                                       
                                                                  </span>

                                                       </div>      
                                                       
                                                      

                                                </div>      
                                                                   
                                                        
                                                   

                                                     

                                                 
                                            </a>
                                           <ul class="dropdown-menu  dropdown-menu-right" role="menu" style="width:280px;">
                                             @include('notifications')
                                            </ul>
                </li>

              </div>

             <div class  ='col-xs-6'>

               <li class="dropdown" style=" list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 2px; padding-right: 2px; "
                                               role="button" aria-expanded="false">
                                               <!--  <img src="/images/message_green.png" />
                                                <span class="badge"
                                                      style="background:red; position: relative; top: -15px;">
                                                   <small> @{{messageNotificationsData}}</small>

                                                     
                                                </span> -->
                                                  <div class = 'row'>
                                               
                                                      <div class = 'col-md-12 col-xs-12 text-center' style="padding: 0px;">
                                                             <!-- <img src="/images/message_green.png"  /> -->
                                                             <!-- <small class="green_main">(@{{messageNotificationsData}})</small>  -->
                                                             <!--   <img src="/images/message_green.png" /> -->
                                                           
                                                          
                                                             <img  height="25" src="/images/icons8-speech-bubble-filled-50.png">

                                                                  <span class="badge"
                                                                        style="background:red; position: relative; top: -15px;">
                                                                     <small> @{{messageNotificationsData}} </small>

                                                                       
                                                                  </span>

                                                       </div>      
                                                       
                                                      

                                                </div>        

                                                
                                            </a>
                                            <ul class="dropdown-menu  dropdown-menu-right" role="menu" style="width:300px;">
                                                 <form id='messages_form' action="messages" method="GET" style="display: none;">
                                                            {{ csrf_field() }}
                                                  </form>
                                                
                                               <button onclick="document.getElementById('messages_form').submit()" class = 'text-center' style="background-color: #00000007; border-color: transparent; width: 94%; margin-left:3%;">
                                                  <small ><a>Read messages </a></small>
                                               </button> 
                                               
                                               @include('chat.list_of_chats')
                                            </ul>
                   </li>

                 </div>

           <div class  ='col-xs-4'>
  

					  

           </div>
                                   
       </div>     
					
	</div>
					

			</div>


</nav>
