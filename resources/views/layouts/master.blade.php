<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

    <script>  // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

  </script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    <!-- CSRF Token
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Fitnessbook') }}</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body style="background-color:rgb(249, 248, 250)">
    
    <div    id="app1">
         <div style="margin-bottom: 100px;">

                   <div class = 'hidden-xs'>
                   @include('inc.nav_bar_header_modern')

                    </div>

                     <div class = 'col-xs hidden-md hidden-lg hidden-xl'>
                         @include('inc.nav_bar_phones')
                     </div> 

         </div>

      
   

        @yield('content')
     </div>
    <?php

       if(Auth::check())
        {
            // echo "<script> var app_id =" . Auth::user()->id . "; </script>"; 
        }
   

    ?>


    <!-- Scripts -->
<!--     <script src="{{ asset('js/app.js') }}"></script>
 -->    <script src='/js/addNewTrainingActions.js'></script> 
    <script src='/js/insertTrainingDataActions.js'></script> 
    <script src='/js/showTrainingProgressActions.js'></script> 
    <script src='/js/trackTrainingActions.js'></script>  
    <script src='/js/trainingProgressBarActions.js'></script>  
    <script src='/js/graphs/bodyCharts.js'></script>  
    <script src='/js/bodyModelActionButtons.js'></script>  
    <script src='/js/calorieModelActions.js'></script>  
    <script src='/js/mainActions.js'></script>  
    <script src='/js/profileActions.js'></script>  

    <script src='/js/profilePicturesAction.js'></script>  
    <script src='/js/optionsActions.js'></script>  
    <script src='/js/graphs/kcalGraphs.js'></script>  
    <script src='/js/coachingActions.js'></script>

     <script type="text/javascript" src="{{ URL::asset('js/profile.js') }}"></script>




  





    






</body>
@include('inc.footer');


</html>
