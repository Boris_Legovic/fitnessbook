@extends('layouts.master')

@section('content')



<div class ='container'>
  <div  class = 'row hidden-sm hidden-md hidden-lg hidden-xl' style="height: 200px; overflow-y:scroll;">
    <div class='text-center'>
        <small>Message list</small>
     </div> 
      <div class = 'col-xs-12'>
                <div class = 'col-xs-12' style=" border-radius: 10px;">

          @include('chat.list_of_chats') 
          </div>
     </div>
   </div> 

  


<div class="row" style="padding:10px;">

<!-- ******************LEFT*********************************************
 -->  <div  class="hidden-xs col-md-3 pull-left">
        <div class = 'col-12' style="background-color:#fff; border-radius: 10px;"> 
                <div class="row" style="padding:10px">
                   <div class="col-md-4"> </div>
                   <div class="col-md-6">Messenger</div>
                   <div class="col-md-2 pull-right">
                    <!--  <a href="{{url('/newMessage')}}">
                       <img src="/img/compose.png" title="Send New Messages"></a> -->
                   </div>
                </div>
                     @include('chat.list_of_chats')
               <hr>
          </div> 
  </div>

  <!-- ******************LEFT END********************************************* -->





<!-- ******************CENTER*********************************************
-->


  <div class="col-xs-12 col-md-6 " style="background-color: #fff;  border-radius: 10px;">
   <h3 align="center">Chat</h3>
  
   <div id = 'chatScroll' style="height: 300px; padding-bottom: 55px; overflow-y:scroll;">
   <div v-for="singleMsg in singleMsgs">
    
    <div v-if="singleMsg.user_from != null">


    <div v-if="singleMsg.user_from == <?php echo Auth::user()->id; ?>">
    
          <div class="col-xs-12 col-md-12" style="padding: 5px;" >
            
               <div v-if="singleMsg.profile_picture == null ">

                    <img src= '/images/empty_trainer_icon_with_background.png' style="width:30px; border-radius:100%; margin-left:5px" class="pull-right">
              </div>      
                       

                <div v-else>

                        
                            <img :src= "singleMsg.profile_picture" style="width:30px; border-radius:100%; margin-left:5px" class="pull-right">

                       
                     
                </div>


                   <div style="float:right; background-color:#0084ff; padding:5px 15px 5px 15px;
                        margin-right:10px;color:#333; border-radius:10px; color:#fff;" >
                          @{{singleMsg.msg}}
                  </div>    
                

                  <div class = 'col-md-12 pull-right'>
                      <small style="margin-right: 20px;" class="pull-right">@{{singleMsg.created_at_time}}</small>
                </div>

            
         
         
         </div>
  </div>  


    <div v-else>
        
       <div class="col-md-12 pull-right"  style="padding: 5px;">
             <div v-if="singleMsg.profile_picture == null ">

                     <img src= '/images/empty_trainer_icon_with_background.png' style="width:30px; border-radius:100%; margin-left:5px" class="pull-left">
            </div>

             <div v-else>


                <div v-if="singleMsg.profile_picture.includes('public')">

                      <img :src= "singleMsg.profile_picture" style="width:30px; border-radius:100%; margin-left:5px" class="pull-right">

                <div v-else>
                
                    <img :src= "singleMsg.profile_picture" style="width:30px; border-radius:100%; margin-left:5px" class="pull-right">

                </div>     

                
            </div>  



             <div style="float:left; background-color:#F0F0F0; padding: 5px 15px 5px 15px;
                 border-radius:10px; text-align:right; margin-left:5px ">
                 @{{singleMsg.msg}}
           </div>
          
           <small style="margin-left: 20px;" class="col-md-12 pull-left">@{{singleMsg.created_at_time}}</small>

      </div>



     </div>
   </div>
   </div>
   </div>
 </div><!--  SCROLL END -->
   <hr>

   <input type="hidden" v-model="conID">
   
  <div class = 'row'> 
           <div class = 'class="col-xs-12 col-md-12 '>
            <textarea class="col-xs-12 col-md-12 form-control" v-model="msgFrom" @keydown="inputHandler">
                    
                  </textarea>
           </div> 
   </div> 

      <button style="margin-top: 10px;" v-on:click="sendMsg" class = 'btn btn-primary'>Send</button>




</div>
<!-- ******************CENTER END*********************************************
-->



<!-- ******************RIGHT*********************************************
-->

  <div class="hidden-xs col-md-3" >
        <div class = 'col-12' style="background-color: #fff; border-radius: 10px;"> 
        
                   <div class="text-center" style="padding: 10px;">User to message

                   </div>
                  
                   <div v-else>

                   </div> 

                  <div v-for="user in userToData" >
                          <div class = 'text-center' style="padding: 10%;">
                             
                              <div v-if="user.profile_picture != null ">
                                      <img  style="width:100%;border-radius:5%;" class="text-center" :src=" user.profile_picture">

                              </div>  
                              
                               <div v-else>

                                     <img  style="width:100%;border-radius:5%;" class="text-center" src="/images/empty_trainer_icon_with_background.png">
                                
                               </div>  




                               <form   method="post"  action = "{{URL::to('loadTrainerProfile')}}" enctype="multipart/form-data">
                                   
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                                    <input type = "hidden" name = "id" v-bind:value = "user.id" />

                                    <button class = 'btn btn-primary' style="width:100%; margin-top: 10px;">View profile</button>
                              
                              </form>


                        </div>
                    
                </div><!--  finish for loop -->

       </div> 
  </div>
  <!-- ******************RIGHT END*********************************************
-->

</div>



@endsection