@extends('layouts.app')

@section('content')




<?php $counter = 0;

      $urlToGo = 'showCoachingDetail';

		if(isset($maxNumberOfPicsExcided))
		{
			if($maxNumberOfPicsExcided)
			{
				echo '<script> alert("Max number of pictures excided. Please update Your pictures plan."); </script>';
			}
		}


 ?>

 <div id = 'main' style="background-color: black;">
<div  class="container-fluid " >
      <div  class = 'row'> <!--start row -->
		    			
      					@include('inc.modals.rate_modal')

      					@if($cover != null)
      							<div id = 'coverPicId' class = 'cover_empty' style="border-radius: 10px; background-image: url('{{Storage::disk('do_spaces')->url($cover)}}');">
      					@else
      							<div id = 'coverPicId' class = 'cover_empty'>
      					@endif	

		    		
		    				 <form  id = 'coverImageFormid' method="post"  action = "	{{url('uploadCoverPicture')}}" enctype="multipart/form-data">
		                			<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />

				    					<input type="file" name = "cover_image" id="coverPicFormId" style="display: none;" />
						    			@if($isOwnerOfProfile)
								    			
								    			<div id='coverPictureButtonId' class='btn btn-primary' style="margin-left: 20px; margin-top: 10px; background-color: #00000057; ">Upload cover
								    			
								    			</div>
								    	@endif		
				    	  
				    	   </form>		
				    	</div>
				</div> <!-- end of row -->

			<div  class = 'row' style="background-color: black;"> 
				
<!-- 						<div class = 'col-xs-12 col-md-4' >

 -->							
 						<div class = 'hidden-xs col-md-4' >
 								@if(count($allPictures)>0)


								<!-- this is for left picture before profile -->
								<div id = '{{$allPictures[0]->image}}' class = 'profile_empty' style="width:100%; background-image: url('{{Storage::disk('do_spaces')->url($allPictures[0]->image)}}'); margin-top:10px; border-radius: 10px;">

										<form  id='{{$counter}}formChange' method="post"  action = "{{url('uploadNormalPicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[0]->image}}" />
										             		
										             		 <input type="file" name = "normal_image" id="{{$counter}}fileToSubmit" style="display: none;" />

										            	    



										            	<script type="text/javascript">
										            			$("#{{$counter}}fileToSubmit").change(function(e) 
										            			{
										            			     if(fileIsImage(this))
										            			     {
										            			        
										            			         $( "#{{$counter}}formChange" ).submit();

										            			     }
										            			     else
										            			     {
										            			        alert('Image is not a valid format!');
										            			     }
										            			});

										            	</script>

										 </form>  
 									 @if($isOwnerOfProfile)
										  <button onclick="  document.getElementById('{{$counter}}fileToSubmit').click()" class="transparent_button"  style="background-color: #00000057; margin: 5px; border-radius: 10px;"><i style="color:#ffffff99;" class="material-icons">&#xE439;</i></button>
 									@endif
											
										<form  id='{{$counter}}form' method="post"  action = "{{url('deletePicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[0]->image}}" />
										             
										            

										 </form>  

										@include('inc.modals.pictures_modal');
								  @if($isOwnerOfProfile)
									    <button style="background-color: #00000057; border-radius: 10px;" data-toggle="modal" data-target="#{{$counter}}modal" class='transparent_button'><i style="color:#ffffff99;" class="material-icons">&#xE872;</i></button>
								    @endif
								    	 <?php $counter ++; ?>
								</div>

								@else

									

								  

							@endif
						</div>

						<div class = 'col-xs-1 hidden-md hidden-lg hidden-xl'>

					    </div>		
				
						<div class = 'col-xs-10 col-md-4 col-lg-4' >
								<div class = 'text-center'>
								
									@if($profile != null)														  	 
										
										<div id = 'profilePicId' class = 'profile_picture_empty' style="width:100%; background-image: url('{{$profile}}'); border-radius: 10px; margin-top:10px; ">	

										<!-- @if(explode("/", $profile)[0] != 'public')

												<div id = 'profilePicId' class = 'profile_picture_empty' style="width:100%; background-image: url('{{$profile}}'); border-radius: 10px; margin-top:10px; ">	

										@else

										<div id = 'profilePicId' class = 'profile_picture_empty' style="width:100%; background-image: url('{{Storage::disk('do_spaces')->url($profile)}}'); border-radius: 10px; margin-top:10px; ">	

										@endif		 -->
									@else
									   <div id = 'profilePicId' class = 'profile_picture_empty' style="width:100%;border-radius: 10px; margin-top: 10px;">
									@endif	
										 <form  id = 'profileImageFormid' method="post"  action = "	{{url('uploadProfilePicture')}}" enctype="multipart/form-data">


               								
               								 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />


										 <input type="file" name = "profile_image" id="profilePicFormId" style="display: none;" />


											<div  class = 'hidden-xs' style="height: 140px;">
											</div>

											<div class="col-xs" style="height: 170px;">
											</div>
											
											@if($isOwnerOfProfile)
												<div id = 'profilePictureButtonId' class='btn btn-primary' style="width: 90%; margin: 9px; bottom: 0; background-color: #00000057;">
														Upload profile picture
												</div>
										    @endif		
										</form>
									</div> <!-- text-center  end-->

								  <!--user data -->
								  @include('inc.user_data')

									
									
								</div> <!-- middle row end -->
								     
											@if($isOwnerOfProfile)
											 <form  id = 'normalImageFormid' method="post"  action = "{{url('uploadNormalPicture')}}" enctype="multipart/form-data">
			               							     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />	
			               							     <input type="file" name = "normal_image" id="uploadNormalPicturesFormId" style="display: none;" />
													<div id='uploadNormalPicturesId' style="width: 100%; margin-top: 10px; margin-bottom: 10px;" class='btn btn-primary'>Upload more pictures
													</div>
											  </form>
										    @else


										    <form  id = 'normalImageFormid' method="post"  action = "{{URL::to('newMessageTo')}}" enctype="multipart/form-data">
										    			
										    			 <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />	
			               							     <input type="hidden" name = "user_to_message" id="id" value="{{$profile_id}}"  />

										    	<button id='sendMessageId' style="width: 100%; margin-top: 10px;" class='btn btn-primary'>Send message
														
												</button>

										   </form>		

										   	@if($personalData[0]->type == 'trainer')
												   		@if($personalData[0]->onlineCoaching == 'Yes')
													 		@if($personalData[0]->coaching_price == 0)
													 			<?php
													 				      $urlToGo = 'goToFreeCoachingRequest';

													 		    ?>  


													 	     	@else
																	 

                                                 				@endif
                                                 				
                                                 					 <form   method="post"  action = "{{URL::to( $urlToGo)}}" enctype="multipart/form-data">
						                                                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
						                                                                    <input id = 'coachId' type = "hidden" name = "coachId" value = "{{$profile_id}}" />

						                                                               <button class='btn btn-primary'  style="width: 100%; margin-top: 10px;">Request coaching</button>

						                                                 </form>

                                                  @else



                                                  	<button onclick="alert('Coach did not set online coaching.')" class='btn btn-primary'  style="width: 100%; margin-top: 10px;">Request coaching</button>


                                                  @endif          

											 <button onclick='showRating()' class='btn btn-primary' style="width: 100%; margin-top: 10px;">Rate</button>

											@endif


											@endif	
								     
						
					
				
			</div>

				  <div class="col-xs-1 hidden-md hidden-lg hidden-xl">	
				  </div>	

						<div class = 'hidden-xs col-md-4'>
								@if(count($allPictures)>1)

								<div id = 'profilePicId' class = 'profile_empty' style="width:100%; background-image: url('{{Storage::disk('do_spaces')->url($allPictures[1]->image)}}'); margin-top: 10px; border-radius: 10px;">	


										<form  id='{{$counter}}formChange' method="post"  action = "{{url('uploadNormalPicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[1]->image}}" />
										             		
										             		 <input type="file" name = "normal_image" id="{{$counter}}fileToSubmit" style="display: none;" />

										            	    



										            	<script type="text/javascript">
										            			$("#{{$counter}}fileToSubmit").change(function(e) 
										            			{
										            			     if(fileIsImage(this))
										            			     {
										            			        
										            			         $( "#{{$counter}}formChange" ).submit();

										            			     }
										            			     else
										            			     {
										            			        alert('Image is not a valid format!');
										            			     }
										            			});

										            	</script>

										 </form>  

										  @if($isOwnerOfProfile)
										  <button onclick="  document.getElementById('{{$counter}}fileToSubmit').click()" class="transparent_button"  style="background-color: #00000057; margin: 5px; border-radius: 10px;"><i style="color:#ffffff99;" class="material-icons">&#xE439;</i></button>
										  @endif
										 		
										<form  id='{{$counter}}form' method="post"  action = "{{url('deletePicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[1]->image}}" />
										             
										            

										 </form>  
										@include('inc.modals.pictures_modal');
								    @if($isOwnerOfProfile)
									    <button style="background-color: #00000057;  border-radius: 10px;" data-toggle="modal" data-target="#{{$counter}}modal" class='transparent_button'><i style="color:#ffffff99;" class="material-icons">&#xE872;</i></button>
								   @endif 
								    	 <?php $counter ++; ?>

								</div>


							@endif
						</div>
				</div>		
					
					<div class  ='row' >	
						
						

							<div class = 'col-xs-6 hidden-md hidden-lg hidden-xl'  style="padding: 5px;" >
 							
 								@if(count($allPictures)>0)


								<!-- this is for left picture before profile -->
								<div id = '{{$allPictures[0]->image}}' class = 'profile_empty' style="width:100%; background-image: url('{{Storage::disk('do_spaces')->url($allPictures[0]->image)}}'); margin-top:10px; border-radius: 10px;">

										<form  id='0formChange' method="post"  action = "{{url('uploadNormalPicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[0]->image}}" />
										             		
										             		 <input type="file" name = "normal_image" id="0fileToSubmit" style="display: none;" />

										            	    



										            	<script type="text/javascript">
										            			$("#0fileToSubmit").change(function(e) 
										            			{
										            			     if(fileIsImage(this))
										            			     {
										            			        
										            			         $( "#0formChange" ).submit();

										            			     }
										            			     else
										            			     {
										            			        alert('Image is not a valid format!');
										            			     }
										            			});

										            	</script>

										 </form>  
								 	 @if($isOwnerOfProfile)
		 
										  <button onclick="  document.getElementById('0fileToSubmit').click()" class="transparent_button"  style="background-color: #00000057; margin: 5px; border-radius: 10px;"><i style="color:#ffffff99;" class="material-icons">&#xE439;</i></button>
									@endif
											
										<form  id='0form' method="post"  action = "{{url('deletePicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[0]->image}}" />
										             
										            

										 </form>  

										@include('inc.modals.pictures_modal');
								   @if($isOwnerOfProfile)

									    <button style="background-color: #00000057; border-radius: 10px;" data-toggle="modal" data-target="#0modal" class='transparent_button'><i style="color:#ffffff99;" class="material-icons">&#xE872;</i></button>
								   @endif	    
								    	
								</div>

								@else

									

								  

							@endif
						</div>

							<div class = 'col-xs-6 hidden-md hidden-lg hidden-xl'   style="padding: 5px;">
 								@if(count($allPictures)>1)


								<!-- this is for left picture before profile -->
								<div id = '{{$allPictures[1]->image}}' class = 'profile_empty' style="width:100%; background-image: url('{{Storage::disk('do_spaces')->url($allPictures[1]->image)}}'); margin-top:10px; border-radius: 10px;">

										<form  id='1formChange' method="post"  action = "{{url('uploadNormalPicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[1]->image}}" />
										             		
										             		 <input type="file" name = "normal_image" id="1fileToSubmit" style="display: none;" />

										            	    



										            	<script type="text/javascript">
										            			$("#1fileToSubmit").change(function(e) 
										            			{
										            			     if(fileIsImage(this))
										            			     {
										            			        
										            			         $( "#1formChange" ).submit();

										            			     }
										            			     else
										            			     {
										            			        alert('Image is not a valid format!');
										            			     }
										            			});

										            	</script>

										 </form>  
										 @if($isOwnerOfProfile)

											  <button onclick="  document.getElementById('1fileToSubmit').click()" class="transparent_button"  style="background-color: #00000057; margin: 5px; border-radius: 10px;"><i style="color:#ffffff99;" class="material-icons">&#xE439;</i></button>
										  @endif
											
										<form  id='1form' method="post"  action = "{{url('deletePicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$allPictures[0]->image}}" />
										             
										            

										 </form>  

										@include('inc.modals.pictures_modal');
								  @if($isOwnerOfProfile)
 
								    <button style="background-color: #00000057; border-radius: 10px;" data-toggle="modal" data-target="#1modal" class='transparent_button'><i style="color:#ffffff99;" class="material-icons">&#xE872;</i></button>
								  @endif  
								    	
								</div>

								@else

									

								  

							@endif
						</div>



						@foreach($allPictures as $rec)
							@if($counter > 3)
							
								@if($counter <= $picturesToShow)
									
							<div class='col-xs-6 col-md-4' style="padding: 5px;">
									<div   class = 'profile_empty'  style=" width:100%;  background-image: url('{{Storage::disk('do_spaces')->url($rec->image)}}'); border-radius: 10px;">	
									@if($isOwnerOfProfile)

											<form  id='{{$counter}}formChange' method="post"  action = "{{url('uploadNormalPicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$rec->image}}" />
										             		
										             		 <input type="file" name = "normal_image" id="{{$counter}}fileToSubmit" style="display: none;" />

										            	    



										            	<script type="text/javascript">
										            			$("#{{$counter}}fileToSubmit").change(function(e) 
										            			{
										            			     if(fileIsImage(this))
										            			     {
										            			        
										            			         $( "#{{$counter}}formChange" ).submit();

										            			     }
										            			     else
										            			     {
										            			        alert('Image is not a valid format!');
										            			     }
										            			});

										            	</script>

										 </form>  
 										@if($isOwnerOfProfile)
											  <button onclick="  document.getElementById('{{$counter}}fileToSubmit').click()" class="transparent_button"  style="background-color: #00000057; margin: 5px; border-radius: 10px;"><i style="color:#ffffff99;" class="material-icons">&#xE439;</i></button>

										 	@endif							

										
										<form  id='{{$counter}}form' method="post"  action = "{{url('deletePicture')}}" enctype="multipart/form-data">
										                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
										                     <input type = "hidden" name = "picture" value = "{{$rec->image}}" />
										             
										            

										 </form>   

											@include('inc.modals.pictures_modal');
								    @if($isOwnerOfProfile)
									    <button  data-toggle="modal" data-target="#{{$counter}}modal" class='transparent_button' style="background-color: #00000057;  border-radius: 10px;"><i style="color:#ffffff99;" class="material-icons">&#xE872;</i>
									    </button>
									    @endif
								    
								     @endif	
								    @endif		
								</div>
								
							
						
						</div>
						
						@endif
						 <?php $counter ++; ?>
						@endforeach
					</div>
			</div>
    </div>
</div>











@endsection
