@extends('layouts.app')

@section('content')

 <div id = 'main'>
  <div  class="container " style="padding-left: 0;" id = 'containerId' >
   
       <div class="row" >
               <div class="col-xs-1 col-md-3"  >
              </div>   
              <div class="col-xs-10 col-md-6  backgroundNeutralDiv" style='padding: 20px; border-radius: 10px;' >
                     		
                     				 <div class = 'row' style="padding: 5px;">
										<div class = 'col-xs-3 col-md-3'>
												<div class ='pull-left'> Name </div>
									    </div>			
										<div class = 'col-xs-9 col-md-9'>
												<div class ='pull-left'>  <input id ='userNameInput' class = 'form-control' type="text" name="priceOfCoaching" placeholder="{{$userData->name}}"></div>
										</div>
									</div>	

									<div class = 'row' style="padding: 5px;">
										<div class = 'col-xs-3  col-md-3'>
												<div class ='pull-left'> Surname </div>
									    </div>			
										<div class = 'col-xs-9 col-md-9'>
												<div class ='pull-left'>  <input id='userLastnameInput' class = 'form-control' type="text" name="priceOfCoaching" placeholder="{{$userData->lastname}}"> </div>
										</div>
									</div>	

									<!-- <div class = 'row' style="padding: 5px;">
										<div class = 'col-md-3'>
												<div class ='pull-left'> Age </div>
									    </div>			
										<div class = 'col-md-9'>
												<div class ='pull-left'>  <input class = 'form-control' type="text" name="priceOfCoaching" placeholder="30"> </div>
										</div>
									</div>	 -->

									<div class = 'row' style="padding: 5px;">
										<div class = 'col-xs-3 col-md-3'>
												<div class ='pull-left'> Proffesion </div>
									    </div>			
										<div class = 'col-xs-9 col-md-9'>
												<div class ='pull-left'> {{$userData->type}} </div>
										</div>
									</div>

									<div class = 'row' style="padding: 5px;">
									
										<div class = 'col-xs-3 col-md-3'>
												<div class ='pull-left'>Max number of pictures in profile:</div>
									    </div>			
									
										<div class = 'col-xs-9 col-md-9'>
												 <label class="radio-inline">
												 	
												 	<script src="https://js.stripe.com/v3/"></script>
												 	 
												 	  <form name='stripe-form' action="{{url('payPictures')}}" method="post" id="payment-form">
												                      <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
												                          
												                    

																              

																 	@if($userData->max_number_of_pictures == '15')
																        <input  type="radio" name="picturesNumber" value = '15' checked="checked">
																    @else
																    	 <input  type="radio" name="picturesNumber" value = '15'>
																    @endif    
																        <p id = 'secondUnitIdLabel'>15 pictures (Free Licence)</p>
																   
																    @if($userData->max_number_of_pictures == '30')
																         <input  type="radio" name="picturesNumber" value = '30' checked="checked">
																     @else  
																     	<input  type="radio" name="picturesNumber" value = '30'>

																     @endif 
																     <p id = 'secondUnitIdLabel'>30 pictures - 48$/year (4$/monthly)</p>
																    
																  
																    @if($userData->max_number_of_pictures == '75')
																         <input  type="radio" name="picturesNumber" value = '75' checked='checked'>
																     @else
																   		 <input  type="radio" name="picturesNumber" value = '75'>

																    @endif
																        <p id = 'secondUnitIdLabel'>75 pictures - 96$/year (8$/monthly)</p>
																  
																    @if($userData->max_number_of_pictures == '150')
																         	<input  type="radio" name="picturesNumber" value = '150' checked="checked">
																    @else
																        	<input  type="radio" name="picturesNumber" value = '150'>

															        @endif 	
																        <p id = 'secondUnitIdLabel'>150 pictures - 168$/year (14$/monthly)</p>
																      
																      <div  id = 'picturePaymentContainer' class = 'hidden'>  
																	        
																	        <div class="form-row ">
																	                      <label for="card-element">
																	                        Credit or debit card
																	                      </label>
																	                         
																	                          <div id="card-element">
																	            <!--             a Stripe Element will be inserted here.
																	             -->          </div>

																	                   
																	                      <div id="card-errors" role="alert"></div>
																	         </div>
																	                   <br>
																	                     <button class='btn btn-primary' style="width: 100%;">
																	                          Pay
																	                    </button>
																    </div>
												         </form> 

												          <script src='/js/stripe_functions.js'></script> 
												</label>
										</div>
									</div>	

										
												

												     
									


								 @if($userData->type == 'trainer')
									Online coaching options
									<div class = 'row' style="padding: 5px;">
										<div class = 'col-xs-3 col-md-3'>
												<div class ='pull-left'>Coaching period</div>
									    </div>			
										<div class = 'col-xs-9 col-md-9'>
												
												 <label class="radio-inline">
												 	 @if($userData->online_coaching_period == '1')
												        <input id='coachingPeriod' type="radio" name="coachingPeriod" value = '1' checked="checked">
												     @else
												     	<input id='coachingPeriod' type="radio" name="coachingPeriod" value = '1'>

												     @endif   
												        <p id = 'secondUnitIdLabel'>One month</p>
												    @if($userData->online_coaching_period == '2')

												         <input id='coachingPeriod' type="radio" name="coachingPeriod" value = '2' checked="checked">
												     @else
												     	<input id='coachingPeriod' type="radio" name="coachingPeriod" value = '2'>

												     @endif    
												        <p id = 'secondUnitIdLabel'>Two months</p>
												      
												     @if($userData->online_coaching_period == '3')
												         <input id='coachingPeriod' type="radio" name="coachingPeriod" value = '3' checked="checked">
												     @else
												     	<input id='coachingPeriod' type="radio" name="coachingPeriod" value = '3'>

												     @endif    
												        <p id = 'secondUnitIdLabel'>Three months</p>

												     @if($userData->online_coaching_period == '4')
												         <input id='coachingPeriod' type="radio" name="coachingPeriod" value = '4' checked="checked">
												     @else
												     	<input id='coachingPeriod' type="radio" name="coachingPeriod" value = '4'>

												     @endif    
												        <p id = 'secondUnitIdLabel'>Four months</p>
												</label>

												
										</div>
									</div>	


										 <div class = 'row' style="padding: 5px;">
											<div class = 'col-xs-3 col-md-3'>
													<div class ='pull-left'> Online coaching </div>
										    </div>			
											<div class = 'col-xs-9 col-md-9'>
													<div class ='pull-left'>  <label class="radio-inline">
												    
												  		<?php 
												  				$isEnabled = ''; 

												  				if($userData->stripe_id == '')
												  				{
												  					$isEnabled = 'disabled';
												  				}
												  				


												  		?>
												

													      @if($userData->onlineCoaching == 'Yes')
													       
													        <input  {{$isEnabled}}  id='coachingTrueId' type="radio" name="onlineCoaching" value = 'Yes' checked="checked">

													         <input  {{$isEnabled}} id='coachingTrueId' type="radio" name="onlineCoaching" value = 'No'>


													      @else
													       
													          <input  {{$isEnabled}} id='coachingTrueId' type="radio" name="onlineCoaching" value = 'Yes'>
													            <div class = 'row'>
													            		
													            		<div class = 'col-xs-1 col-md-1'>

													            		 		<p id = 'secondUnitIdLabel'>Yes</p> 
													            		</div> 	

													            		<div class = 'col-xs-11 col-md-11'>	

													            		  @if($isEnabled == 'disabled')
													        				  <p style="color:red;"> Connect with stripe for enable online coaching! </p>
													          			  @endif

													          			  </div> 
													            </div> 
													             
													          

													      @endif
													      
												      
												     @if($userData->onlineCoaching == 'No')
												         <input id='coachingFalseId' type="radio" name="onlineCoaching" value = 'No' checked="checked">
												     @else
												     	<input id='coachingFalseId' type="radio" name="onlineCoaching" value = 'No'>

												     @endif
												        <p id = 'secondUnitIdLabel'>No</p>
												        
												   
												</label> </div>
											</div>
										</div>	




									<div class = 'row' style="padding: 5px;">
										<div class = 'col-xs-3 col-md-3'>
												<div class ='pull-left'> Set coaching price </div>
									    </div>			
										<div class = 'col-xs-9 col-md-9'>
												<div  class ='pull-left'> 
													@if($userData->stripe_id != '')
														<input id = 'coachingPriceId' class = 'form-control' type="text" name="priceOfCoaching" placeholder="{{$userData->coaching_price}}"> 
												    @else
												        	<input   id = 'coachingPriceId' class = 'form-control' type="text" name="priceOfCoaching" placeholder="{{$userData->coaching_price}}"> 
												     @endif	
												</div>
										</div>

							
									</div>

									

									    <div class = 'row' style="padding: 5px;">
											<div class = 'col-xs-3 col-md-3'>
													<div class ='pull-left'> Price currency </div>
										    </div>			
											<div class = 'col-xs-9 col-md-9'>
													<div class ='pull-left'>  <label class="radio-inline">
												 @if($userData->currency == '$')
												        <input  type="radio" name="priceCurrency" value = '$' checked="true">
												 @else      
												 		<input  type="radio" name="priceCurrency" value = '$' >
												 @endif

												        <p id = 'secondUnitIdLabel'>$</p>
												  @if($userData->currency == '€')  
												         <input  type="radio" name="priceCurrency" value = '€' checked="true">
												  @else     
												  		<input  type="radio" name="priceCurrency" value = '€' >
												  @endif
												        <p id = 'secondUnitIdLabel'>€</p>
												        
												   
												</label> </div>
											</div>
										</div>	
								@endif
										
 								


 								@if($userData->stripe_id == '')
									    Connect credit card for recive payments
										<a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_CIc4uY7wp8wK6Lfry97IC6H0WdsDlx1a&scope=read_write"><div class = 'btn btn-primary'>Connect with stripe</div></a> 
										

								@else
									Payment platform status :  
										<a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_CIc4uY7wp8wK6Lfry97IC6H0WdsDlx1a&scope=read_write"><div class = 'btn btn-primary' style="background-color: green;">Connected</div></a> 
										
										<div id='stripeNull'></div>
								@endif		


					
											<div class = 'row' style="padding: 5px;">
											<div class = 'col-xs-3 col-md-3'>
													<div class ='pull-left'> Weight unit</div>
										    </div>			
											<div class = 'col-xs-9 col-md-9'>
													<div class ='pull-left'>  <label class="radio-inline">
												  @if($userData->weight_unit == 'Kg')
												        <input  type="radio" name="weightUnit" value = 'Kg' checked="checked">
												  @else
												  	    <input  type="radio" name="weightUnit" value = 'Kg'>
												  @endif     
												        <p id = 'secondUnitIdLabel'>Kg</p>
												   @if($userData->weight_unit == 'Lbs')     
												         <input  type="radio" name="weightUnit" value = 'Lbs' checked="checked">
												   @else
												   		<input  type="radio" name="weightUnit" value = 'Lbs'>

												   @endif    

												        <p id = 'secondUnitIdLabel'>Lbs</p>
												        
												   
												</label> </div>
											</div>
										</div>		
										
										<div class = 'row' style="padding: 5px;">
											<div class = 'col-xs-3 col-md-3'>
													<div class ='pull-left'> Size unit</div>
										    </div>			
											<div class = 'col-xs-9 col-md-9'>
													<div class ='pull-left'>  <label class="radio-inline">
												    @if($userData->size_unit == 'Cm')
												        <input  type="radio" name="sizeUnit" value = 'Cm' checked="checked">
												    @else
												        <input  type="radio" name="sizeUnit" value = 'Cm'>

												    @endif

												        <p id = 'secondUnitIdLabel'>Cm</p>
												       
												     @if($userData->size_unit == 'Inch')
												         <input  type="radio" name="sizeUnit" value = 'Inch' checked="checked">
												     @else
												     	<input  type="radio" name="sizeUnit" value = 'Inch'>

												  		@endif    
												        <p id = 'secondUnitIdLabel'>Inch</p>
												        
												   
												</label> </div>
											</div>
										</div>															


									<!-- 	<div class = 'btn btn-primary' style="width: 100%; margin-top: 15px;">
												Save
										</div> -->


										




												<!--  <label class="radio-inline">
												        <input id='coachingPeriod' type="radio" name="priceCurrency" value = '$'>
												        <p id = 'secondUnitIdLabel'>$</p>
												         <input id='coachingPeriod' type="radio" name="coachingPeriod" value = '€'>
												        <p id = 'secondUnitIdLabel'>€</p>
												        
												   
												</label>

	 -->
            	</div>

               <div class="col-xs-1 col-md-3"  >
                      
              </div> 
        </div>
  </div>
</div>


<script type="text/javascript">
	
	var stripe = document.getElementById('stripeNull');
    console.log('stripe ', stripe);
	function checkIfStripeNull()
	{
		if(stripe == null)
		{
			alert('Connect stripe account to recive payments!');
			document.getElementById('coachingFalseId').checked = true;
		}
	}

	function checkUserHaveStripe()
	{
		
		var stripe = document.getElementById('stripeNull');

		if(stripe == null)
		{
			alert('Connect stripe account to recive payments!');
			 var price = $( "#coachingPriceId" ).val(0);


		}
	}

</script>








@endsection
