<?php $numberOfData = 0;  
      $max = count($trainerData);
        
      $searchTypeCheck_SearchByName = '';
      $searchTypeCheck_SearchByTraining = '';

      $orderByCheck_LowestPrice = '';
      $orderByCheck_HighestPrice = '';
      $orderByCheck_HighestRate = '';
      $orderByCheck_NumberOfRates = '';

      $filterByCheck_FreeAndPaid = '';
      $filterByCheck_Free = '';
      $filterByCheck_Paid = '';
      $checkedTrainer = '';
      $checkedAthlete = '';

      if($isAthlete)
      {
           $checkedAthlete = 'checked';
      }
      else
      {
          $checkedTrainer  = 'checked';
        

      }
    



?>

@extends('layouts.app')

@section('content')

        <div class = 'container' id = 'containerId'>
              <div style="margin-top: 30px;">
              </div>
                      <div class = 'row backgroundNeutralDiv' style=" padding: 20px; border-radius: 7px; margin: 5px;">
                        <div class = 'col-md-6'>
                            
                           
                             
                               <div class="input-group">
                                  <input id = 'searchInputId' type="text" class="form-control" placeholder="Seach" name="searchBarValue">
                                 
                                  <span class="input-group-btn">
                                       
                                       <button onclick="searchFilterTrainer()" class="btn btn-primary material-icons">&#xE8B6;</button> 
                                          
                                       
                                      <!--   <button style="margin-left:5px;" class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Search type
                                           <span class="caret"></span></button> -->
                                      
                                      <!--  <div class="dropdown-menu" role="menu" aria-labelledby="menu1" style="padding: 5px;">
                                             <div>
                                                <label class="radio-inline">
                                                  <input  {{$searchTypeCheck_SearchByName}} type="radio" name="searchType" value = 'owner'>Search by owner name
                                                </label>
                                             </div>  
                                             <div> 
                                                <label class="radio-inline">
                                                  <input  {{$searchTypeCheck_SearchByTraining}}  type="radio" name="searchType" value = 'training'>Search by training name
                                                </label>
                                             </div>
                                            
                                           </div>  --> 
                                             
                                               
                                           
                                    </span>
                               </div>
                             
                           

                          
                        </div>
                         <div class = 'col-xs-12 hidden-md hidden-sm hidden-lg hidden-xl' style="padding:2px;">

                        </div>  
                     <div class = 'col-md-3' >
                         <div class = 'row'>
                              <div class="col-md-6">
                                     <button  style="width: 100%;" class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Order by
                                     <span class="caret"></span></button>
                                     <div class="dropdown-menu" role="menu" aria-labelledby="menu1" style="padding: 5px;">
                                      <!--  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Lowest price first</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest price first</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest rate</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Number of rates</a></li>   -->  
                                         <div>
                                                <label class="radio-inline">
                                                  <input    onclick="searchFilterTrainer()"  type="radio" name="orderBy"  {{$orderByCheck_LowestPrice}} value = 'lowest_first'>Lowest price first
                                                </label>
                                             </div>  
                                             <div> 
                                                <label class="radio-inline">
                                                  <input  onclick="searchFilterTrainer()" type="radio" name="orderBy"  {{$orderByCheck_HighestPrice}} value = 'highest_first'>Highest price first
                                                </label>
                                             </div>
                                              <div> 
                                                <label class="radio-inline">
                                                  <input  onclick="searchFilterTrainer()" type="radio" name="orderBy"  {{$orderByCheck_HighestRate}} value = 'highest_rate'>Highest rate
                                                </label>
                                             </div>
                                              <div> 
                                                <label class="radio-inline">
                                                  <input onclick="searchFilterTrainer()" type="radio" name="orderBy"  {{$orderByCheck_NumberOfRates}}  value = 'number_of_rates'>Number of rates
                                                </label>
                                             </div>
                                     </div>
                              </div>
                                <div class="col-md-6">
                                    
                                   
                                            <div class = 'row'>
                                                <label class="radio-inline">
                                                  <input    onclick="searchFilterTrainer()"  type="radio" name="searchFor"  value = 'trainer' {{$checkedTrainer}}>Trainer
                                                </label>
                                            </div>
                                             <div class = 'row'>
                                            
                                                <label class="radio-inline">
                                                  <input  onclick="searchFilterTrainer()" type="radio" name="searchFor" value = 'athlete' {{$checkedAthlete}}>Athlete
                                                </label>
                                            </div>
                                          </div>
                                              
                                          
                             
                       
                               </div>   

                     <div class = 'col-md-2'>
                        
                        
                     </div>   
                        
                        
              </div>
            </div>
         
<!--               **************start with users***********
 -->              <div class = 'transparent' style="background-color: white;">
                    <div class = 'row' >
      @foreach ($trainerData as $rec)
                 
                    
                        
                             
                        <form  id = 'loadTrainerProfile{{$numberOfData}}' method="post"  action = "{{URL::to('loadTrainerProfile')}}" enctype="multipart/form-data">

                     
                       <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />

                                                                             <input id='coachId' type = "hidden" name = "id" value = "{{$rec->trainer_id}}" />

                           <div class = 'col-xs-12 col-md-3 col-lg-3' onclick="document.getElementById('loadTrainerProfile{{$numberOfData}}').submit()" >
                                  <div class = 'text-center backgroundNeutralDiv'  style="margin: 0px; margin-bottom: 20px;  padding: 10px; border-radius: 10px;">
                                   <?php 
                                     
                                      if($rec->profile_picture == NULL)
                                      {
                                        $imageLink = '/images/empty_trainer_icon_with_background.png'; 
                                      }
                                      else
                                      {

                                        if(explode("/", $rec->profile_picture)[0] != 'public')
                                        {

                                            $imageLink = $rec->profile_picture; 

                                        }
                                        else
                                        {

                                             $imageLink = Storage::disk('do_spaces')->url($rec->profile_picture); 
                                        }
                                      

                                       
                                      }


                                       
                                   ?>
                                          <div id = 'profilePicId' class = 'profile_empty hidden-xs'  style="height: 220px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 5px;">  
                                            </div>
                                            
                                              <div id = 'profilePicId' class = 'profile_empty hidden-md hidden-sm hidden-lg hidden-xl'  style="height: 350px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 5px;">  
                                                  </div>
                                         
                                         <div class="text-center">
                                         </div>

                                                
                                                  

                                                      
                                                
                                  
                                            <!--  <div class = 'text-center'> -->
                                                   
                                                     <p>{{$rec->name}}&nbsp;{{$rec->lastname}} </p>
                                                    @if(!$isAthlete) 
                                                         @if($rec->number_of_reviews == 1)
                                                             <p> {{$rec->number_of_reviews}} review</p>
                                                         @elseif($rec->number_of_reviews == null)
                                                             <p> 0 reviews</p>
                                                          @else
                                                             <p> {{$rec->number_of_reviews}} reviews</p>
                                                         @endif
                                                   
                                                       
                                                       @for($i=1;$i<11;$i++)

                                                              @if($rec->rate >= $i)

                                                                 <span style="color:orange;">★</span>


                                                              @else


                                                                  <span>★</span>

                                                              @endif



                                                         @endfor
                                                       
                                                     @if($rec->about == NULL)
                                                     <p>No personal information</p>
                                                     @else
                                                       <p>{{$rec->about}}</p>
                                                     @endif

                                           
                                            <!--  </div> -->
                                             <!--   <div class="col-md-12"> -->
                                                  <!--   <div class = 'text-center'> -->
                                                      
                                                       @if($rec->onlineCoaching == 'Yes')                    
                                                               @if($rec->coaching_price == 0)
                                                                
                                                               @else
                                                                     <p>Coaching period : {{$rec->online_coaching_period}}</p>
                                                                     
                                                                     <p>Coaching price: {{$rec->coaching_price}}{{$rec->currency}}</p>
                                                               @endif

                                                                <!--  <button class='btn btn-primary' style="width: 100%; margin-bottom: 10px;">View profile</button> -->
                                                           @else   

                                                           <p>Coaching period : Not set</p>

                                                            <p>Coaching price: Not set</p>

                                                          @endif    
                                                               @endif

                                                             
                                                              <form   method="post"  action = "{{URL::to('showCoachingDetail')}}" enctype="multipart/form-data">
                                                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />
                                                                    <input id = 'coachId' type = "hidden" name = "coachId" value = "{{$rec->trainer_id}}" />
                                                            @if(!$isAthlete)
                                                             <!--   <button class='btn btn-primary'  style="width: 100%;">Request coaching</button> -->
                                                            @endif    
                                                            </form>
                                                            
                                                    <!--  </div> -->

                                              </div>
                                  </div>


                           
                               <?php $numberOfData ++; ?>
                              @if($numberOfData == $max)
                                 
                                   </div>
                              @endif     
                                </form>
                      @endforeach

    
             
                </div>
            </div>
          </div>

            @if($is_error)
                <?php echo "<script>alert('You can not message Yourself!');</script>;"; ?>
            @endif
        <!-- </div> -->

      <script src='/js/filterActions.js'></script>  
@endsection




<!-- 
<script type="text/javascript">
    
    $("#input-id").rating();





</script>
 -->


