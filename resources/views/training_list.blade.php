<?php $numberOfData = 0;  
      $max = count($trainingData);
     
      
      
      $searchTypeCheck_SearchByName = '';
      $searchTypeCheck_SearchByTraining = '';

      $orderByCheck_LowestPrice = '';
      $orderByCheck_HighestPrice = '';
      $orderByCheck_HighestRate = '';
      $orderByCheck_NumberOfRates = '';

      $filterByCheck_FreeAndPaid = '';
      $filterByCheck_Free = '';
      $filterByCheck_Paid = '';

      // if($filterByCheck_FreeAndPaid == '' && $filterByCheck_Free == '' && $filterByCheck_Paid == '' )
      // {
      //      $filterByCheck_FreeAndPaid = 'checked';
      // }


      

?>

@extends('layouts.app')

@section('content')


<div> 
    
     
        <div class = 'container' id = 'containerId'>
             
            <div class = 'row backgroundNeutralDiv' style=" padding: 10px; border-radius: 7px; margin: 5px;">
                        <div class = 'col-xs-12 col-md-8'>
                            
                           
                             
                               <div class="input-group">
                                  <input id = 'searchInputId' type="text" class="form-control" placeholder="Seach" name="searchBarValue">
                                 
                                  <span class="input-group-btn">
                                       
                                       <button onclick="searchFilterTraining()" class="btn btn-primary material-icons">&#xE8B6;</button> 
                                          
                                       
                                        <button style="margin-left:5px;" class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Search type
                                           <span class="caret"></span></button>
                                      
                                       <div class="dropdown-menu" role="menu" aria-labelledby="menu1" style="padding: 5px;">
                                             <div>
                                                <label class="radio-inline">
                                                  <input  {{$searchTypeCheck_SearchByName}} type="radio" name="searchType" value = 'owner'>Search by owner name
                                                </label>
                                             </div>  
                                             <div> 
                                                <label class="radio-inline">
                                                  <input  {{$searchTypeCheck_SearchByTraining}}  type="radio" name="searchType" value = 'training'>Search by training name
                                                </label>
                                             </div>
                                            
                                           </div>  
                                             
                                               
                                           
                                    </span>
                               </div>
                             
                           

                          
                        </div>

                        <div class = 'col-xs-12 hidden-md hidden-sm hidden-lg hidden-xl' style="padding:2px;">

                        </div>  

                     <div class = 'col-xs-12 col-md-3' >
                         <div class = 'row'>
                              <div class="col-xs-6  col-md-6">
                                     <button  style="width: 100%;" class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Order by
                                     <span class="caret"></span></button>
                                     <div class="dropdown-menu" role="menu" aria-labelledby="menu1" style="padding: 5px;">
                                      <!--  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Lowest price first</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest price first</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Highest rate</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Number of rates</a></li>   -->  
                                         <div>
                                                <label class="radio-inline">
                                                  <input    onclick="searchFilterTraining()"  type="radio" name="orderBy"  {{$orderByCheck_LowestPrice}} value = 'lowest_first'>Lowest price first
                                                </label>
                                             </div>  
                                             <div> 
                                                <label class="radio-inline">
                                                  <input  onclick="searchFilterTraining()" type="radio" name="orderBy"  {{$orderByCheck_HighestPrice}} value = 'highest_first'>Highest price first
                                                </label>
                                             </div>
                                              <div> 
                                                <label class="radio-inline">
                                                  <input  onclick="searchFilterTraining()" type="radio" name="orderBy"  {{$orderByCheck_HighestRate}} value = 'highest_rate'>Highest rate
                                                </label>
                                             </div>
                                              <div> 
                                                <label class="radio-inline">
                                                  <input onclick="searchFilterTraining()" type="radio" name="orderBy"  {{$orderByCheck_NumberOfRates}}  value = 'number_of_rates'>Number of rates
                                                </label>
                                             </div>



                                     </div>

                              </div>
                                <div class="col-xs-6 col-md-6">
                                     <button style="width: 100%;" class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Filter by
                                     <span class="caret"></span></button>
                                     <div class="dropdown-menu" role="menu" aria-labelledby="menu1" style="padding: 5px;">
                                        <div>
                                                <label class="radio-inline">
                                                  <input onclick="searchFilterTraining()"  {{$filterByCheck_Paid}}  type="radio" name="filterBy" value = 'paid'>Paid
                                                </label>
                                             </div>  
                                             <div> 
                                                <label class="radio-inline">
                                                  <input onclick="searchFilterTraining()"  {{$filterByCheck_Free}}  type="radio" name="filterBy" value = 'free'>Free
                                                </label>
                                             </div>
                                              <div> 
                                                <label class="radio-inline">
                                                    <input onclick="searchFilterTraining()" {{$filterByCheck_FreeAndPaid}} type="radio" name="filterBy" value = 'free_and_paid'>Free and paid
                                                </label>
                                             </div>
                                      <!--  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Free</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Paid</a></li>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Free and Paid</a></li> -->
                                     </div>
                              </div>
                        </div>  
                     </div>   

                     <div class = 'col-xs-2 col-md-2'>
                        
                        
                     </div>   
                        
                        
              </div>
              
                    <div class = 'transparent' >
                  <div class = 'row'  style="margin-bottom: 20px;">
            @foreach ($trainingData as $rec)
                       
                 
                                
                                 <div  class = 'col-xs-12 col-md-3 col-lg-3'  style="margin-bottom: 20px;  padding: 20px; border-radius: 10px;" onclick="document.getElementById('{{'enterTrainingDetails' .  $numberOfData}}').submit();">
                                       
                                        <div class = 'text-center backgroundNeutralDiv' style="padding: 10px; border-radius: 10px;">
                                         <?php 

                                              $imageLink = Storage::disk('do_spaces')->url($rec->image); 


                                             
                                         ?>
                                         
                                                 <div id = 'profilePicId' class = 'profile_empty hidden-xs'  style="height: 200px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 5px;">  
                                                  </div>

                                                    <div id = 'profilePicId' class = 'profile_empty hidden-md hidden-sm hidden-lg hidden-xl'  style="height: 270px; background-image: url('{{$imageLink}}');  border-radius: 10%; margin-bottom: 5px;">  
                                                  </div>


                                             
                                                   
                                                   @if($rec->number_of_reviews == 1)
                                                       <p> {{$rec->number_of_reviews}} review</p>
                                                   @elseif($rec->number_of_reviews == null)
                                                     <p> 0 reviews </p>
                                                   @else

                                                    <p> {{$rec->number_of_reviews}} reviews</p>

                                                   @endif

                                                   
                                              

                                                      
                                                             @for($i=1;$i<11;$i++)

                                                                    @if($rec->rate >= $i)

                                                                         <span style="color:orange;">★</span>

                                                                    @else

                                                                     <span>★</span>

                                                                    @endif



                                                               @endfor

                                                            
                                                             @if( $rec->rate == null)
                                                                
                                                                        <p>Not rated yet</p>
                                                                 
                                                            @else
                                                               <p>Rate : {{ $rec->rate}} / 10</p>
                                                              

                                                            @endif

                                                            
                                                         
                                        
                                                    <div style="height: 25px; overflow: hidden;" >
                                                          <p> {{$rec->title}} </p>
                                                     </div>     

                                                           <p> Owner: {{$rec->name}}&nbsp;{{$rec->lastname}} </p>
                                                  
                                                    <div class = 'row' style="padding: 10px;">
                                                                    <div class = 'col-xs-4 col-md-4'>
                                                                       @if($rec->muscle_mass == 1)
                                                                        <img style="width: 30px; height: 30px;" src="/images/icons8-muscle-filled-50.png">
                                                                        @else

                                                                           <img style="width: 30px; height: 30px;" src="/images/icons8-muscle-filled-50-2.png">
                                                                        @endif

                                                                    </div>  
                                                                     <div class = 'col-xs-4 col-md-4'>
                                                                        @if($rec->strength == 1)
                                                                        <img style="width: 30px; height: 30px;" src="/images/icons8-weightlifting-filled-50-3.png">
                                                                        @else

                                                                           <img style="width: 30px; height: 30px;" src="/images/icons8-weightlifting-filled-50-2.png">
                                                                        @endif


                                                                    </div>  
                                                                     <div class = 'col-xs-4 col-md-4'>
                                                                        @if($rec->burn_kcal == 1)
                                                                        <img style="width: 30px; height: 30px;" src="/images/icons8-crossfit-filled-50.png">
                                                                        @else

                                                                           <img style="width: 30px; height: 30px;" src="/images/icons8-crossfit-filled-50-2.png">
                                                                        @endif
                                                                    </div>  
                                                           </div> 

                                                            <div class = 'row' style="padding: 10px;">
                                                                    <div class = 'col-xs-4 col-md-4'>
                                                                        Muscle
                                                                    </div>  
                                                                     <div class = 'col-xs-4 col-md-4'>
                                                                       Strength
                                                                    </div>  
                                                                     <div class = 'col-xs-4 col-md-4'>
                                                                       Cardio
                                                                    </div>  
                                                           </div> 
                                                        
                                                               
                                                               <form  id = '{{"enterTrainingDetails" . $numberOfData}}' method="post"  action = "{{URL::to('loadDetailTrainingDescription')}}" enctype="multipart/form-data">
                                                                                  <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />

                                                                                   <input type = "hidden" name = "training_id" value = "{{$rec->id_training}}" />

                                                                </form>

                                                                                 <!--  if rate is enabled, user bought training and he is in the list  -->
                                                               
                                                               
                                                                @if(!$rateEnabled) 
                                                                     @if($rec->price == 0)
                                                                         <button name = 'training_id' value="{{$rec->id_training}}" id = "{{$rec->id_training}}" class='btn btn-primary' style="width: 100%;">Free</button>
                                                                     @else

                                                                           <div class='btn btn-primary' style="width: 100%;">{{$rec->price}}{{$rec->currency}}</div>
                                                                    
                                                                    
                                                                     @endif

                                                                 @else 




                                                                 @endif    

                                                                   
                                                                      <!-- <table style="margin: auto;">
                                                                            <tr>
                                                                                  <td> 
                                                                                    {{$rec->price}}
                                                                                  </td>
                                                                                  <td>
                                                                                    
                                                                                    {{$rec->currency}}
                                                                                  </td>
                                                                        </tr>
                                                                      </table> -->
                                                                     
                                                                   
                                                                  
                                                          
                                                   
                                        </div>
                                      </div>
                                
                                     <?php $numberOfData ++; ?>
                                    @if($numberOfData == $max)
                                       
                                         </div>
                                    @endif     

                            @endforeach

          
                   
                      </div>
                    </div>
            </div>
        <!-- </div> -->

      <script src='/js/filterActions.js'></script>  

     
@endsection


<!-- 
<script type="text/javascript">
    
    $("#input-id").rating();





</script>
 -->


