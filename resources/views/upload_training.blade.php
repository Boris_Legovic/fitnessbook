

@extends('layouts.app')

@section('content')

    <div class = 'container'>

        @if(isset($fileToBig))
          <div class  = 'row'>
             <div class = 'text-center col-md-12'>
                <div class="col-md-12 alert alert-danger">
                  <p> Training upload failed. File is too big. Use file under 20mb. </p>
                </div>
             </div>
            </div> 
                
        @endif
         
         @if(isset($pictureToBig))
                

                    <div class  = 'row'>
             <div class = 'text-center col-md-12'>
                <div class="col-md-12 alert alert-danger">
                  <p> Training upload failed. Image is too big. Use image under 9mb. </p>
                </div>
             </div>
            </div> 

                
            

         @endif

         @if(isset($sucess))
              

                  <div class  = 'row'>
             <div class = 'text-center col-md-12'>
                <div class="col-md-12 alert alert-success">
                 <p> File uploaded successfull! </p>
                </div>
             </div>
            </div> 

              
           
         @endif

         <form  id = 'formTraining' method="post"  action = "{{URL::to('setDataToDB')}}" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>" />

                        @if(Auth::user()->stripe_id == '')
                                <div id = 'stripeState' value='false'>
                                    
                                </div>
                        
                        @else
                                <div id = 'stripeState' value='true'>
                                    
                                </div>

                        @endif

                       <div class = 'row'>
                                        <div class = 'col-xs-2 col-md-4 col-lg-4' >
                                        </div>

                                       <div class = 'col-xs-8 col-md-4 col-lg-4' >
                                                    
                                                     <div class = 'text-center'>
                                                             
                                                                <div class = 'row'>
                                                                       <div class = 'col-md-2 col-lg-2 col-xl-2'>
                                                                       </div>

                                                                        <div class = 'col-md-8 col-lg-8 col-xl-8'>
                                                                                 <img id = 'trainingImageId' style=" width: 100%;" src="/images/image_empty_light.png"  alt="Side">
                                                                        </div>
                                                                        <div class = 'col-md-2 col-lg-2 col-xl-2'>
                                                                       </div> 
                                                                  </div> 


                                                                         <div class = 'col-md-12'>
                                                                             <small id='imageNameId'>Max image size - 9mb</small>
                                                                    </div>
                                                                    <p id='imageNameId'>No image selected</p>
                                                                   
                                                            <input type="file" name = "image" id="fileImageId" style="display: none;" />                                                            

                                                             <div id = 'fileTrainingButtonId' class="btn btn-primary" style="height: 40px; width: 100%;"> 
                                                                <p id = 'upladTrainingTextId'>Upload training manual</p>
                                                                 

                                                              
                                                            </div>
                                                             <div class = 'col-md-12'>
                                                                     <small>Max file size - 20mb</small>
                                                              </div>       

                                                             <input name = 'file' type="file" id="fileTrainingId" style="display: none;" />  

                                                           
                                                             <div style="height: 20px;">
                                                                 
                                                             </div>
                                                            <div class= 'text-center'>
                                                                   <div>
                                                                        <input id = 'title' class = 'form-control' type="text" name="title" placeholder="Title">
                                                                   </div>
                                                                   <br>
                                                                   <div>
                                                                         Description:   <textarea id = 'descriptionId' name = "description" class = 'form-control' rows = '5'></textarea>
                                                                               

                                                                        

                                                                     </div>   


                                                             </div>
                                                         </div>
                                                      
                                                      <div class = 'row' >

                                                                                    <div class = 'col-xs-3 col-md-3 col-lg-3'>

                                                                                            Type:

                                                                                     </div>       
                                                                                   
                                                                                    <div class = 'col-xs-3 col-md-3 col-lg-3'>
                                                                                        <label class="radio-inline">
                                                                                                 <input  id='typeTrainingMuscleId' type="checkbox" name="typeMuscle" value = '1'>
                                                                                                 <p>Muscle</p>




                                                                                         </label>  
                                                                                    </div>
                                                                                    
                                                                                   <div class = 'col-xs-3 col-md-3 col-lg-3'>
                                                                                          <label class="radio-inline">
                                                                                                 <input id='typeTrainingStrengthId' type="checkbox" name="typeStrength" value = '1'> <p>  Strength</p>
                                                                                         </label>
                                                                                    </div>

                                                                                      <div class = 'col-xs-3 col-md-3 col-lg-3'>
                                                                                          <label class="radio-inline">
                                                                                                 <input  id='typeTrainingCardioId' type="checkbox" name="typeCardio" value = '1'> <p>Cardio</p>
                                                                                         </label>
                                                                                    </div>



                                                                           </div>
                                                             
                                                                         <div class = 'row' >

                                                                                    <div class = 'col-xs-4 col-md-4 col-lg-4'>

                                                                                            Licence:

                                                                                     </div>       
                                                                                   
                                                                                    <div class = 'col-xs-4 col-md-4 col-lg-4'>
                                                                                        <label class="radio-inline">
                                                                                                 <input onclick='hidePaidOptions()' id='firstUnitId' type="radio" name="typeMeassure" value = 'Free' checked>
                                                                                                 <p id = 'firstUnitIdLabel'>  Free</p>




                                                                                         </label>  
                                                                                    </div>
                                                                                    
                                                                                   <div class = 'col-xs-4 col-md-4 col-lg-4'>
                                                                                          <label class="radio-inline">
                                                                                                 <input onclick='showPaidOptions()' id='secondUnitId' type="radio" name="typeMeassure" value = 'Paid'> <p id = 'secondUnitIdLabel'>  Paid</p>
                                                                                         </label>
                                                                                    </div>



                                                                           </div>

                                                                            <div class = 'row' >
                                                                                     <div class = 'col-xs-4 col-md-4 col-lg-4'>

                                                                                             Price:
                                                    
                                                                                   </div>

                                                                                     <div class = 'col-xs-4 col-md-4 col-lg-4'>


                                                                                                <input id='price' class = 'form-control' type="text" name="price" placeholder="0,00"> 


                                                                                    </div>

                                                                                      <div class = 'col-xs-4 col-md-4 col-lg-4'>
                                                                                        <label class="radio-inline">
                                                                                                 <input id='firstUnitId' type="radio" name="curencyValue" value = '$' checked> <p id = 'firstUnitIdLabel'>$</p>
                                                                                         </label>  
                                                                                   
                                                                                  
                                                                                          <label class="radio-inline">
                                                                                                 <input  id='secondUnitId' type="radio" name="curencyValue" value = '€'> <p id = 'secondUnitIdLabel'>€</p>
                                                                                         </label>
                                                                                   

                                                                  

                                                                                    </div> 

                                                                         </div>   



                                                           


                                                                    <div class = 'row'>

                                                                             <div class = 'col-xs-12 col-md-12 col-lg-12'>

                                                                                     <div onclick="uploadComercialTraining()" id = 'fileTrainingButtonId' class="btn btn-primary" style="height: 40px; width:100%; margin-top:10px; margin-bottom: 20px;"> <p id = 'upladTrainingTextId'>Submit training</p>
                                                                                       <!--  <button id ='submitId'  type="submit">Submit</button> -->
                                                                              
                                                                                      </div>
                                                                              
                                                                              </div>


                                                                    </div>  

                                                         </div>
                                            

                                            <div class = 'col-xs-2 col-md-4 col-lg-4' >
                                        </div>    


                                     
        </div>  <!-- row ends here -->
</form>
</div> <!-- container ends here -->
     <script src="js/uploadTrainingActions.js"></script>


     @endsection

<!--     </body>
</html> -->

       

