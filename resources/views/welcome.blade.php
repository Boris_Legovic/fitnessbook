<html>
    <head>
        <link rel="stylesheet" href="/css/app.css">
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    </head>


    <style type="text/css">
        

        h2
        {
                color: #68EFAD;
        }

        h4
        {
                color: white;
        }

        h3
        {
                color: white;
        }

        p{
                color: white;
        }

        .text-pink{
                color: #f599db;
        }

        .row
        {
            margin:auto;
        }    

        .middleIcons
        {
           height: 50px;
        }


    </style>
    
    <body >


         
        <section id="services" style="  background-image: url('/images/trainer_customer_on_bench.jpg');  background-position: center; background-repeat: no-repeat; background-size: cover;">
             <div style="background-color: #00000061;">
                         <div class="container-fluid">
                              @include('inc.navbar') 
                         </div>

                      <div class="container">
                                        
                                


                        <div class="row" style="padding-top: 40%;">
                         

                          <div class="col-lg-12 col-md-12 text-center">

                            <h3 style="color: white;" class="section-subheading text-muted">The most proffesional fitness platform for trainers and athletes</h3>
                          </div>
                        </div>
                            <div class="row text-center" style="padding-bottom: 10%;">
                              <div class="col-md-4 text-center">
                                <img height="40px;" src="/images/icons8-edit-property-filled-50-2.png">
                                <h4 style="color:white;" class="service-heading">Record data</h4>
                                <p style="color: white;" class="text-muted">Record training data, create custom trainings, and visualize progress.</p>
                              </div>
                              <div class="col-md-4 text-center ">
                              <img  height="40px;" src="/images/icons8-personal-trainer-filled-50-2.png">
                                <h4  style="color: white;" class="service-heading">Find coach</h4>
                                <p style="color: white;" class="text-muted">Find the best coach online and enjoy an incredible coaching experience.</p>
                              </div>
                              <div  class="col-md-4 text-center">
                              <img height='40px;' src="/images/icons8-bench-press-filled-50.png">
                                <h4  style="color: white;" class="service-heading">Search training</h4>
                                <p  style="color: white;" class="text-muted">Get the most advanced training programs on the planet. Download, share or buy routines from the store. </p>
                              </div>
                            </div>

                          <!--   <div class = 'row text-center'>
                                
                                 <h4  style="color: white;" class="service-heading">Join the community</h4>
                                   <p  style="color: white;" class="text-muted">Get the most advanced training programs on the planet. Download, share or buy routines from the store. </p>

          
                            </div> -->

                      </div>
               </div>       
         
           
    </section>


    <section id="services" style="background-color: #9B26AF;"  >
      <div class="container-fluid" style="padding: 0;">

                



        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading" style="color: white;">Coaches</h2>
            <h3 class="section-subheading text-pink ">Running Your bussines was never so simple.</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>

                <div class = 'text-center' style=" width: 100%;">
                        <div  style="font-size: 50px; color: #68efad;" class="material-icons">&#xE860;</div>

                </div>    


            <h4 class="service-heading">Sell Your training programs</h4>
            <p class = 'text-pink'>Fitnessbook platform allows You to sell You training programs!. Upload the manual and earn money!</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
            </span>
             <div class = 'text-center' style=" width: 100%;">
                    <div  style="font-size: 50px; color: #68efad;" class="material-icons">&#xE80B;</div>

            </div>    
            <h4 class="service-heading">Online coaching</h4>
            <p class = 'text-pink'>With the simple tools from Fitnessbook, You can train clients from any place in the world. Just set the price for coaching, and wait for clients.</p>
          </div>
          <div class="col-md-4">
           
            <div class = 'text-center' style=" width: 100%;">
                    <div  style="font-size: 50px; color: #68efad;" class="material-icons">&#xE85C;</div>

            </div>    

            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Visualise client progress</h4>
            <p class = 'text-pink'>Check all Your client progress, and help them progress faster!</p>
          </div>
        </div>
      </div>
    </section>
<!-- 
    <section id="services" style="  background-image: url('/images/mirin.jpg'); height: 100%; background-position: center; background-repeat: no-repeat; background-size: cover;">
     
      <div class="container-fluid">

        <div class="row">
                  <div class="col-lg-12 text-center">
                    <h3 style="color: white;" class="section-subheading text-muted">Simple body measure tracking</h3>
                  </div>
         </div>

        <div class="row">
        
                <div class = 'col-md-4'  >



                </div>

                <div class = 'col-md-4' style="background-image: url('/images/training_data.png'); border-radius: 10px; height: 370px; background-position: center; background-repeat: no-repeat; 
                     background-size: cover; ">



                </div>


                 <div class = 'col-md-4' >



                </div>


         </div>
      </div>
    </section> -->

      <section id="services">
     
      <div class="container"   >
        <div class="row" style=" margin: auto;">
        
                <div class = 'col-md-6'>
                        <img  src="/images/new_body_data.png" style="width: 100%;"   />
                        <div style="padding-left: 20px;">
                             <h4 class="service-heading text-muted">Track body measure</h4>
                            
                             <p class="text-muted">
                                    Visualise progress by tracking you body data.
                             </p>
                        
                         <p class="text-muted">  

                            You can directly track data from mobile app.

                         </p> 

                         </div>    

         
                </div>
                <div class= 'col-md-6'>
                         
                                                          <img  src="/images/alone_white_clear.jpg" style="width: 100%;" />

                         <div class = 'row' style="width: 100%; background-color: #68EFAD; border-radius: 10px; margin: auto;" >

                                    <div class="col-md-6">
                                            
                                             <div class = 'text-center' style=" width: 100%;">
                                                    <div  style="font-size: 50px; color: #3b846d;" class="material-icons">&#xE422;</div>

                                            </div>    
                                            <h4 class="service-heading text-center"><b>MUSCLE/FAT</b></h4>
                                            <p class = 'text-muted text-center'>Compare Your body composition with other people around the world. </p>
                                    </div>

                                     <div class="col-md-6">
                                            
                                             <div class = 'text-center' style=" width: 100%;">
                                                    <div  style="font-size: 50px; color: #3b846d;" class="material-icons">&#xE84E;</div>

                                            </div>    
                                            <h4 class="service-heading text-center"><b>BMI</b></h4>
                                            <p class = 'text-muted text-center'>Check Your BMI and improve overall health.</p>
                                    </div>
                         </div>
                       
                </div>

        

     </div>

      </div>
    </section>

     <section id="services" style="background-color: #68EFAD;">
            
            <div class="container-fluid" style="padding: 0;">

           <div class="row">
                <div class="col-lg-12 text-center">
                  <h2 class="section-heading text-muted">Athletes and lifters</h2>
                  <h3 class="section-subheading text-muted">Dont waste time. Make real progress.</h3>
                </div>
              </div>
              <div class="row text-center">
                <div class="col-md-4">
                  <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                  </span>

                  <div class = 'text-center' style=" width: 100%;">
                          <div  style="font-size: 50px; color: #3b846d; " class="material-icons">&#xE85D;</div>

                  </div>  

                  <h4 class="service-heading">Track data to improve</h4>
                  <p class="text-muted">By tracking Your data, You will be able to visualize if Your training regime really works. If You don't see progress, find a coach or a new training program from our database. Simple and easy. </p>
                </div>
                <div class="col-md-4">
                  <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                  </span>

                    <div class = 'text-center' style=" width: 100%;">
                            <div  style="font-size: 50px; color: #3b846d; " class="material-icons">&#xE8B6;</div>

                    </div>  

                  <h4 class="service-heading">Need a coach?</h4>
                  <p class="text-muted">Find the best coach for Your goals. Lose weight, get muscle or just train to stay healthy. No matter what Your goal is, we will provide top fitness trainer for online coaching. Each trainer have a rate from his customers, so pick the best! </p>
                </div>
                <div class="col-md-4">
                  <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                  </span>

                    <div class = 'text-center' style=" width: 100%;">
                            <div  style="font-size: 50px; color: #3b846d; " class="material-icons">&#xEB43;</div>

                    </div>  

                  <h4 class="service-heading">Need a training program/routine?</h4>
                  <p class="text-muted">In Fitnessbook database, You can find the best training programs rated by users. Also, anyone can upload his own training book and routine.</p>
                </div>
              </div>
         </div>
     
    </section>

     <section id="services" style="background-color: white; background-image: url('/images/gianni_profile.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
     
      <div class="container-fluid" >
        <div class="row">
        
                <div class = 'col-md-5'>
                        <img src="/images/client_list.png" style="width: 100%;" />
                        <div style="margin-left: 20px;">
                             <h4 class="service-heading text-muted">Track client progress</h4>
                            
                             <p class="text-muted">
                                    See all Your customer data body composition progress in one view. 
                             </p>
                        
                         <p class="text-muted">  

                            Communicating with clients was never so simple!

                         </p> 

                         </div>    

         
                </div>

                <div class= 'col-md-7'>
                         <img src="/images/data_training.png" style="width: 100%;" />
                         <div style="margin-left: 20px;">
                             <h4 class="service-heading text-muted">Athlete track their treining progress</h4>
                            
                             <p class="text-muted">
                                    Visualise all your reps and weight lifted per training session and improve Your performance!
                             </p>
                        
                         <p class="text-muted">  


                         </p> 

                         </div>  
                       
                </div>

        

         <div class="col-md-5">

         </div>

     </div>

      </div>
    </section>

     <section id="services" style="background-color: #9B26AF;"   >
      <div class="container-fluid" style="padding: 0;">

                



        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading" style="color: white;">Fitnessbook - free online coaching platform</h2>
            <h3 class="section-subheading" style="color: white; ">Athletes, proffesionals, casual lifters and personal trainers</h3>
          </div>
        </div>
        
       

          <div class ='row' >
                <div class = 'row'>
                       
                     <div class = 'col-md-4'>  
                          <div class = 'text-center'>
                               <img class = 'middleIcons'  src="/images/icons8-combo-chart-filled-50.png">
                               <p>Track body composition</p>
                          </div>     
                
                    </div>

                     <div class = 'col-md-4'>  
                          <div class = 'text-center'>
                                 <img class = 'middleIcons' src="/images/icons8-deadlift-filled-50.png">
                                 <p>Find, create or buy the best training routines online</p>
                          </div>       
                    </div>
              
                <div class = 'col-md-4'>  

                    <div class = 'text-center'>
                           <img   class = 'middleIcons' src="/images/icons8-compare-filled-50.png">
                            <p>Compare Your body composition with all the users around the world</p>

                     </div>      
               </div>
               

             </div>
               
                <div class = 'row'>

                    <div class = 'col-md-4'>  
                          <div class = 'text-center'>
                                <img class = 'middleIcons'  src="/images/icons8-statistics-filled-50.png">
                                 <p>Track Your strength. Strength is a great indicator of a good training routine</p>

                          </div>      
                  </div>
              
                    <div class = 'col-md-4'>  
                            <div class = 'text-center'>
                                  <img class ='middleIcons' src="/images/icons8-sell-filled-50.png">
                                   <p>Sell You training routine. Just connect with stripe, set the price and upload Your routine. Fitnessbook charges only 9% of every transaction.</p>

                             </div>     
                    </div>
                  
                    <div class = 'col-md-4'>  

                            <div class = 'text-center'>
                                  <img class = 'middleIcons' src="/images/icons8-google-images-filled-50.png">
                                  <p>Upload photos of Yourself to inspire other. Show Your progress or Your clients progress to the world.</p>


                             </div>     

                     </div>   
              </div>
          </div>  

       
      </div>
    </section>

     <section id="services">
     
      <div class="container"   >
        <div class="row" style=" margin: auto;">
        
                <div class = 'col-md-3'>
                    

         
                </div>
                <div class= 'col-md-6'>
                         
                                                          <img  src="/images/trainer_and_customer.jpg" style="width: 100%;" />

                         <div class = 'row' style="width: 100%;  margin: auto;" >

                                    <div class="col-md-12 text-center">
                                           <h4 style="color: black;">Download mobile app and track training at the gym!</h4> 
                                           
                                    </div>
                                    <div class = 'row text-center'>

                                          <img onclick="alert('Application will be available soon!')" src="images/apple2.png">
                                           <img  onclick="alert('Application will be available soon!')" style="height: 85px;" src="images/google-play-badge.png">
                                     </div>  


                                    
                         </div>

                         
                        <div class="text-center">
                            <a href ='https://web.facebook.com/Fitnessbookorg-199772057349908/?modal=admin_todo_tour' ><img src="/images/facebok_icon.png" class="img-fluid" alt="" style="height: 50px; width: 50px;">
 </a>
                               <p style="color: black;"> Follow us on Facebook </p>
                         </div>
                </div> 

               <div class = 'col-md-3'>
               
               </div> 

                
        

     </div>

      </div>
    </section>

         <section id="services" style="background-color: white;">
          <div  style="background-color: #68EFAD; height: 1px; width: 90%; margin: auto;">
          </div>
     
      <div class="container"   >
        <div class="row" style=" margin: auto;">
        
                <div class = 'col-md-3'>
                    

         
                </div>
         

           <div class = 'col-md-6'>
              <div class = 'text-center'>
                   <h2 class="section-heading" style="color: #68EFAD;">Help us!
                   </h2>

                   <p style="color: #68EFAD;"> Fitnessbook is still in early development. If You like our work, donate and help us to improve!</p>
                  
                  <a href='https://www.paypal.me/fitnessbook'><button class='btn btn-primary' style="background-color: #68EFAD">Donate</button> </a>

                 
              </div>

           </div>

               <div class = 'col-md-3'>
               
               </div> 

                
        

     </div>

      </div>
    </section>



    </body>
</div>
   @include('inc.footer')





</html>
