<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

//privacy, terms and cookies

Route::get('privacyPolicy', function () {
    return view('privacy.privacy_policy');
});

Route::get('cookies', function () {
    return view('privacy.cookies');
});

Route::get('terms_of_service', function () {
    return view('privacy.terms_of_service');
});


Route::get('/contact/sent',function(){
  return view('contact')->with('isSent',true);
});

Route::get('/contact',function(){
	return view('contact');
});

Route::get('/trainingProgressShow',function(){
  return view('training_progress')->with('progress',true);
});
//first parameter is input, second is the controller and function side
Route::post('/contact/submit','MessagesController@submit');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('addNewTraining', 'DatabaseQuerryController@addNewTraining');

Route::get('clientProgress', 'CoachListController@clientProgress');

Route::post('addNewTraining', 'DatabaseQuerryController@addNewTrainingPost');

Route::post('loadTrainingList', 'DatabaseQuerryController@loadTrainingList'); // for personal training, not list of trainings to download

Route::post('loadTrainingDataInsert', 'DatabaseQuerryController@loadTrainingDataInsert');

Route::post('addNewTrainingDataToDB', 'DatabaseQuerryController@addNewTrainingDataToDB');

Route::post('loadTrainingProgress', 'DatabaseQuerryController@loadTrainingProgress');

Route::post('loadAddTrainingModal', 'DatabaseQuerryController@loadAddTrainingModal');

Route::post('addNewMeassurementToDB', 'DatabaseQuerryController@addNewMeassurementToDB');

Route::post('loadMeassurementData', 'DatabaseQuerryController@loadMeassurementData');

Route::get('loadBodyRank', 'DatabaseQuerryController@loadBodyRank');

Route::get('loadMuscleFatPercentage', 'DatabaseQuerryController@loadMuscleFatPercentage');






Route::post('loadKcal', 'DatabaseQuerryController@loadKcal');

Route::post('insertKcal', 'DatabaseQuerryController@insertKcal');

Route::post('findTrainingIndex', 'TrainingListController@index');

Route::post('findCoachIndex', 'CoachListController@index');

Route::post('showCoachingDetail','CoachListController@showCoachingDetail');


Route::post('uploadTrainingIndex', 'UploadTrainingController@uploadTrainingIndex');

Route::post('setDataToDB', 'UploadTrainingController@setDataToDB');

Route::post('loadDetailTrainingDescription','TrainingDetailsDescripton@loadDetailTrainingDescription');

Route::post('checkout','TransactionsController@checkout');

Route::post('payPictures','TransactionsController@payPictures');


Route::post('checkoutCoaching','TransactionsController@checkoutCoaching');




Route::get('stripeUserConfirmation','TransactionsController@stripeUserConfirmation');

Route::post('downloadAlreadyBoughtFile','TransactionsController@downloadAlreadyBoughtFile');



Route::post('showPicturesView','MainProfile@showPicturesView');

Route::post('uploadCoverPicture','MainProfile@uploadCoverPicture');

Route::post('uploadProfilePicture','MainProfile@uploadProfilePicture');

Route::post('uploadNormalPicture','MainProfile@uploadNormalPicture');

Route::post('deletePicture','MainProfile@deletePicture');




Route::get('verifyEmailFirst','Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');

Route::get('showPicturesView','MainProfile@showPicturesView');

Route::get('settings','UserOptionsController@settings');

Route::get('verifyEmailFirst','Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');

Route::get('verify/{email}/{verifyToken}','Auth\RegisterController@sendEmailDone')->name('sendEmailDone');

Route::post('/updateName','UserOptionsController@updateName');

Route::post('/updateLastname','UserOptionsController@updateLastname');

Route::post('/updatePictures','UserOptionsController@updatePictures');

Route::post('/coachingPeriod','UserOptionsController@coachingPeriod');

Route::post('/onlineCoaching','UserOptionsController@onlineCoaching');

Route::post('/updateCoachingPrice','UserOptionsController@updateCoachingPrice');

Route::post('/priceCurrency','UserOptionsController@priceCurrency');

Route::post('/weightUnit','UserOptionsController@weightUnit');

Route::post('/sizeUnit','UserOptionsController@sizeUnit');

Route::post('showTrainerList', 'TrainerListController@showTrainerList');

Route::get('showTrainerList', 'TrainerListController@showTrainerList');

Route::post('loadTrainerProfile','MainProfile@loadTrainerProfile');

Route::post('downloadFile', 'TransactionsController@downloadFile');

Route::get('/getMessages', function(){
          $allUsers1 = DB::table('users')
          ->Join('conversation','users.id','conversation.user_one')
          ->where('conversation.user_two', Auth::user()->id)->get();
          //return $allUsers1;

          $allUsers2 = DB::table('users')
          ->Join('conversation','users.id','conversation.user_two')
          ->where('conversation.user_one', Auth::user()->id)->get();
          $merged_arrays = array_merge($allUsers1->toArray(), $allUsers2->toArray());
            
           usort($merged_arrays,function($first,$second)
           {
               return $first->created_at < $second->created_at;
           });
 


          return $merged_arrays;
        });

Route::get('/getMessages/{id}', function($id){
					//update cov status

          $conversation = DB::table('conversation')->where('id',$id)->get();

          $user_one_or_two = '';

            if($conversation[0]->user_one == Auth::user()->id)
            {
                  $user_one_or_two = 'user_one_status';
            }
            else
            {
                 $user_one_or_two = 'user_two_status';
            }



              DB::table('conversation')->where('id',$id)
                 ->update([
                  $user_one_or_two => 0 // now read by user
                   ]);
					
         
          $userMsg = DB::table('messages')
          ->select()
          ->join('users', 'users.id','messages.user_from')
          ->where('messages.conversation_id', $id)
          ->orderBy('created_at_time', 'asc')
          ->get();
          return $userMsg;
        });


Route::get('/getUserConversationPicture/{id}', function($id){
          //update cov status
          
          $conversation_row =  DB::table('conversation')->where('id',$id)->get();
          $user_to_speak;

          if($conversation_row[0]->user_one == Auth::user()->id)
          {
            $user_to_speak = $conversation_row[0]->user_two;
          } 
          else
          {
             $user_to_speak = $conversation_row[0]->user_one;

          }

          $userToSpeakWithInfo = DB::table('users')
          ->where('id', $user_to_speak )
          ->get();
         
          return $userToSpeakWithInfo;
       
        });


Route::post('search', 'PostsController@search');
Route::get('try',function(){
	return App\post::with('user','likes','comments')->get();
});

Route::get('newMessage','ProfileController@newMessage');
Route::post('newMessageTo', 'ProfileController@newMessageTo');
Route::post('/sendMessage', 'ProfileController@sendMessage');
Route::get('messages', 'ProfileController@messages');

Route::post('idOfUserToRecive', 'ProfileController@idOfUserToRecive');
Route::post('getLoggedInUserId','ProfileController@getLoggedInUserId');
Route::post('updateMessageNotification','ProfileController@updateMessageNotification');
Route::post('updateNotification','ProfileController@updateNotification');

Route::post('updateNotificationRequest','ProfileController@updateNotificationRequest');




Route::post('rateTrainer','RateController@rateTrainer');
Route::post('rateTraining','RateController@rateTraining');
Route::post('showTrainingRateModal','RateController@showTrainingRateModal');
Route::get('trainingsBought','TrainingListController@trainingsBought');
Route::get('getNotifications','ProfileController@getNotifications');
Route::get('notificationList','ProfileController@notificationList');

Route::get('filterTraining','TrainingListController@filterTraining');
Route::get('filterTrainer','TrainerListController@filterTrainer');

Route::post('checkIfUserHaveStatusVerified','DatabaseQuerryController@checkIfUserHaveStatusVerified');

Route::post('deleteTraining','DatabaseQuerryController@deleteTraining');

Route::post('sendTraining','DatabaseQuerryController@sendTraining');

Route::post('loadClientList','DatabaseQuerryController@loadClientList');

Route::get('getNewMessageUser','ProfileController@getNewMessageUser');

Route::get('resetPassword','ProfileController@resetPassword');

Route::post('goToFreeCoachingRequest', 'TransactionsController@goToFreeCoachingRequest');

Route::post('aproveFreeCoaching', 'TransactionsController@aproveFreeCoaching');

Route::get('trainerRequestList','ProfileController@trainerRequestList');

Route::post('loginUserWithFacebook','DatabaseQuerryController@loginUserWithFacebook');


Route::get('enterApp','DatabaseQuerryController@enterApp');

Route::get('goToDetailFacebookSelection','DatabaseQuerryController@goToDetailFacebookSelection');


Route::post('enterAppWithDetails','DatabaseQuerryController@enterAppWithDetails');




































